<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<!-- Footer goes here-->
<footer class="global-footer"> 
  <div class="container"> 
    <div class="global-footer-logo"> 
      <div class="global-footer-container"><img src="<?php bloginfo('template_directory'); ?>/assets/images/temp/ipg-logo.jpg" atl=""/>
        <div class="global-footer-text-container">
          <div class="global-footer-text">&copy; <?php echo date('Y'); ?> C &amp; G Property Management</div><a href="<?php bloginfo('wpurl'); ?>/about-cg" class="global-footer-link">About Us</a><a href="<?php bloginfo('wpurl'); ?>/application-form" class="global-footer-link">Contact</a>
        </div>
      </div>
      <div class="global-footer-minicontainer"><span class="global-footer-designed">Designed and Developed by </span><a href="http://kurt-j.com" class="global-footer-devloped">Kurt Johnson.</span></div>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
