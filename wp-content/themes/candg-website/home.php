<?php
/**
  *Template Name: Home
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); 

if(have_posts()):
while(have_posts()): the_post();
?>

<!-- END HEADER-->
  <!-- START : PAGE CONTENT-->
  <!-- START FEATURE CALLOUT-->
  <?php if( get_field('hero_image') ): ?>
  <div id="featureCallout" style="background-image: url(<?php the_field('hero_image'); ?>);" class="feature-callout">
    <div class="container">
      <h2 class="feature-callout__initial-header"><?php the_field('hero_thin_title'); ?></h2>
      <h2 class="feature-callout__final-header"><?php the_field('hero_thick_title'); ?></h2>
      <a href="<?php the_field('hero_button_link'); ?>" class="button-callout"><?php the_field('hero_button_text'); ?></a> </div>
  </div>
  <?php endif; ?>
  <!-- END FEATURE CALLOUT-->
  <!-- START : TRIPLE CALLOUT-->
  <section class="section">
    <div class="container">
      <header class="section__header">
        <h3 class="section__main-title"><?php the_field('section_bold_title'); ?></h3>
        <h4 class="section__sub-title"><?php the_field('section_thin_title'); ?></h4> </header>

        <?php if( have_rows('callout') ): ?>
          <ul class="callout-list">
            <?php while( have_rows('callout') ): the_row(); 
                // vars
                $image = get_sub_field('callout_main_image');
                $title = get_sub_field('callout_bold_text');
                $des = get_sub_field('callout_description');
            ?>
                <li class="callout-list__item">
                 <a href="#/" data-featherlight="&lt;h3&gt;<?php echo $title; ?>&lt;/h3&gt;&lt;p&gt;<?php echo $des; ?> &lt;/p&gt;"
                  class="callout-list__item__anchor"><img src="<?php echo $image['url']; ?>" alt=""><span class="callout-list__item__title"><?php echo $title; ?></span></a>
            <?php endwhile; ?>
          </ul>
        <?php endif; ?>
      </ul>
    </div>
  </section>


  <!-- END : TRIPLE CALLOUT-->
  <!-- START FEATURE CALLOUT-->
  <?php if( get_field('callout_background_image') ): ?>
  <div id="featureCallout" style="background-image: url(<?php the_field('callout_background_image'); ?>);" class="feature-callout feature-callout--mini">
    <?php endif; ?>
    <div class="container">
      <h2 class="feature-callout__initial-header"><?php the_field('callout_title'); ?></h2>
      <h2 class="feature-callout__final-header"><?php the_field('callout_subtitle'); ?></h2><a href="<?php the_field('callout_button_link'); ?>" id="button"><?php the_field('callout_button_text'); ?></a> </div>
  </div>
  <!-- END FEATURE CALLOUT-->
  <!-- START : ALTERNATE CALLOUT-->
  <section class="section">
    <div class="container" id="find_more">
      <header class="section__header">
        <h3 class="section__main-title"><?php the_field('bold_txt'); ?></h3>
        <h4 class="section__sub-title"><?php the_field('thin_txt'); ?></h4> </header>
         <?php if( have_rows('callouts') ): ?>
          <ul class="callout-list">
            <?php while( have_rows('callouts') ): the_row(); 
                // vars
                $image = get_sub_field('photo');
                $image2 = get_sub_field('photo2');
                $title = get_sub_field('bold_text');
                $title2 = get_sub_field('bold_text2');
                $des = get_sub_field('popup_description');
                $des2 = get_sub_field('popup_description2');
            ?>
                <li class="callout-list__item callout-list__item--large">
                  <a href="#/" data-featherlight="&lt;h3&gt;<?php echo $title2; ?>&lt;/h3&gt;&lt;p&gt;<?php echo $des2; ?> &lt;/p&gt;"
                  class="callout-list__item__anchor"><img src="<?php echo $image2['url']; ?>" alt=""><span class="callout-list__item__title"><?php echo $title2; ?></span></a>   

                <li class="callout-list__item">
                  <a href="#/" data-featherlight="&lt;h3&gt;<?php echo $title; ?>&lt;/h3&gt;&lt;p&gt;<?php echo $des; ?> &lt;/p&gt;"
                  class="callout-list__item__anchor"><img src="<?php echo $image['url']; ?>" alt=""><span class="callout-list__item__title"><?php echo $title; ?></span></a>     
            <?php endwhile; ?>
          </ul>
        <?php endif; ?>
      </ul>
    </div>
  </section>
  <!-- END : ALTERNATE CALLOUT-->

<?php 
endwhile;
endif;

get_footer(); ?>