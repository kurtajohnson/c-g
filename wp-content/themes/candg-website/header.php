<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head><title>C&amp;G</title>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
  <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>
  <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
  <link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/assets/css/main.css"/>
  <script src="//cdnjs.cloudflare.com/ajax/libs/require.js/2.1.17/require.min.js"></script>

  <link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/assets/icons/favicon.png" type="image/x-icon"/>

  <script src="<?php bloginfo('template_directory'); ?>/assets/js/main.js"></script>
  <script src="<?php bloginfo('template_directory'); ?>/assets/js/svg4everybody.js"></script>
  <!--[if lt IE 9]>
  <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
  <![endif]-->

  <?php wp_head(); ?>

</head>

<body <?php body_class(); ?> >

<!-- START : HEADER-->
<header id="header" class="header">
  <div class="nav-bg"></div>
  <div class="container">
    <h1 class="logo"><a href="<?php bloginfo('wpurl'); ?>">></a>C&amp;G</h1>
    <div class="un-nav"><a href="tel:+1(352)243-4424" class="call-us">+1(352)243-4424</a><a href="<?php bloginfo('wpurl'); ?>/book-listing/" class="button">Book my getaway</a></div>
  </div>
</header>
<!-- END : HEADER-->