<?php
/**
 * Template Name: Book listing
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

  <!-- START : PAGE CONTENT-->
  <div id="featureCallout" style="background-image: url(<?php bloginfo('template_directory'); ?>/assets/images/featured-renters-callout-mini.jpg);" class="feature-callout-renters__mini undefined">
    <div class="container"></div>
  </div>
  <!-- END FEATURE CALLOUT BANNER-->
  <!-- START : section-colum-search-->
  <section class="section-lightblue borderline">
    <div class="container">
      <div class="section__column">
        <div class="hero_container">
          <div class="hero_label">DATES</div>
          <div class="mini_form-">
            <div class="mini_form">
              <label class="mini_form__label check">Check-In</label>
              <input type="text" id="checkin" name="checkin" value="mm/dd/yy" class="mini_form__input"> </div>
            <div class="mini_form">
              <label class="mini_form__label">Check-Out</label>
              <input type="text" id="checkout" name="checkout" value="mm/dd/yy" class="mini_form__input"> </div>
            <div class="mini_form">
              <label class="mini_form__label guest">Guests</label>
              <div data-reactid=".1.0.0.$=14:0.1" class="select-number">
                <select id="guests" name="guests" data-reactid=".1.0.0.$=14:0.1.0">
                  <option value="1">1 </option>
                  <option value="2">2 </option>
                  <option value="3">3 </option>
                  <option value="4">4 </option>
                  <option value="5">5 </option>
                  <option value="6">6 </option>
                  <option value="7">7 </option>
                  <option value="8">8 </option>
                  <option value="9">9 </option>
                  <option value="10">10 </option>
                  <option value="11">11 </option>
                  <option value="12">12 </option>
                  <option value="13">13 </option>
                  <option value="14">14 </option>
                  <option value="15">15 </option>
                  <option value="16">16+ </option>
                </select>
              </div>
            </div>
          </div>
          <div class="hero_label">AMENITIES</div>
          <ul class="large_form-container">
            <?php
              // your taxonomy name
              $tax = 'amenities';
              // get the terms of taxonomy
              $terms = get_terms( $tax, $args = array('hide_empty' => true,));

              // loop through all terms
              foreach( $terms as $term ) {
                echo '<li class="form_listing" data-amenity="' . $term->slug . '"><a href="' . get_term_link($term) . '">' . $term->name . '</a></li>';
              } 
            ?>
          </ul>
          <div class="hero_label">
            <div class="mini_form_btn"><a href="#/" id="book-it-btn">SHOW LISTINGS</a></div>
          </div>
        </div>
      </div>
      <div class="section__column">
        <h3 class="column__title">Property Search</h3>
        <div class="pages">
          <div class="current__pages">1 - 8 of 40 Rentals</div>
          <ul class="navbar__pages">
            <li class="navbar__pages-list"><a href="#" class="navbar__pages-item active">1</a></li>
            <li class="navbar__pages-list"><a href="#" class="navbar__pages-item"> 2</a></li>
            <li class="navbar__pages-list"><a href="#" class="navbar__pages-item"> 3</a></li>
            <li class="navbar__pages-list"><a href="#" class="navbar__pages-item"> 4</a></li>
            <li class="navbar__pages-list">
              <a href="#" class="navbar__pages-item">
                <div class="navbar__pages-arrow"></div>
              </a>
            </li>
          </ul>
        </div>
        <div class="booking-items">
          <ul class="callout-list"></ul>
          <li class="callout-list__item-list">
            <a href="/wp_data/?page_id=26 " class="callout-list__item__anchor"><img src="<?php bloginfo('template_directory'); ?>/assets/images/temp/house1.jpg" alt="">
              <div class="callout-list__item-list__price">$159</div>
              <div class="callout__list-item_name">Luxury Family Retreat</div>
              <div class="callout__list-item_discription">4 bedroom / 3 bath 1 story / 1900sq.ft</div>
            </a>
          </li>
          <li class="callout-list__item-list">
            <a href="/wp_data/?page_id=26" class="callout-list__item__anchor"><img src="<?php $field = get_field('property_cover_image', $post_id, $format_value); ?>" alt="" class="callout-booking-img">
              <div class="callout-list__item-list__price"><?php $field = get_field('property_day_rate_us', $post_id, $format_value); ?></div>
              <div class="callout__list-item_name"><?php $field = get_field('title', $post_id, $format_value); ?></div>
              <div class="callout__list-item_discription"><?php $field = get_field('property_bedrooms', $post_id, $format_value); ?> bedroom / <?php $field = get_field('property_bathrooms', $post_id, $format_value); ?> bath <?php $field = get_field('property_floors', $post_id, $format_value); ?> story / <?php $field = get_field('property_square_footage', $post_id, $format_value); ?></div>
            </a>
          </li>
          <li class="callout-list__item-list">
            <a href="/wp_data/?page_id=26 " class="callout-list__item__anchor"><img src="<?php bloginfo('template_directory'); ?>/assets/images/temp/house1.jpg" alt="" class="callout-booking-img">
              <div class="callout-list__item-list__price">$159</div>
              <div class="callout__list-item_name">Luxury Family Retreat</div>
              <div class="callout__list-item_discription">4 bedroom / 3 bath 1 story / 1900sq.ft</div>
            </a>
          </li>
          <li class="callout-list__item-list">
            <a href="/wp_data/?page_id=26 " class="callout-list__item__anchor"><img src="<?php bloginfo('template_directory'); ?>/assets/images/temp/house2.jpg" alt="" class="callout-booking-img">
              <div class="callout-list__item-list__price">$79</div>
              <div class="callout__list-item_name">Disney Vacation Home </div>
              <div class="callout__list-item_discription">3 bedroom / 3 bath 2 story / 1400sq.ft</div>
            </a>
          </li>
          <li class="callout-list__item-list">
            <a href="/wp_data/?page_id=26 " class="callout-list__item__anchor"><img src="<?php bloginfo('template_directory'); ?>/assets/images/temp/house1.jpg" alt="" class="callout-booking-img">
              <div class="callout-list__item-list__price">$159</div>
              <div class="callout__list-item_name">Luxury Family Retreat</div>
              <div class="callout__list-item_discription">4 bedroom / 3 bath 1 story / 1900sq.ft</div>
            </a>
          </li>
          <li class="callout-list__item-list">
            <a href="/wp_data/?page_id=26 " class="callout-list__item__anchor"><img src="<?php bloginfo('template_directory'); ?>/assets/images/temp/house2.jpg" alt="">
              <div class="callout-list__item-list__price">$79</div>
              <div class="callout__list-item_name">Disney Vacation Home </div>
              <div class="callout__list-item_discription">3 bedroom / 3 bath 2 story / 1400sq.ft </div>
            </a>
          </li>
          <li class="callout-list__item-list">
            <a href="/wp_data/?page_id=26 " class="callout-list__item__anchor"><img src="<?php bloginfo('template_directory'); ?>/assets/images/temp/house1.jpg" alt="">
              <div class="callout-list__item-list__price">$159</div>
              <div class="callout__list-item_name">Luxury Family Retreat</div>
              <div class="callout__list-item_discription">4 bedroom / 3 bath 1 story / 1900sq.ft</div>
            </a>
          </li>
          <li class="callout-list__item-list">
            <a href="/wp_data/?page_id=26 " class="callout-list__item__anchor"><img src="<?php bloginfo('template_directory'); ?>/assets/images/temp/house2.jpg" alt="" class="callout-booking-img">
              <div class="callout-list__item-list__price">$79</div>
              <div class="callout__list-item_name">Disney Vacation Home</div>
              <div class="callout__list-item_discription">3 bedroom / 3 bath 2 story / 1400sq.ft</div>
            </a>
          </li>
          <li class="callout-list__item-list">
            <a href="/wp_data/?page_id=26 " class="callout-list__item__anchor"><img src="<?php bloginfo('template_directory'); ?>/assets/images/temp/house1.jpg" alt="" class="callout-booking-img">
              <div class="callout-list__item-list__price">$159</div>
              <div class="callout__list-item_name">Luxury Family Retreat</div>
              <div class="callout__list-item_discription">4 bedroom / 3 bath 1 story / 1900sq.ft</div>
            </a>
          </li>
          <li class="callout-list__item-list">
            <a href="/wp_data/?page_id=26 " class="callout-list__item__anchor"><img src="<?php bloginfo('template_directory'); ?>/assets/images/temp/house2.jpg" alt="" class="callout-booking-img">
              <div class="callout-list__item-list__price">$79</div>
              <div class="callout__list-item_name">Disney Vacation Home </div>
              <div class="callout__list-item_discription">3 bedroom / 3 bath 2 story / 1400sq.ft</div>
            </a>
          </li>
        </div>
        <div class="pages">
          <div class="current__pages">1 - 8 of 40 Rentals</div>
          <ul class="navbar__pages">
            <li class="navbar__pages-list"><a href="#" class="navbar__pages-item active">1</a></li>
            <li class="navbar__pages-list"><a href="#" class="navbar__pages-item"> 2</a></li>
            <li class="navbar__pages-list"><a href="#" class="navbar__pages-item"> 3</a></li>
            <li class="navbar__pages-list"><a href="#" class="navbar__pages-item"> 4</a></li>
            <li class="navbar__pages-list">
              <a href="#" class="navbar__pages-item">
                <div class="navbar__pages-arrow"></div>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <!-- END FEATURE CALLOUT-->
  <!-- END : PAGE CONTENT-->
<!-- Footer goes here-->
<?php get_footer(); ?>
