define(["jquery", "flexslider"], function($) {
  return $('.feature-callout-renters__banner').on('click', function() {
    consol.log('.feature-callout-renters__banner');
    $('#carousel').flexslider({
      animation: 'slide',
      controlNav: true,
      animationLoop: false,
      slideshow: false,
      itemWidth: 63,
      itemMargin: 5,
      keyboard: true,
      asNavFor: '#slider'
    });
    return $('#slider').flexslider({
      animation: 'slide',
      controlNav: true,
       keyboard: true,
      animationLoop: false,
      slideshow: false,
      sync: '#carousel'
    });
  });
});
