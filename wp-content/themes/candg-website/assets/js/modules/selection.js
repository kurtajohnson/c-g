define(["jquery", "jqueryui"], function($) {

  var tax ="?amenities="//taxonomy name
  var urlString = '/wp_data/book-listing/' + tax;// add taxnomy to the url
  var urlParam = [];//Arry
  
  $(window).on('load',function(){//load page

    var paramStr = decodeURIComponent(window.location.search.substring(1));//get the strings
    var paramSegs = paramStr.split('&');//split the &
    var params = [];//Array
   
    for(var i = 0; i < paramSegs.length; i++) {
      var paramSeg = paramSegs[i].split('=');//seperate =
      params[paramSeg[0]] = paramSeg[1];//joing taxonomy and Array
    console.log(paramSeg[0]);
    console.log(paramSeg[1]);
    }
    
    if(paramSeg[0] == 'amenities'){// check if taxonomy is amenites 
    // ====================================================== keeping the class highlighted =======================================================================
      var paramsval = paramSeg[1].split(',');// Anything that has a comma in the array slit each one

      for (var i = paramsval.length - 1; i >= 0; i--) {//loop through  only the Array
        $('.form_listing[data-amenity="' + paramsval[i] + '"]').addClass('highlight');//Select the data array and select that class and add an addtitonal class
        if (paramsval[i]== ''){
          //do nothing
          window.location.reload(true);
        }else{
          urlParam.push(paramsval[i]);// throw  everything into the URL

        }
        
      }; 
    }

  })//load page END
 //==============================================================================================================================================================================

  //=============== Amenities List Items - Click Event ================================

    $('.large_form-container').on('click', '.form_listing', function(event) {
      event.preventDefault();//prevents the normal function
      var getThisTerm = $(this).data('amenity');// grab the data added in the html called amenityswith the name of the 

      if ($(this).hasClass('highlight')) {//does it have the class condition

        $(this).removeClass('highlight');// if it does remove it

        urlParam.splice( $.inArray(getThisTerm, urlParam), 1 );

      } else {

        $(this).addClass('highlight');
        urlParam.push(getThisTerm);
      };

    });

  //=============== SHOW LISTINGS - Click Event ================================

  $('body').on("click", '#book-it-btn', function(e) {
    e.preventDefault();
    console.log("book it button");
    var urlParamString = "";

    $.each(urlParam, function( index, value ) {

      if (index === 0 ){
        urlParamString += value;
      } else {
        urlParamString += ',' + value;
      }

      var checkInDATE = '&checkin=';// check in
      var checkOutDATE = '&checkout=';//check out
      var numGuest = '&guest=';//guest
      var valueIn = $('#checkin').val();//grab value of check in date
      var valueOut = $('#checkout').val();//grab value of check out date
      var totalG = $('#guests').val();//grab total value of guest
      //+ checkInDATE + valueIn + checkOutDATE + valueOut + numGuest + totalG
      var finalOutput = "http://" + location.host + urlString + urlParamString;//http://www.localhost/wp_data/book-listing/?amenities=(urlParamString for amenities only )&checkin=(nothing yet)&checkout=(nothing yet)&guest=(nothing yet)

      window.location.href = finalOutput;


    });

  });

});

  // var checkInDATE = '&checkin=';// check in
  // var checkOutDATE = '&checkout=';//check out
  // var numGuest = '&guest=';//guest
  // var valueIn = $('body #checkin').val();//grab value of check in date
  // var valueOut = $('body #checkout').val();//grab value of check out date
  // var totalG = $('body #guests').val();//grab total value of guest
  // checkInDATE + valueIn + checkOutDATE + valueOut + numGuest + totalG

//keeps grabing the last part of the url which is guest and what ever number  I need it to grab amenities and the array of elements