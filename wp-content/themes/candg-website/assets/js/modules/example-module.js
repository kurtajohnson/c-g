define(["jquery"], function($) {
  var ExampleModule;
  return ExampleModule = (function() {
    function ExampleModule(options) {
      this.options = options;
      this.delegateEvents();
    }

    ExampleModule.prototype.delegateEvents = function() {
      var $element, _self;
      $element = this.options.$element;
      _self = this;
      $element.off().on("click", ".item-block", function(event) {
        return _self.exampleMethod();
      });
      return $element.off().on("click", ".item-block", (function(_this) {
        return function(event) {
          return _this.exampleMethod();
        };
      })(this));
    };

    ExampleModule.prototype.exampleMethod = function() {
      return this.options.exVar1 + this.options.exVar2;
    };

    return ExampleModule;

  })();
});
