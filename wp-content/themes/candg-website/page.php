<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

</div>
<?php if (has_post_thumbnail( $post->ID ) ): ?>
<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
  <div id="featureCallout" style="background-image: url(<?php echo $image[0]; ?>);" class="feature-callout-renters__mini">
    <div class="container"></div>
  </div>
  <?php endif; ?>
  <!-- END FEATURE CALLOUT BANNER-->
  <!-- START : COLUMS-->
  <section class="section-lightblue borderline">
    <div class="container">
      <div class="section__column">
        <div class="master__center">
          <div class="menu-icon"><a href="#" class="nav-toggle"><span></span></a></div>
        </div>

            <?php 
            $defaults = array(
              'theme_location'  => '',
              'menu'            => 'sidebar',
              'container'       => 'div',
              'container_class' => '',
              'container_id'    => '',
              'menu_class'      => 'menu',
              'menu_id'         => '',
              'echo'            => true,
              'fallback_cb'     => 'wp_page_menu',
              'before'          => '',
              'after'           => '',
              'link_before'     => '',
              'link_after'      => '',
              'items_wrap'      => '<ul id="%1$s" class="section__appform">%3$s</ul>',
              'depth'           => 0,
              'walker'          => ''
            );

            wp_nav_menu( $defaults ); ?> 


        <!-- End naviagtion menue -->
      </div>
      <div class="section__column">
        <!-- Aplication from -->
        <h1 class="section__title-form"><?php the_title(); ?></h1>
        <div class="column__container">
        	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
          <?php the_content(); ?>
      <?php endwhile; ?>
  <?php endif; ?>

        </div>
      </div>
    </div>
  </section>
  <!-- END : Colums-->
  <!-- END : PAGE CONTENT-->
<!-- Footer goes here-->

<?php get_footer(); ?>
