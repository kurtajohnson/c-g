<?php
/**
 * Template Name: Book listing
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

  <!-- START : PAGE CONTENT-->
  <div id="featureCallout" style="background-image: url(<?php bloginfo('template_directory'); ?>/assets/images/featured-renters-callout-mini.jpg);" class="feature-callout-renters__mini undefined">
    <div class="container"></div>
  </div>
  <!-- END FEATURE CALLOUT BANNER-->


<!-- ===================================================  URL  =================================================================== -->
<?php 

  $protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === FALSE ? 'http' : 'https';//http
  $host     = $_SERVER['HTTP_HOST'];// www.demo.com
  $script   = $_SERVER['REQUEST_URI'];// /test/myscript.php?city=Dallas,florida&start=1,2,3
  
 
  $currentUrl = $protocol . '://' . $host . $script;// get the current page URL of the page

  if(isset($_GET['amenities'])) {
    $amens = $_GET['amenities'];

    $amenTermsArray = explode ("," , $amens);

    if ($amens == '') {
        $amens = get_terms('amenities');
        // convert array of term objects to array of term IDs
        $amenTermsArray = wp_list_pluck( $amens, 'slug' );

        $x = "EXISTS"; //show all
      } else {

        $x = "AND"; //show only what i pick
      }
    } else {
        $amens = get_terms('amenities');
        // convert array of term objects to array of term IDs
        $amenTermsArray = wp_list_pluck( $amens, 'slug' );

        $x = "EXISTS"; //show all

  }



?>
<!-- ======================================================/ URL ================================================================ -->

  <!-- START : section-colum-search-->
  <section class="section-lightblue borderline" style="height:auto;">
    <div class="container">
      <div class="section__column">
        <div class="hero_container">
          <div class="hero_label">DATES</div>
          <div class="mini_form-">
            <div class="mini_form">
              <label class="mini_form__label check">Check-In</label>
              <input type="text" id="checkin" name="checkin" value="mm/dd/yy" class="mini_form__input"> </div>
            <div class="mini_form">
              <label class="mini_form__label">Check-Out</label>
              <input type="text" id="checkout" name="checkout" value="mm/dd/yy" class="mini_form__input"> </div>
            <div class="mini_form">
              <label class="mini_form__label guest">Guests</label>
              <div data-reactid=".1.0.0.$=14:0.1" class="select-number">
                <select id="guests" name="guests" data-reactid=".1.0.0.$=14:0.1.0">
                  <option value="1">1 </option>
                  <option value="2">2 </option>
                  <option value="3">3 </option>
                  <option value="4">4 </option>
                  <option value="5">5 </option>
                  <option value="6">6 </option>
                  <option value="7">7 </option>
                  <option value="8">8 </option>
                  <option value="9">9 </option>
                  <option value="10">10 </option>
                  <option value="11">11 </option>
                  <option value="12">12 </option>
                  <option value="13">13 </option>
                  <option value="14">14 </option>
                  <option value="15">15 </option>
                  <option value="16">16+ </option>
                </select>
              </div>
            </div>
          </div>
          <div class="hero_label">AMENITIES</div>
          <!-- ======================================================All Amenites -->
          <ul class="large_form-container">
            <?php

              // your taxonomy name
              $tax = 'amenities';
              // get the terms of taxonomy
              $terms = get_terms( $tax, $args = array('hide_empty' => true,));

              // loop through all terms
              foreach( $terms as $term ) {
            
              // Add the amenitys
               echo '<li class="form_listing" data-amenity="' . $term->slug . '"><a href="' . get_term_link($term) . '">' . $term->name . '</a></li>';
              } 

            ?>
          </ul>
           <!-- ======================================================/ All Amenites -->
          <div class="hero_label">
            <div class="mini_form_btn"><a href="<?php echo $currentUrl; ?>" id="book-it-btn">SHOW LISTINGS</a></div>
          </div>
        </div>
      </div>
      <div class="section__column">
        <div class="booklisting-wholecontainer">
          <!-- ========================================================All My properties -->
        <h3 class="column__title">Property Search</h3>
        <?php

           $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
           $args = array(
            'post_type'      => 'properties',
            'posts_per_page' => 8,
            'paged'          => $paged,
            'current'        => 0,
            'total'          => '',
            'tax_query'      => array(

                                'relation' => 'AND',
//relation (string) - The logical relationship between each inner taxonomy array when there is more than one. Possible values are 'AND', 'OR'. Do not use with a single inner taxonomy array.
                                  array(
                                     //taxonomy (string) - Taxonomy.
                                      'taxonomy' => 'amenities',
//field (string) - Select taxonomy term by. Possible values are 'term_id', 'name' and 'slug'. Default value is 'term_id'.
                                      'field'    => 'slug',
                                    // terms (int/string/array) - Taxonomy term(s).
                                      'terms'    => $amenTermsArray,
//operator (string) - Operator to test. Possible values are 'IN', 'NOT IN', 'AND', 'EXISTS' and 'NOT EXISTS'. Default value is 'IN'. 
                                      'operator' => $x,//only show those amenities I clicked on
                                    ),
                                   array(
                                     //taxonomy (string) - Taxonomy.
                                      'taxonomy' => 'amenities',
//field (string) - Select taxonomy term by. Possible values are 'term_id', 'name' and 'slug'. Default value is 'term_id'.
                                      'field'    => 'slug',
                                    // terms (int/string/array) - Taxonomy term(s).
                                      'terms'    => $amenTermsArray,
//operator (string) - Operator to test. Possible values are 'IN', 'NOT IN', 'AND', 'EXISTS' and 'NOT EXISTS'. Default value is 'IN'. 
                                      'operator' => 'IN',    
                                    )

                                    )
           );

          

           query_posts($args); 

           ?>

          
           <!-- ========================================================/ All My properties -->
        <div class="pages">
          <!-- ======================================================== Pagination  -->
          <?php $current_page = max( 1, get_query_var('paged') ); ?>
            <div class="current__pages"> <?php echo  $current_page; ?> - <?php echo $wp_query->max_num_pages;?>  of <?php echo $wp_query->found_posts;?> Rentals</div>
          <ul class="navbar__pages">
          <!-- Costume Pagination here -->
          <?php the_posts_pagination( array('mid_size'  => 4,'prev_text' => '<img src="' . get_bloginfo('stylesheet_directory') . '../assets/images/arrow-prev.png' . '">', 'next_text' =>'<img src="' . get_bloginfo('stylesheet_directory') . '../assets/images/arrow-next.png' . '">',) ); ?>
          </ul>
          <!-- ========================================================/ Pagination  -->
        </div>
        <div class="booking-items">
          <ul class="callout-list">
            <!-- ========================================================/ Post   -->
           <?php
           
            if ( have_posts() ) :
                while ( have_posts() ) : the_post(); ?>
                  <?php if ( has_post_thumbnail() ) { ?>
                  <!-- =====================================================first Image of the slider -->
                  <?php } ?>
                     <?php if( have_rows('slider') ):
                      // vars
                      $main_field = get_field('slider');
                      $first_img = $main_field[0]['image']['url'];
                    ?>
                    <!-- =====================================================/ first Image of the slider -->
                  <li class="callout-list__item-list">
                    <a href="<?php the_permalink(); ?>" class="callout-list__item__anchor"><img src="<?php echo $first_img; ?>" alt="" class="callout-booking-img">
                      <?php endif; ?>

                      <?php

                      $value = get_field( "property_day_rate_us" ); //Check if value exists

                      if( $value ) {
                      ?>
                      <div class="callout-list__item-list__price">$<?php echo $value; ?></div><!-- Put value here -->

                      <!-- End of checking for value exists -->

                      <?php } ?>

                      <div class="callout__list-item_name"><?php echo get_the_title(); ?></div><!-- Add title of post -->
                      <div class="callout__list-item_discription"><?php echo get_field('property_bedrooms'); ?> Bedroom / <?php echo get_field('property_bathrooms'); ?> Bath <?php echo get_field('property_floors'); ?> Storey / <?php echo get_field('property_square_footage'); ?> sq. ft.</div><!-- add all of these feilds -->
                    </a>
                  </li>
                <?php endwhile;
            endif;
            wp_reset_postdata();
        ?>

        </ul>
        <!-- ========================================================/ Post   -->

        </div>
        <div class="pages">
          <!-- ======================================================== Pagination  -->
          <?php $current_page = max( 1, get_query_var('paged') ); ?>

            <div class="current__pages"> <?php echo  $current_page; ?> - <?php echo $wp_query->max_num_pages;?>  of <?php echo $wp_query->found_posts;?> Rentals</div>
          <ul class="navbar__pages">
           <?php the_posts_pagination( array('mid_size'  => 4,'prev_text' => '<img src="' . get_bloginfo('stylesheet_directory') . '../assets/images/arrow-prev.png' . '">', 'next_text' =>'<img src="' . get_bloginfo('stylesheet_directory') . '../assets/images/arrow-next.png' . '">',) ); ?>
           <!-- ========================================================/ Pagination  -->
          </ul>
        </div>
       </div>
      </div>
    </div>
  </section>
  <!-- END FEATURE CALLOUT-->
  <!-- END : PAGE CONTENT-->
<!-- Footer goes here-->
<?php get_footer(); ?>
