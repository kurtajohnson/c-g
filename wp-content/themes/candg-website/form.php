<?php
/**
  *Template Name: Form
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

  <!-- START : PAGE CONTENT--> 
  <?php if( get_field('cover_image') ): ?>
  <div id="featureCallout" style="background-image: url(<?php the_field('cover_image'); ?>);" class="feature-callout-renters__mini undefined">
    <?php endif; ?>
    <div class="container"></div>
  </div>
  <!-- END FEATURE CALLOUT BANNER-->
  <!-- START : COLUMS-->
  <section class="section-lightblue borderline">
    <div class="container">
      <div class="section__column">
        <div class="master__center">
          <div class="menu-icon"><a href="#" class="nav-toggle"><span></span></a></div>
        </div>
        <!-- Nav menue -->
       <!--  <?php if(have_posts()) : ?>
        <ul class="section__appform">
        <?php while(have_posts()) : the_post(); 
        $name = get_field('page_name');
        $link = get_field('link');
        ?>
        <li class="callout-form__item__anchor"> <a href="<?php echo $link; ?>" class="callout-form__item__anchor active"><?php echo $name; ?></a></li>
        <?php endwhile; ?>
        <?php else : ?>
        <li class="callout-form__item__anchor"> <a href="<?php echo $link; ?>" class="callout-form__item__anchor"><?php echo $name; ?></a></li>
        <?php endif; ?> -->

        <ul class="section__appform">
          <li class="callout-form__item__anchor"><a href="<?php the_field('link_1'); ?>" class="callout-form__item__anchor active">Application Form</a></li>
          <li class="callout-form__item__anchor"><a href="<?php the_field('link_2'); ?>" class="callout-form__item__anchor">Need Help?</a></li>
          <li class="callout-form__item__anchor"><a href="<?php the_field('link_3'); ?>" class="callout-form__item__anchor">How It Works</a></li>
          <li class="callout-form__item__anchor"><a href="<?php the_field('link_4'); ?>" class="callout-form__item__anchor">Client Rentals</a></li>
          <li class="callout-form__item__anchor"><a href="<?php the_field('link_5'); ?>" class="callout-form__item__anchor">Homeowner Services</a></li>
          <li class="callout-form__item__anchor"><a href="<?php the_field('link_6'); ?>" class="callout-form__item__anchor">Why C&amp;G is the Right Choice</a></li>
          <li class="callout-form__item__anchor"><a href="<?php the_field('link_7'); ?>" class="callout-form__item__anchor">Fees &amp; Charges</a></li>
          <li class="callout-form__item__anchor"><a href="<?php the_field('link_8'); ?>" class="callout-form__item__anchor">About C&amp;G</a></li>
        </ul>


        <!-- End naviagtion menue -->
      </div>
      <div class="section__column">
        <!-- Aplication from -->
        <div class="section__title-form"><?php the_field('form_title'); ?></div>
        <div class="column__container">
          <?php the_content(); ?>
        </div>
      </div>
    </div>
  </section>
  <!-- END : Colums-->
  <!-- END : PAGE CONTENT-->
<!-- Footer goes here-->
<?php get_footer(); ?>