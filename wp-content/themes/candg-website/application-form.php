<?php
/**
  *Template Name: Application Form
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

  <!-- START : PAGE CONTENT-->
  <?php if( get_field('cover_image') ): ?>
  <div id="featureCallout" style="background-image: url(<?php the_field('cover_image'); ?>);" class="feature-callout-renters__mini undefined">
    <?php endif; ?>
    <div class="container"></div>
  </div>
  <!-- END FEATURE CALLOUT BANNER-->
  <!-- START : COLUMS-->
  <section class="section-lightblue borderline">
    <div class="container">
      <div class="section__column">
        <div class="master__center">
          <div class="menu-icon"><a href="#" class="nav-toggle"><span></span></a></div>
        </div>
        <!-- Nav menue -->

        
        <ul class="section__appform">
          <li class="callout-form__item__anchor"><a href="<?php the_field('link_1'); ?>" class="callout-form__item__anchor active">Application Form</a></li>
          <li class="callout-form__item__anchor"><a href="<?php the_field('link_2'); ?>" class="callout-form__item__anchor">Need Help?</a></li>
          <li class="callout-form__item__anchor"><a href="<?php the_field('link_3'); ?>" class="callout-form__item__anchor">How It Works</a></li>
          <li class="callout-form__item__anchor"><a href="<?php the_field('link_4'); ?>" class="callout-form__item__anchor">Client Rentals</a></li>
          <li class="callout-form__item__anchor"><a href="<?php the_field('link_5'); ?>" class="callout-form__item__anchor">Homeowner Services</a></li>
          <li class="callout-form__item__anchor"><a href="<?php the_field('link_6'); ?>" class="callout-form__item__anchor">Why C&amp;G is the Right Choice</a></li>
          <li class="callout-form__item__anchor"><a href="<?php the_field('link_7'); ?>" class="callout-form__item__anchor">Fees &amp; Charges</a></li>
          <li class="callout-form__item__anchor"><a href="<?php the_field('link_8'); ?>" class="callout-form__item__anchor">About C&amp;G</a></li>
        </ul>

        <!-- End naviagtion menue -->
      </div>
      <div class="section__column">
        <!-- Aplication from -->
        <div class="section__title-form"><?php the_field('form_title'); ?></div>
        <div class="column__container">
          <div class="pma__title">Rent Your Property Today!</div>
          <div class="pma__sub">Complete the application below to start turning your home into a real investment!</div>
          <form id="applicationForm">
            <div class="pma__m-box">
              <div class="pma__lable">First Name*</div>
              <div class="pma__input">
                <input type="text" name="firstname" minlength="2" id="firstname" class="required"> </div>
            </div>
            <div class="pma__m-box">
              <div class="pma__lable">Last Name*</div>
              <div class="pma__input">
                <input type="text" name="lastname" minlength="2" id="lastname" class="required"> </div>
            </div>
            <div class="pma__m-box">
              <div class="pma__lable">Email Address*</div>
              <div class="pma__input">
                <input type="text" name="email" id="email" class="required"> </div>
            </div>
            <div class="pma__m-box">
              <div class="pma__lable">Confirm Email*</div>
              <div class="pma__input">
                <input type="text" name="cemail" id="cemail" class="required"> </div>
            </div>
            <div class="pma__box">
              <div class="pma__lable">Property Address*</div>
              <div class="pma__input">
                <input type="text" name="address" id="address" class="required"> </div>
            </div>
            <div></div>
            <div class="pma__m-box">
              <div class="pma__lable">City*</div>
              <div class="pma__input">
                <input type="City" name="city" id="city" class="required"> </div>
            </div>
            <div class="pma__m-box">
              <div class="pma__lable">State</div>
              <div class="pma__input">
                <select name="ST">
                  <option value="Alabama" class="lo">Alabama</option>
                  <option value="Alaska">Alaska</option>
                  <option value="Arizona">Arizona</option>
                  <option value="Arkansas">Arkansas</option>
                  <option value="California">California</option>
                  <option value="Colorado">Colorado</option>
                  <option value="Connecticut">Connecticut</option>
                  <option value="Delaware">Delaware</option>
                  <option value="District of Columbia">District of Columbia</option>
                  <option selected="Florida" value="Florida">Florida</option>
                  <option value="Georgia">Georgia</option>
                  <option value="Hawaii">Hawaii</option>
                  <option value="Idaho">Idaho</option>
                  <option value="Illinois">Illinois</option>
                  <option value="Indiana">Indiana</option>
                  <option value="Iowa">Iowa</option>
                  <option value="Kansas">Kansas</option>
                  <option value="Kentucky">Kentucky</option>
                  <option value="Louisiana">Louisiana</option>
                  <option value="Maine">Maine</option>
                  <option value="Maryland">Maryland</option>
                  <option value="Massachusetts">Massachusetts</option>
                  <option value="Michigan">Michigan</option>
                  <option value="Minnesota">Minnesota</option>
                  <option value="Mississippi">Mississippi</option>
                  <option value="Missouri">Missouri</option>
                  <option value="Montana">Montana</option>
                  <option value="Nebraska">Nebraska</option>
                  <option value="Nevada">Nevada</option>
                  <option value="New Hampshire">New Hampshire</option>
                  <option value="New Jersey">New Jersey</option>
                  <option value="New Mexico">New Mexico</option>
                  <option value="New York">New York</option>
                  <option value="North Carolina">North Carolina</option>
                  <option value="North Dakota">North Dakota</option>
                  <option value="Ohio">Ohio</option>
                  <option value="Oklahoma">Oklahoma</option>
                  <option value="Oregon">Oregon</option>
                  <option value="Pennsylvania">Pennsylvania</option>
                  <option value="Rhode Island">Rhode Island</option>
                  <option value="South Carolina">South Carolina</option>
                  <option value="South Dakota">South Dakota</option>
                  <option value="Tennessee">Tennessee</option>
                  <option value="Texas">Texas</option>
                  <option value="Utah">Utah</option>
                  <option value="Vermont">Vermont</option>
                  <option value="Virginia">Virginia</option>
                  <option value="Washington">Washington</option>
                  <option value="West Virginia">West Virginia</option>
                  <option value="Wisconsin">Wisconsin</option>
                  <option value="Wyoming">Wyoming</option>
                </select>
              </div>
            </div>
            <div class="pma__m-box">
              <div class="pma__lable">Zipcode*</div>
              <div class="pma__input">
                <input type="text" name="zipcode" id="zipcode" class="required"> </div>
            </div>
            <div class="pma__m-box">
              <div class="pma__lable">Phone Number*</div>
              <div class="pma__input">
                <input type="text" name="phone" id="phone" class="required"> </div>
            </div>
            <div class="pma_s-box_container">
              <div class="pma__s-box">
                <div class="pma__lable">Furnished?</div>
                <div class="pma__select">
                  <select id="furnished" name="furnished">
                    <option value="Yes">No </option>
                    <option value="No">Yes </option>
                  </select>
                </div>
              </div>
              <div class="pma__s-box">
                <div class="pma__lable"># of Bedrooms</div>
                <div class="pma__select">
                  <select id="bedrooms" name="bedrooms">
                    <option value="1">1 </option>
                    <option value="2">2 </option>
                    <option value="3">3 </option>
                    <option value="4">4 </option>
                    <option value="5">5</option>
                  </select>
                </div>
              </div>
              <div class="pma__s-box">
                <div class="pma__lable"># of Bathrooms</div>
                <div class="pma__select">
                  <select id="bathrooms" name="bathrooms">
                    <option value="1">1 </option>
                    <option value="2">2 </option>
                    <option value="3">3 </option>
                    <option value="4">4 </option>
                    <option value="5">5</option>
                  </select>
                </div>
              </div>
              <div class="pma__s-box">
                <div class="pma__lable"># of Masters</div>
                <div class="pma__select">
                  <select id="masters" name="masters">
                    <option value="1">1 </option>
                    <option value="2">2 </option>
                    <option value="3">3 </option>
                    <option value="4">4 </option>
                    <option value="5">5</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="pma__lable-l">
              <input type="submit" id="submit" value="SUBMIT APPLICATION"> </div>
          </form>
          <!-- End of Application from -->

        </div>
      </div>
    </div>
  </section>
  <!-- END : Colums-->
  <!-- END : PAGE CONTENT-->
<!-- Footer goes here-->
<?php get_footer(); ?>