<?php
/**
 * Template Name: Book listing
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

  <!-- START : PAGE CONTENT-->
  <div id="featureCallout" style="background-image: url(<?php bloginfo('template_url'); ?>/assets/images/featured-renters-callout-mini.jpg);" class="feature-callout-renters__mini">
    <div class="container"></div>
  </div>
  <!-- END FEATURE CALLOUT BANNER-->
  <!-- START : section-colum-search-->
  <section class="section-lightblue borderline">
    <div class="container">
      <div class="section__column">
        <div class="hero_container">
          <div class="hero_label">DATES</div>
          <div class="mini_form-">
            <div class="mini_form">
              <label class="mini_form__label check">Check-In</label>
              <input type="text" id="checkin" name="checkin" value="mm/dd/yy" class="mini_form__input"> </div>
            <div class="mini_form">
              <label class="mini_form__label">Check-Out</label>
              <input type="text" id="checkout" name="checkout" value="mm/dd/yy" class="mini_form__input"> </div>
            <div class="mini_form">
              <label class="mini_form__label guest">Guests</label>
              <div data-reactid=".1.0.0.$=14:0.1" class="select-number">
                <select id="guests" name="guests" data-reactid=".1.0.0.$=14:0.1.0">
                  <option value="1">1 </option>
                  <option value="2">2 </option>
                  <option value="3">3 </option>
                  <option value="4">4 </option>
                  <option value="5">5 </option>
                  <option value="6">6 </option>
                  <option value="7">7 </option>
                  <option value="8">8 </option>
                  <option value="9">9 </option>
                  <option value="10">10 </option>
                  <option value="11">11 </option>
                  <option value="12">12 </option>
                  <option value="13">13 </option>
                  <option value="14">14 </option>
                  <option value="15">15 </option>
                  <option value="16">16+ </option>
                </select>
              </div>
            </div>
          </div>
          <div class="hero_label">AMENITIES</div>
          <ul class="large_form-container">
            <?php
              // your taxonomy name
              $tax = 'amenities';
              // get the terms of taxonomy
              $terms = get_terms( $tax, $args = array('hide_empty' => true,));

              // loop through all terms
              foreach( $terms as $term ) {
                if( has_term($term->name,$tax ) )
                    // display term archive
                  echo '<li class="form_listing"><a href="#/" id="">' . $term->name . '</a></li>';
                  
                else
                // cross out all unselected terms in archive
                  echo '<li class="form_listing"><a href="#/" id="">' . $term->name . '</a></li>';
              } 
            ?>
          </ul>
          <div class="hero_label">
            <div class="mini_form_btn"><a href="#/" id="book-it-btn">SHOW LISTINGS</a></div>
          </div>
        </div>
      </div>
      <div class="section__column">
        <h3 class="column__title">Property Search</h3>
        <div class="pages">
          <div class="current__pages">1 - 8 of 40 Rentals</div>
          <ul class="navbar__pages">
            <li class="navbar__pages-list"><a href="#" class="navbar__pages-item active">1</a></li>
            <li class="navbar__pages-list"><a href="#" class="navbar__pages-item"> 2</a></li>
            <li class="navbar__pages-list"><a href="#" class="navbar__pages-item"> 3</a></li>
            <li class="navbar__pages-list"><a href="#" class="navbar__pages-item"> 4</a></li>
            <li class="navbar__pages-list">
              <a href="#" class="navbar__pages-item">
                <div class="navbar__pages-arrow"></div>
              </a>
            </li>
          </ul>
        </div>
        <div class="booking-items">
          <ul class="callout-list"></ul>

           <?php
            $loop = new WP_Query( array( 'post_type' => 'properties' ) );
            if ( $loop->have_posts() ) :
                while ( $loop->have_posts() ) : $loop->the_post(); ?>
                  <?php if ( has_post_thumbnail() ) { ?>

                  <?php } ?>
                     <?php if( have_rows('slider') ):
                      // vars
                      $main_field = get_field('slider');
                      $first_img = $main_field[0]['image']['url'];
                    ?>
                  <li class="callout-list__item-list">
                    <a href="<?php the_permalink(); ?>" class="callout-list__item__anchor"><img src="<?php echo $first_img; ?>" alt="" class="callout-booking-img">
                      <?php endif; ?>
                      <div class="callout-list__item-list__price">$<?php echo get_field('property_day_rate_us'); ?></div>
                      <div class="callout__list-item_name"><?php echo get_the_title(); ?></div>
                      <div class="callout__list-item_discription"><?php echo get_field('property_bedrooms'); ?> Bedroom / <?php echo get_field('property_bathrooms'); ?> Bath <?php echo get_field('property_floors'); ?> Storey / <?php echo get_field('property_square_footage'); ?> sq. ft.</div>
                    </a>
                  </li>
                <?php endwhile;
                if (  $loop->max_num_pages > 1 ) : ?>
                    <div id="nav-below" class="navigation">
                        <div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Previous', 'domain' ) ); ?></div>
                        <div class="nav-next"><?php previous_posts_link( __( 'Next <span class="meta-nav">&rarr;</span>', 'domain' ) ); ?></div>
                    </div>
                <?php endif;
            endif;
            wp_reset_postdata();
        ?>

          



        </div>
        <div class="pages">
          <div class="current__pages">1 - 8 of 40 Rentals</div>
          <ul class="navbar__pages">
            <li class="navbar__pages-list"><a href="#" class="navbar__pages-item active">1</a></li>
            <li class="navbar__pages-list"><a href="#" class="navbar__pages-item"> 2</a></li>
            <li class="navbar__pages-list"><a href="#" class="navbar__pages-item"> 3</a></li>
            <li class="navbar__pages-list"><a href="#" class="navbar__pages-item"> 4</a></li>
            <li class="navbar__pages-list">
              <a href="#" class="navbar__pages-item">
                <div class="navbar__pages-arrow"></div>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <!-- END FEATURE CALLOUT-->
  <!-- END : PAGE CONTENT-->
<!-- Footer goes here-->
<?php get_footer(); ?>
