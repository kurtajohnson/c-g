<?php
/**
  *Template Name: Property Management Services
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<!-- END HEADER-->
 
  <!-- START : PAGE CONTENT-->
  <!-- START FEATURE CALLOUT-->
 <?php if( get_field('hero_image') ): ?>
  <div id="featureCallout" style="background-image: url(<?php the_field('hero_image'); ?>);" class="feature-callout undefined">
   <?php endif; ?>
    <div class="container">
      <h2 class="feature-callout__initial-header"><?php the_field('hero_thick_title'); ?></h2>
      <h2 class="feature-callout__final-header"><?php the_field('hero_thin_title'); ?></h2><a href="<?php the_field('hero_button_link'); ?>" class="button-callout"><?php the_field('hero_button_text'); ?></a> 
      <?php
      $value = get_field( "feature-callout-subtext" );

if( $value ) {
      ?>
      <div class="feature-callout-text">or <span><a href="<?php the_field('feature-callout-sublink'); ?>" class='feature-callout-sublink'><?php echo $value ?></a></span></div>
     <?php
      }
      ?>
    </div>
  </div>
  <!-- END FEATURE CALLOUT-->
  <!-- START : TRIPLE CALLOUT-->
  <section class="section">
    <div class="container">
      <header class="section__header">
        <h3 class="section__main-title"><?php the_field('section_bold_title'); ?></h3>
        <h4 class="section__sub-title"><?php the_field('section_thin_title'); ?></h4> </header>
        <?php if( have_rows('callout') ): ?>
          <ul class="callout-list">
            <?php while( have_rows('callout') ): the_row(); 
                // vars
                $image = get_sub_field('callout_main_image');
                $title = get_sub_field('callout_bold_text');
                $des = get_sub_field('callout_description');
            ?>
                <li class="callout-list__item">
                 <a href="#/" data-featherlight="&lt;h3&gt;<?php echo $title; ?>&lt;/h3&gt;&lt;p&gt;<?php echo $des; ?> &lt;/p&gt;"
                  class="callout-list__item__anchor"><img src="<?php echo $image['url']; ?>" alt=""><span class="callout-list__item__title"><?php echo $title; ?></span></a>
            <?php endwhile; ?>
          </ul>
        <?php endif; ?>
      </ul>
    </div>
  </section>
  <!-- END : TRIPLE CALLOUT-->
  <!-- START FEATURE CALLOUT-->
  <?php if( get_field('callout_background_image') ): ?>
  <div id="featureCallout" style="background-image: url(<?php the_field('callout_background_image'); ?>);" class="feature-callout feature-callout--mini">
     <?php endif; ?>
    <div class="container">
      <h2 class="feature-callout__initial-header"><?php the_field('callout_title'); ?></h2>
      <h2 class="feature-callout__final-header"><?php the_field('callout_subtitle'); ?></h2><a href="<?php the_field('callout_button_link'); ?>#find_more" class="button"><?php the_field('callout_button_text'); ?></a> </div>
  </div>
  <!-- END FEATURE CALLOUT-->
  <!-- END : PAGE CONTENT-->
<!-- Footer goes here-->
<?php get_footer(); ?>