<?php
/**
  *Template Name: Properties
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<?php
    // Start the loop.
    while ( have_posts() ) : the_post(); ?>

<div class="feature-callout-renters__banner">
   <?php if( have_rows('slider') ):
    // vars
    $main_field = get_field('slider');
    $first_img = $main_field[0]['image']['url'];
    ?>
    <a id="featureCallout" style="background-image: url('<?php echo $first_img; ?>');" href="#" class="feature-callout_img">

    <?php endif; ?>
      <div class="openicon"><img src="<?php bloginfo('template_directory'); ?>/assets/images/openicon.png"></div>
    </a>
    <div class="container_div_bottom">
      <div class="pricing__container">
        <?php if ( get_field('property_day_rate_us') ): ?>
          <div class="banner__pricing">$<?php the_field('property_day_rate_us'); ?> <span class="span-perweek">per day</span></div>
        <?php endif; ?>
        <?php if ( get_field('property_weekly_rate_us') ): ?>
          <div class="banner__pricing">$<?php the_field('property_weekly_rate_us'); ?> <span class="span-perweek">per week</span></div>
        <?php endif; ?>
        <?php if ( get_field('property_weekly_rate_uk') ): ?>
          <div class="banner__pricing">&#163;<?php the_field('property_weekly_rate_uk'); ?><span class="span-perweek">per week</span></div>
        <?php endif; ?>
      </div>
      <div class="banner__container">
        <div class="container">
          <div class="banner__contant-title"><?php the_title(); ?></div> <address class="banner__contant-address"><?php the_field('property_location'); ?></address>
          <ul class="banner__contant">
            <li class="banner__contant-list">
              <div class="banner__svg">
                <svg role="img" title="Play">
                  <use xlink:href="<?php bloginfo('template_directory'); ?>/assets/images/sprite.svg#multiple25"></use>
                </svg>
              </div><span class="banner__label"><?php the_field('property_guests'); ?> Guests</span> </li>
            <li class="banner__contant-list">
              <div class="banner__svg">
                <svg role="img" title="Play">
                  <use xlink:href="<?php bloginfo('template_directory'); ?>/assets/images/sprite.svg#open203"></use>
                </svg>
              </div><span class="banner__label"><?php the_field('property_bedrooms'); ?> Bedrooms  </span> </li>
            <li class="banner__contant-list">
              <div class="banner__svg">
                <svg role="img" title="Play">
                  <use xlink:href="<?php bloginfo('template_directory'); ?>/assets/images/sprite.svg#bathtub3"></use>
                </svg>
              </div><span class="banner__label"><?php the_field('property_bath'); ?> Bathrooms</span> </li>
            <li class="banner__contant-list">
              <div class="banner__svg">
                <svg role="img" title="Play">
                  <use xlink:href="<?php bloginfo('template_directory'); ?>/assets/images/sprite.svg#ascending8"></use>
                </svg>
              </div><span class="banner__label"><?php the_field('property_floors'); ?> Floors</span> </li>

            <!-- user selects pool from the amenities show otherwise do not show -->
            <?php if ( has_term( 'pool', 'amenities') ) { ?> <!-- php condition-->
            <!-- normal html -->
              <li class="banner__contant-list">
                <div class="banner__svg">
                  <svg role="img" title="Play">
                    <use xlink:href="<?php bloginfo('template_directory'); ?>/assets/images/sprite.svg#stick-man1"></use>
                  </svg>
                </div><span class="banner__label">Pool</span>
              </li>
              <!-- normal html -->
            <?php } ?>
            <!-- closing php condition -->
            
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div id="hiddengallery">
    <div class="overlay">
      <div id="viewer">
        <div id="slider" class="flexslider">
          <?php if( have_rows('slider') ): ?>
          <ul class="slides">
            <?php while( have_rows('slider') ): the_row(); 
                // vars
                $image = get_sub_field('image');
                ?>
                <!-- TODO: only need the background url, the rest of the CSS needs to be in the stylesheet -->
                <li><div style="background: no-repeat center; background-size: contain; height: 100%; width: 100%; background-image: url('<?php echo $image['url']; ?>');"></div></li>

                <?php endwhile; ?>
                </ul>
                <?php endif; ?>
          <a href="#" id="close"></a>
        </div>
        <div id="carousel" class="flexslider">
           <?php if( have_rows('slider') ): ?>
          <ul class="slides">
            <?php while( have_rows('slider') ): the_row(); 
                // vars
                $image = get_sub_field('image');
                ?>
                <li><img src="<?php echo $image['url']; ?>" class="img-slider"> /></li>

                <?php endwhile; ?>
                </ul>
                <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
  <section class="section-lightblue">
    <div class="container">
      <div class="section__column">
        <div class="mini_form-container">
          <div class="mini_form">
            <label class="mini_form__label check">Check-In</label>
            <input type="text" id="checkin" name="checkin" value="mm/dd/yy" class="mini_form__input"> </div>
          <div class="mini_form">
            <label class="mini_form__label">Check-Out</label>
            <input type="text" id="checkout" name="checkout" value="mm/dd/yy" class="mini_form__input"> </div>
          <div class="mini_form">
            <label class="mini_form__label guest">Guests</label>
            <div data-reactid=".1.0.0.$=14:0.1" class="select-number">
              <select id="guests" name="guests" data-reactid=".1.0.0.$=14:0.1.0">
                <option value="1">1 </option>
                <option value="2">2 </option>
                <option value="3">3 </option>
                <option value="4">4 </option>
                <option value="5">5 </option>
                <option value="6">6 </option>
                <option value="7">7 </option>
                <option value="8">8 </option>
                <option value="9">9 </option>
                <option value="10">10 </option>
                <option value="11">11 </option>
                <option value="12">12 </option>
                <option value="13">13 </option>
                <option value="14">14 </option>
                <option value="15">15 </option>
                <option value="16">16+ </option>
              </select>
            </div>
          </div>
          <div class="mini_form_btn"><a href='<?php bloginfo('wpurl'); ?>/booking-form' data-featherlight="text goes here" id="bookPropertyForm">Request To Book</a></div>
        </div>
        <div style="display: none" class="master__center">
          <div class="menu-icon"><a href="#/" class="nav-toggle"><span></span></a></div>
        </div>
        <div class="section__appform">
          <div class="amenities__title_DT">AMENITIES</div>
          <div class="amenities__title">AMENITIES</div>
          <ul class="amenities__container">
            <!-- taxonomy -->
            <?php
              // your taxonomy name
              $tax = 'amenities';
              // get the terms of taxonomy
              $terms = get_terms( $tax, $args = array('hide_empty' => false,));

              // loop through all terms
              foreach( $terms as $term ) {
                if( has_term($term->name,$tax ) )
                    // display term archive
                  echo '<li class="amenities__list">' . $term->name . '</li>';
                  
              }
            ?>
            <!-- /taxonomy -->
          </ul>
        </div>
      </div>
      <div class="section__column">
        <div class="column__container">
          <?php the_content(); ?>
        </div>
      </div>
    </div>
  </section>

<?php // End the loop.
    endwhile;
    ?>
<!-- Footer goes here-->
<?php get_footer(); ?>