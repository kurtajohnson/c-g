<?php
/** Apartment Slider
  *
  * This file is used to display the apartment slider used
  * in single apartment pages.
  *
  * It displays a large image on top (with slightly smaller,
  * hanging-out images for the next/prev navigation) and a
  * line of thimbnails underneath.
  *
  * The large images should be responsive and the line of
  * thumbnails should break into multiple pages as the
  * window is resized.
  *
  * @package The Vacation Rental
  *
  */

  $image_data = ( !empty( $post->postmeta['apartment_slider_images'] ) ) ? explode( ',', $post->postmeta['apartment_slider_images'] ) : false;

  	if( !empty( $image_data ) AND is_array( $image_data ) AND count( $image_data ) >= 3 ) :

		$images = array();
		while( $image_id = current( $image_data ) ) {
			$large = wp_get_attachment_image_src( $image_id, 'eb_col_1' );
			$small = wp_get_attachment_image_src( $image_id, 'eb_thumb' );
			$images[] = array(
				'large' => $large[0],
				'small' => $small[0],
			);
			next( $image_data );
		}

	?>

		<div id='rfas-container' data-speed='<?php echo $post->postmeta['slider_speed'] ?>' data-transition_speed='<?php echo $post->postmeta['slider_transition_speed'] ?>'>

			<div id='rfas-image-cache'>
				<?php while( $image = current( $images ) ) : ?>
					<img id='rfas-image-cache-<?php echo key( $images) ?>' data-id="<?php echo key( $images ) ?>" src='<?php echo $image['large'] ?>' class='visible'>
				<?php next( $images ); endwhile; reset( $images ) ?>
			</div>


			<div id='rfas-windows'>

				<div id='rfas-prev-container'><div class='box smallpadding visible-full'>
					<div id='rfas-prev' class='image'>
						<div class="shadow"></div>
						<img src='<?php echo $images[count($images) -1]['large'] ?>' class='visible'>
						<img src='' class='invisible'>
					</div>
					<div class="outer-shadow"><div class="left"></div><div class="middle"></div><div class="right"></div></div>
				</div></div>

				<div id='rfas-current-container'><div class='box smallpadding'>
					<div id='rfas-current' class='image'>
						<div class="shadow"></div>
						<img src='<?php echo $images[0]['large'] ?>' class='visible'>
						<img src='' class='invisible'>
					</div>
					<div class="outer-shadow"><div class="left"></div><div class="middle"></div><div class="right"></div></div>
				</div></div>

				<div id='rfas-next-container'><div class='box smallpadding visible-full'>
					<div id='rfas-next' class='image'>
						<div class="shadow"></div>
						<img src='<?php echo $images[1]['large'] ?>' class='visible'>
						<img src='' class='invisible'>
					</div>
					<div class="outer-shadow"><div class="left"></div><div class="middle"></div><div class="right"></div></div>
				</div></div>

			</div>

			<nav id='rfas-nav' class='flexslider'>
				<ul class="slides">
					<?php
						while( $image = current( $images ) ) :
						$current = ( key( $images ) == 0 ) ? 'current' : '';
					?>
					<li id='rfas-thumb-<?php echo key( $images ) ?>' data-id='<?php echo key( $images ) ?>' class='image box smallpadding <?php echo $current ?>'>
						<div class='inner'>
							<img src="<?php echo $image['small'] ?>" />
						</div>
					</li>
					<?php next( $images ); endwhile;  ?>
				</ul>
			</nav>

		</div>
<?php endif ?>