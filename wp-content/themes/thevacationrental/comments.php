<?php
/**
  * Comment Display
  *
  * The area of the page that contains both current comments
  * and the comment form. The actual display of comments is
  * handled by a callback to RF_Framework::show_comment() which is
  * located in the RF_Framework class. If more specific functionality is
  * needed [themename]_show_comment() is used in the functions.php file.
  *
  * @package The Vacation Rental
  *
  */
  global $post, $blueprint, $framework;

?>

<?php if( comments_open() ) : ?>
	<div <?php echo $blueprint->boxed_class( 'comments' ) ?>>

		<?php if ( post_password_required() ) : ?>
			<div class="comments nopassword">
				<hgroup class='notice'>
					<h1><?php echo $framework['options']['password_protected_title'] ?></h1>
					<h2><?php echo $framework['options']['password_protected_message'] ?></h2>
				</hgroup>
			</div>
		<?php return; endif; ?>

		<div class='comments'>

				<?php if ( have_comments() ) : ?>
					<h1 class="comments-title"><?php comments_number( 'No Comments Yet', '1 Comment', '% Comments'  ) ?> on "<?php the_title() ?>"</h1>

					<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
						<nav class="comment-nav above">
							<a href='<?php previous_comments_link() ?>' class="previous">Older Comments</a>
							<a href='<?php next_comments_link() ?>' class="next">Newer Comments</a>
						</nav>
					<?php endif ?>

					<ol class="commentlist"><?php wp_list_comments( array( 'callback' => array( $framework, 'show_comment' ) ) ) ?></ol>

					<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
						<nav class="comment-nav below">
							<a href='<?php previous_comments_link() ?>' class="previous">Older Comments</a>
							<a href='<?php next_comments_link() ?>' class="next">Newer Comments</a>
						</nav>
					<?php endif ?>
				<?php endif ?>

			<?php
				ob_start();
				$args = array(
					'comment_notes_after' => '',
					'comment_notes_before' => ''
				);
				comment_form( $args );
				$comment_form = ob_get_clean();
				$replacement = '<p class="form-submit">'. do_shortcode( '[button background="secondary" text="$1"]' ) .'</p>';

				$comment_form = preg_replace( "/<p class=[\"|']form-submit[\"|']>(.*?)<\/p>/s", $replacement, $comment_form);



				echo $comment_form;

			?>
		</div><!-- .comments -->
	</div>

<?php elseif( is_single( 'post' ) ) : ?>
	<div <?php echo $blueprint->boxed_class( 'comments' ) ?>>
		<hgroup class='notice'>
			<h1><?php echo $framework['options']['comments_closed_title'] ?></h1>
			<h2><?php echo $framework['options']['comments_closed_message'] ?></h2>
		</hgroup>
	</div>
<?php endif; ?>