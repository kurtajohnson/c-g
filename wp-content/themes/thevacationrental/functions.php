<?php
/** Theme Functions File
  *
  * @package The Vacation Rental
  *
  **/

include( 'framework/classes/TVR_Apartments.class.php' );
include( 'framework/classes/TVR_Apartment.class.php' );

$apartments = new TVR_Apartments();


include( 'framework/init.php' );
$framework = new EB_Framework( $eb_config, $eb_defaults );
$eb_widgets = new EB_Widgets( $framework );
$blueprint = new EB_Blueprint( $framework );

if ( ! isset( $content_width ) ) $content_width = 1140;

add_theme_support( 'post-thumbnails' );
add_theme_support( 'automatic-feed-links' );

	class Walker_Nav_Menu_Dropdown extends Walker_Nav_Menu{
		function start_lvl(&$output, $depth){
			$indent = str_repeat("\t", $depth);
		}

		function end_lvl(&$output, $depth){
			$indent = str_repeat("\t", $depth);
		}

		function start_el(&$output, $item, $depth, $args){
			global $framework;
 			$selected = ( $framework->get_current_url() == $item->url ) ? 'selected="selected"' : '';
			$item->title = str_repeat("&nbsp;", $depth * 4).$item->title;
			parent::start_el($output, $item, $depth, $args);
			$output = str_replace('<li', '<option ' . $selected . ' value="' . $item->url . '"', $output);
		}

		function end_el(&$output, $item, $depth){
			$output .= "</option>\n";
		}
	}

	function tvr_dropdown_pages() {
		global $framework;
		$dropdown = wp_dropdown_pages( 'hierarchical=1&echo=0' );
		preg_match_all("/value=\"([0-9]*)\"/", $dropdown, $matches );
		foreach( $matches[1] as $key => $id ) {
			$url = get_permalink( $id );
			$selected = ( $url == $framework->get_current_url() ) ? 'selected="selected"' : '';
			$dropdown = str_replace( $matches[0][$key], $selected . ' value="' . $url . '"', $dropdown );
		}
		echo $dropdown;
	}



function tvr_docs() {
?>
<div class='documentation_video'>
<iframe src="http://player.vimeo.com/video/55473220?byline=0&amp;portrait=0&amp;badge=0&amp;color=ffffff" width="500" height="388" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
</div>

<div class='documentation_video'>
<iframe src="http://player.vimeo.com/video/55472370?byline=0&amp;portrait=0&amp;badge=0&amp;color=ffffff" width="500" height="388" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
</div>

<div class='documentation_video'>
<iframe src="http://player.vimeo.com/video/55474043?byline=0&amp;portrait=0&amp;badge=0&amp;color=ffffff" width="500" height="388" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
</div>

<?php

}


add_action( 'wp_footer', 'tvr_analytics' );
function tvr_analytics() {
	global $framework;
	if( !empty( $framework->options['analytics'] ) ) {
		echo $framework->options['analytics'];
	}
}

function tvr_readme() {

$myFile = get_template_directory() . "/readme.txt";
$fh = fopen($myFile, 'r');
$data = fread($fh, filesize($myFile));
fclose($fh);


$data = preg_replace( '/###(.*)/', '<h4>$1</h4>' , $data );
$data = preg_replace( '/##(.*)/', '<h3>$1</h3>' , $data );
$data = preg_replace( '/#(.*)/', '<h2>$1</h2>' , $data );

$data = wpautop( $data );

echo $data;

}


add_filter( 'blueprint_format_image', 'tvr_format_image', 10, 5 );

function tvr_format_image( $html = '', $post_id, $post_thumbnail_id, $size = '', $attr = '' ) {
	$container_class = ( !empty($attr['container_class'] ) ) ? $attr['container_class'] : '';
	$html = '<div class="image ' . $container_class . '">' . $html . '</div>';
	return $html;
}




add_action( 'wp_enqueue_scripts', 'tvr_theme_scripts' );
function tvr_theme_scripts() {
	wp_register_script( 'thevacationrental', get_template_directory_uri() . '/js/thevacationrental.js', array( 'jquery', 'jquery-ui-datepicker' ) );
	wp_register_script( 'jquery-isotope', get_template_directory_uri() . '/js/vendor/jquery.isotope.min.js', array( 'jquery' ) );
	wp_register_script( 'jquery-flexslider', get_template_directory_uri() . '/js/vendor/jquery.flexslider.min.js', array( 'jquery' ) );
	wp_register_script( 'jquery-uniform', get_template_directory_uri() . '/js/vendor/jquery.uniform.min.js', array( 'jquery' ) );
	wp_register_script( 'jquery-scrollto', get_template_directory_uri() . '/js/vendor/jquery.scrollTo.min.js', array( 'jquery' ) );
	wp_register_script( 'booking', get_template_directory_uri() . '/js/booking.js', array( 'jquery' ) );
	wp_register_script( 'booking-widget', get_template_directory_uri() . '/js/booking-widget.min.js', array( 'jquery' ) );

    wp_localize_script( 'thevacationrental', 'tvr', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
    wp_localize_script( 'booking', 'tvr', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
    wp_localize_script( 'booking-widget', 'tvr', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

    wp_enqueue_script( 'booking' );
     wp_enqueue_script( 'booking-widget' );
   wp_enqueue_script( 'jquery-ui-slider' );
    wp_enqueue_script( 'jquery-uniform' );
    wp_enqueue_script( 'jquery-scrollto' );
    wp_enqueue_script( 'jquery-ui-datepicker' );
    wp_enqueue_script( 'jquery-isotope' );
    wp_enqueue_script( 'jquery-flexslider' );
    wp_enqueue_script( 'thevacationrental' );


}

add_action( 'admin_enqueue_scripts', 'tvr_admin_scripts' );

function tvr_admin_scripts() {
	global $current_screen, $framework;

	$minified = ( EB_ENVIRONMENT == 'test' ) ? '' : '.min';

	if( $current_screen->base == 'tvr_booking_page_tvr_booking_calendar' ) {

	    wp_register_script(
	    	'booking-calendar',
	    	EB_ADMIN_THEME_URL . '/js/booking-calendar' . $minified . '.js',
	    	array( 'jquery' )
	    );
	    wp_register_script(
	    	'fullcalendar',
	    	EB_ADMIN_THEME_URL . '/js/fullcalendar' . $minified . '.js',
	    	array( 'jquery' )
	    );

 		wp_enqueue_script( 'fullcalendar' );
		wp_enqueue_script( 'booking-calendar' );

	    wp_register_style(
	    	'fullcalendar',
	    	get_template_directory_uri() . '/css/fullcalendar.css'
	    );
		 wp_enqueue_style( 'fullcalendar' );


	}
}


add_action( 'wp_ajax_nopriv_action_get_apartment_data', 'action_get_apartment_data' );
add_action( 'wp_ajax_action_get_apartment_data', 'action_get_apartment_data' );

function action_get_apartment_data() {
	$apartment = new TVR_Apartment( $_POST['apartment_id'] );
	$data['unavailable'] = $apartment->get_unavailable_dates();
	$data['maximum_guests'] = $apartment->maximum_guests;
	$data['current_price'] = $apartment->current_price;
	$data['minimum_stay'] = $apartment->data->minimum_stay;

	echo json_encode( $data );
	die();
}


add_action( 'wp_ajax_action_get_apartment_daterange_price', 'action_get_apartment_daterange_price' );
add_action( 'wp_ajax_nopriv_action_get_apartment_daterange_price', 'action_get_apartment_daterange_price' );
function action_get_apartment_daterange_price() {
	$apartment = new TVR_Apartment( $_POST['apartment_id'] );
	echo $apartment->get_daterange_price( $_POST['checkin'], $_POST['checkout'] );

	die();
}


add_action( 'wp_ajax_action_insert_booking', 'action_insert_booking' );
add_action( 'wp_ajax_nopriv_action_insert_booking', 'action_insert_booking' );

function action_insert_booking() {
	$data = array(
		'post_type' => 'tvr_booking',
		'post_status' => 'draft',
		'post_title' => get_the_title( $_POST['apartment_id'] ) . ' [' . date( 'Y-m-d H:i' ) . ']',
		'post_content' => wpautop( $_POST['message'] ),
	);

	$booking_id = wp_insert_post( $data );
	$stay_interval['start_date'] = $_POST['checkin'];
	$stay_interval['end_date'] = $_POST['checkout'];
	update_post_meta( $booking_id, 'apartment', $_POST['apartment_id'] );
	update_post_meta( $booking_id, 'guests_number', $_POST['guests'] );
	update_post_meta( $booking_id, 'contact_name', $_POST['name'] );
	update_post_meta( $booking_id, 'contact_email', $_POST['email'] );
	update_post_meta( $booking_id, 'contact_phone', $_POST['phone'] );
	update_post_meta( $booking_id, 'stay_interval', $stay_interval );


	$to = get_option( 'admin_email' );
	$subject = 'New Booking For ' . get_the_title( $booking_id );
	$message = "
		<html>
		<head><title>New Booking</title></head>
		<body>
			<h2>Booking Details</h2>
			<strong>Apartment</strong>: " . get_the_title( $booking_id ) . "<br>
			<strong>Check In</strong>: " .  $stay_interval['start_date'] . "<br>
			<strong>Check Out</strong>: " . $stay_interval['end_date'] . "<br>
			<strong>Guests</strong>: " .  $_POST['guests'] . "<br>
			<br>
			<h2>Contact Details</h2>
			<strong>Name</strong>: " . $_POST['name'] . "<br>
			<strong>Email</strong>: " . $_POST['email'] . "<br>
			<strong>Phone</strong>: " . $_POST['phone'] . "<br>
			<br>
			<h2>View online</h2>
			<strong><a href='" . get_edit_post_link($booking_id) . "'>Click here to view it online</a></strong><br>
		</body>
		</html>
	";

	wp_mail( $to, $subject, $message );

	header( 'Location: ' . $_POST['redirect'] . '?complete=true'  );


	die();

}






?>