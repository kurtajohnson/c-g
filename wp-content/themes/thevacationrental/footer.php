<?php
/** Footer
  *
  * Loads the footer of the website.
  *
  * @package The Vacation Rental
  *
  */
  global $framework, $blueprint, $post;

  if( ! is_object( $blueprint ) ) {
  	$blueprint = new EB_Blueprint( $framework );
  }


?>


</div> <!-- Site Content -->


<div class='container' id='site-footer'>


<?php if( !empty( $framework->options['footer_page'] ) ) :
	$temp_post = $post;
	$post = get_post( $framework->options['footer_page'] );
	$blueprint->add_data();
	setup_postdata($post);
?>

	<div class='row'>
		<div class='twelvecol'>
			<div id='footer-page'>
			<?php include( get_template_directory() . '/templates/' . $blueprint->get_blueprint_template( $framework->options['footer_page'] ) ); ?>
			</div>
		</div>
	</div>

<?php
	$post = $temp_post;
endif;
?>


	<div class='row'>

		<div class='widget-area hidden-phone'>
		<?php

			$sidebars = wp_get_sidebars_widgets();
			if( isset( $sidebars['footerbar'] ) ) {
				$widgets = count( $sidebars['footerbar'] );

				ob_start();
				dynamic_sidebar( 'Footerbar' );
				$footerbar = ob_get_clean();

				$footerbar = str_replace("\n", '|||', $footerbar);
				preg_match_all('/<div class="box widget fbwidget scheme-footer (.*?)">(.*?)<div class="end"><\/div><\/div>/m', $footerbar, $matches);

				$elements = $matches[0];

				$count = count($elements);

				$widgets = array();
				$columns = array(
					1 => 'twelvecol',
					2 => 'sixcol',
					3 => 'fourcol',
					4 => 'threecol',
					6 => 'twocol',
				);
				$i=1;
				foreach( $elements as $element ) {
					$last = ( $i == $count ) ? 'last' : '';
					$first = ( $i == 1 ) ? 'first' : '';
					$element = str_replace("|||", "\n", $element);
					$widgets[] = '<div class="' . $columns[$count] . ' ' . $first . ' ' . $last . '">' . $element . '</div>';
					$i++;
				}

				$footerbar = implode( $widgets );
				echo $footerbar;
			}
		?>
		</div>


	</div>

	<div class='row'>
		<?php if( !empty( $framework->options['site_footer_copyright_text'] ) ) : ?>
			<div class='copyright'><?php echo $framework->options['site_footer_copyright_text'] ?></div>
		<?php endif ?>

		<div class='navigation visible-desktop'>
			<?php
			wp_nav_menu( array(
				'theme_location' => 'site_footer',
			))
			?>
		</div>
		<div class='navigation hidden-desktop navigation-select'>
			<?php
				wp_nav_menu(array(
				  'theme_location' => 'site_footer',
				  'walker'         => new Walker_Nav_Menu_Dropdown(),
				  'items_wrap'     => '<select>%3$s</select>',
				  'fallback_cb'    => 'RF_Framework::rf_dropdown_pages',
				));
			?>
		</div>
	</div>
</div>

<?php wp_footer(); ?>
</body>
</html>