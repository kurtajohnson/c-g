<?php
/**  Default
  *
  *	This layout shows posts in the default way
  *
  */
	global $apartment, $framework;

	$classes = '';
	if( !empty( $apartment->type ) ) {
		$types = array_keys( $apartment->type );
		$classes .= ' type-' . implode( ' type-', $types ) . ' ';

	}
	if( !empty( $apartment->amenities ) ) {
		$amenities = array_keys( $apartment->amenities );
		$classes .= ' amenity-' . implode( ' amenity-', $amenities ) . ' ';
	}

	$details = '';
	if( !empty( $apartment->apartment_details  ) ) {
		$details = array();
		foreach( $apartment->apartment_details as $name => $value ) {
			$name = str_replace( ' ', '_', strtolower( $name ) );
			$details[] = 'data-' . $name . '="' . $value . '"';
		}
		$details = implode( ' ', $details );
	}


	$classes .= ' price-' . $apartment->current_price;
	$classes .= ' apartment-' . $apartment->ID;
?>

<div class='apartmentlist-layout-default apartment <?php echo $classes ?>' id='apartment-<?php echo $apartment->ID ?>' <?php echo $details ?> data-id='<?php echo $apartment->ID ?>'>
	<div class='row'>

		<?php if( has_post_thumbnail( $apartment->ID ) ) : ?>
		<div class='apartment-image box has-binder smallpadding binded left'>
			<a class='hoverlink' href='<?php echo get_permalink( $apartment->ID ) ?>'><?php echo wp_get_attachment_image( $apartment->data->_thumbnail_id, 'eb_col_3' ) ?></a>
			<div class='binder-top'></div>
			<div class='binder-bottom'></div>
		</div>
		<?php endif ?>
		<div class='apartment-content box binded right nopadding'><div class='box-content'>
			<div class='row'>
				<div class='apartment-sidebar'>
					<h3 class='price'><?php echo $framework->get_price_format( $apartment->current_price , 'price-' . $post->ID ) ?></h3>
					<ul class='details'>
						<li>Max Guests: <span class='guests'><?php echo $apartment->maximum_guests ?></span></li>

					<?php if( !empty( $apartment->apartment_details ) ) : ?>
					<?php
						foreach( $apartment->apartment_details as $name => $value ) :
						$field_class = str_replace( ' ', '_', strtolower( $name ) );
					?>
						<li><?php echo $name ?>: <span class='<?php echo $field_class ?>'><?php echo $value ?></span></li>
					<?php endforeach ?>
					<?php endif ?>
					</ul>
					<?php echo do_shortcode( '[button url="' . get_permalink( $apartment->book_now_page ) . '?apartment_id=' . $apartment->ID . '" text="Book Now" background="secondary" gradient="yes" radius="2px"]' ) ?>

				</div>
				<div class='apartment-main'>
					<h1 class='title post_title'><a href='<?php echo get_permalink( $apartment->ID ) ?>'><?php echo $apartment->data->post_title ?></a></h1>
					<?php if( !empty( $apartment->type ) ) : ?>
					<div class='type'>Type: <?php echo implode( ', ', array_values( $apartment->type ) ) ?></div>
					<?php endif ?>


					<div class='content'><?php echo $apartment->excerpt ?></div>

					<?php if( !empty( $apartment->amenities ) ) : ?>
					<div class='amenities'>
						<h2>Amenities</h2>
						<?php echo implode( ', ', array_values( $apartment->amenities ) ) ?>
					</div>
					<?php endif ?>

					<div class="booknow-phone">
						<?php echo do_shortcode( '[button url="' . get_permalink( $apartment->book_now_page ) . '?apartment_id=' . $apartment->ID . '" text="Book Now" background="secondary" gradient="yes" radius="2px"]' ) ?>
						<h3 class='price'><?php echo $framework->get_price_format( $apartment->current_price , 'price-' . $post->ID ) ?></h3>

					</div>


				</div>

			</div></div>

		</div>
	</div>
</div>

