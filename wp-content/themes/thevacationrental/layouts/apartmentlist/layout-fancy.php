<?php
/**  Fancy
  *
  *	This layout shows posts in the default way
  *
  */
	global $apartment_id, $apartment;
	if( has_post_thumbnail( $apartment_id ) ) :

		$classes = '';
		if( !empty( $apartment['types'] ) ) {
			$types = array_values( $apartment['types'] );
			$classes .= ' type-' . implode( ' type-', $types ) . ' ';
		}
		if( !empty( $apartment['amenities'] ) ) {
			$amenities = array_values( $apartment['amenities'] );
			$classes .= ' amenity-' . implode( ' amenity-', $amenities ) . ' ';
		}

		$details = '';
		if( !empty( $apartment['apartment_details']  ) ) {
			$details = array();
			foreach( $apartment['apartment_details'] as $name => $value ) {
				$name = str_replace( ' ', '_', strtolower( $name ) );
				$details[] = 'data-' . $name . '="' . $value . '"';
			}
			$details = implode( ' ', $details );
		}


		$classes .= ' price-' . $apartment['current_price'];
		$classes .= ' apartment-' . $apartment_id;
	?>

	<div <?php echo $details ?> class='apartmentlist-layout-fancy apartment <?php echo $classes ?>' id='apartment-<?php echo $apartment_id ?>' data-id='<?php echo $apartment_id ?>'>
		<div class='row'>

			<div class='apartment-image box smallpadding'>
				<div class='inner'>
					<?php echo wp_get_attachment_image( $apartment['_thumbnail_id'], 'rf_col_1' ) ?>

					<h3 class='price'>$<span class='current_price'><?php echo $apartment['current_price'] ?></span>
					<?php echo do_shortcode( '[button margin="none" text="Book Now" background="green" gradient="yes" radius="2px"]' ) ?>
					</h3>


					<div class='apartment-content'>
						<div class='content-inner'>
							<h1 class='title post_title'><a href='<?php echo get_permalink( $apartment_id ) ?>'><?php echo $apartment['post_title'] ?></a></h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif ?>