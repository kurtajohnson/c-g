<?php
/*
	Default Imageslider Layout
	This layout shows images in the default way
*/
global $imageslider;
?>
<?php if( $imageslider->have_posts() ) : ?>

<div class="imageslider">
  <ul class="slides">
	<?php
		while( $imageslider->have_posts() ) : $imageslider->the_post();
		$image = wp_get_attachment_image_src( $post->ID, 'rf-col-1' )
	?>
		<li>
			<img src='<?php echo $image[0] ?>'>
			<?php if( $attributes['show_title'] == 'yes' ) : ?>
				<div class='title'><h1><?php the_title() ?></h1></div>
			<?php endif ?>

		</li>
	<?php endwhile ?>
  </ul>
</div>


<?php endif ?>
