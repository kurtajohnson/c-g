<?php
/** Thumbnails
  *
  * This layout shows posts one under another with an
  * image on one side and the post text on the other.
  *
  * The image is joined to the post text box with a
  * binder image. The image is always shown on the
  * opposite side of the sidebar
  *
  * @package The Vacation Rental
  *
  */
	global $blueprint, $framework;

	$sidebar_class = ( $blueprint->has_sidebar() ) ? 'sidebar-on' : 'sidebar-off';
	$image_size = ( $sidebar_class == 'sidebar-off' ) ? 'rf_col_2' : 'rf_col_3';

	$image_position_class = ( $blueprint->get_sidebar_position() == 'left' ) ? 'right' : 'left';
	$details_position_class = $blueprint->get_sidebar_position();
?>

<div <?php post_class( 'post-layout-thumbnail ' . $sidebar_class ) ?>>

	<div class='row'>
		<?php if( has_post_thumbnail() ) : ?>
		<div class='box smallpadding has-binder binded <?php echo $image_position_class ?> image-container'>
			<div class='image'>
				<a href='<?php the_permalink() ?>' class='imagelink hoverlink'><?php the_post_thumbnail( $image_size ) ?><div class='shadow'></div></a>
			</div>
			<div class='binder-top'></div>
			<div class='binder-bottom'></div>
		</div>
		<?php endif ?>

		<div class='box details binded <?php echo $details_position_class ?>'>

			<?php if( $post->post_title != '' ) : ?>
				<h1 class='post-title'><a href='<?php the_permalink() ?>'><?php the_title() ?></a></h1>
			<?php endif ?>

			<div class='post-meta horizontal'>
				<div class='meta post-author'>Author: <?php the_author_posts_link() ?></div>
				<?php if( has_category() ) : ?>
					<div class='meta post-category'>
					Categories: <?php the_category( ', ' ) ?>
					</div>
				<?php endif ?>
			</div>

			<div class='content'>
				<?php the_excerpt() ?>
			</div>
			<a title='Read the full post' class='read-more' href='<?php the_permalink() ?>'><?php echo $framework->options['read_more'] ?><span class='read-more-arrow'></span></a>


		</div>
	</div>

</div>