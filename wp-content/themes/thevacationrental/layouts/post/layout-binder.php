<?php
/** Binder Layout
  *
  * This layout shows posts one under another with a large
  * image on top The image is joined with a binder graphic
  * to the post underneath it.
  *
  * The post contains a meta section in its sidebar and a
  * section showing the title, excerpt and read more links.
  *
  * @package The Vacation Rental
  *
  */
  global $blueprint, $framework, $post;

?>
<div <?php post_class( 'post-layout-binder' ) ?>>
	<?php if( has_post_thumbnail() ) : ?>
	<div class='box smallpadding has-binder binded top image-container'>
		<div class='image'><a href='<?php the_permalink() ?>' class='imagelink hoverlink'><?php the_post_thumbnail( 'rf_col_1' ) ?><div class='shadow'></div></a></div>
		<div class='binder-left'></div>
		<div class='binder-right'></div>
	</div>
	<div class='box binded bottom nopadding post-content'>
	<?php else : ?>
	<div class='box nopadding post-content'>
	<?php endif ?>
		<aside class='post-meta side hidden-phone'>
			<h3 class='post-date'>
				<span class='month'><?php the_time( 'd M' ) ?></span>
				<span class='year'><?php the_time( 'Y' ) ?></span>
			</h3>
			<div class='separator'></div>

			<div class='meta post-author'>Author: <?php the_author_posts_link() ?></div>
			<?php if( has_category() ) : ?>
				<div class='meta post-category'>
				Categories: <?php the_category( ', ' ) ?>
				</div>
			<?php endif ?>
			<?php if( comments_open() ) : ?>
			<div class='meta post-comments'>
				Comments: <a href='<?php comments_link(); ?>'><?php comments_number( '0', '1', '%' ); ?></a>
			</div>
			<?php endif ?>
		</aside>
		<div class='post-text'>
			<?php if( $post->post_title != '' ) : ?>
				<h1 class='post-title'><a href='<?php the_permalink() ?>'><?php the_title() ?></a></h1>
			<?php endif ?>

			<div class='post-meta horizontal visible-phone'>
				<div class='meta post-author'>Author: <?php the_author_posts_link() ?></div>
				<?php if( has_category() ) : ?>
					<div class='meta post-category'>
					Categories: <?php the_category( ', ' ) ?>
					</div>
				<?php endif ?>
			</div>


			<div class='content'>
				<?php the_excerpt() ?>
			</div>
			<a title='Read the full post' class='read-more' href='<?php the_permalink() ?>'><?php echo $framework->options['read_more'] ?><span class='read-more-arrow'></span></a>
		</div>
		<div class='clear'></div>
	</div>
</div>