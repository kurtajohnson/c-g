<?php
/** Simple
  *
  * This layout shows posts one under another with no image,
  * just the title, the excerpt and the read more link
  *
  * @package The Vacation Rental
  *
  */
  global $blueprint, $theme_options, $framework;

?>
<div <?php post_class( 'post-layout-simple' ) ?>>
	<div class='box nopadding post-content'>
		<div class='post-text'>
			<?php if( $post->post_title != '' ) : ?>
				<h1 class='post-title'><a href='<?php the_permalink() ?>'><?php the_title() ?></a></h1>
			<?php endif ?>

			<div class='content'>
				<?php the_excerpt() ?>
			</div>
			<a title='Read the full post' class='read-more' href='<?php the_permalink() ?>'><?php echo $framework->options['read_more'] ?><span class='read-more-arrow'></span></a>
		</div>
		<div class='clear'></div>
	</div>
</div>