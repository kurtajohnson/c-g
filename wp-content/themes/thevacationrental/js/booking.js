;( function( $, window, document, undefined ) {

	var defaults = {
		messages : {
			select_apartment : 'Select an apartment',
			contact_details : 'Make sure to tell us your name and enter a valid email address',
			select_date: 'Please select dates before you continue',
			invalid_range: 'The range you have selected is not valid for this apartment.',
			invalid_guests: 'The apartment you have selected can not house as many guests as you need',
		},
		unavailable_dates: []
	};

	function tvr_booknow( element, options ) {

		element.config = $.extend( {}, defaults, options );
		element.now = $.datepicker.formatDate( 'yy-mm-dd', new Date() );
		element.checkout = element.find( 'input[name="checkout"]' );
		element.checkin = element.find( 'input[name="checkin"]' );
		element.apartment = element.find( '#apartment_id' );
		element.dates = element.find( 'input[name="checkout"], input[name="checkin"]' );
		element.guests = element.find( 'input[name="guests"]' );
		element.messageTemplate = element.find('#message');
		element.submit = 0;

		element.minimum_stay = element.attr('data-minimum_stay')

	}

	$.fn.tvr_booknow = function( options ) {

		new tvr_booknow( this, options );
		var form = this;


		$(window).load( function() {
			if( form.length > 0 ) {
			check_range();
			check_minimum_stay();
			}
		})


		/***********************************************/
		/*           Changing Field Values             */
		/***********************************************/

		// Apartment Selection

		form.apartment.on( 'change', function(){
			form.trigger('startLoad');

			if( check_apartment( $(this).val() ) === true ) {

				set_apartment_data( $(this).val() );
				set_guest_options();

				check_range();
				check_minimum_stay();
			}

			form.trigger('finishLoad');
		});

		form.dates.on( 'change', function() {
			if( is_valid_range() === false ) {
				show_error( 'invalid_range' );
			}
			else if( is_valid_minimum_stay() === false ){
				var minimum_stay = form.minimum_stay;
				var minimum_stay_text = '';
				if (minimum_stay == 1 ) {
					minimum_stay_text = '1 day';
				}
				else {
					minimum_stay_text = minimum_stay + ' days';
				}
				show_error( 'custom', 'The minimum stay is ' + minimum_stay_text  );
			}
			else {
				hide_error();
			}
		});


		/***********************************************/
		/*                 Apartment                   */
		/***********************************************/

		function check_apartment( apartment_id ) {

			if( apartment_id === '' && typeof apartment_id === 'undefined' || apartment_id.length === 0 ) {
				show_error( 'select_apartment' );
				disable_inputs();
				return false;
			}
			else {
				hide_error( 'select_apartment' );
				enable_inputs();
				return true;
			}
		}

		/***********************************************/
		/*                   Calendar                  */
		/***********************************************/

		checkin_options = {
			beforeShowDay: function( fullDate ) {
				var date = $.datepicker.formatDate( 'yy-mm-dd', fullDate );

				var checkoutDate = form.checkout.datepicker( "getDate" );

				if( checkoutDate !== null ) {
					checkoutDate.setDate( checkoutDate.getDate() - 1 );
					checkoutDate = $.datepicker.formatDate( 'yy-mm-dd', checkoutDate );
				}

				if( checkoutDate !== null && date > checkoutDate ) {
					return [false, 'past', 'After Checkout Date'];
				}
				else if( date  < form.now ) {
					return [false, 'past', 'Passed Date'];
				}
				else if ( $.inArray( date, form.config.unavailable ) != -1 ) {
					return [false, 'unavailable', 'Unavailable'];
				}
				else {
					return [true, 'available', 'Available'];
				}

			}
		};

		form.checkin.datepicker( checkin_options );

		var checkout_reached_unavailable = 0;

		checkout_options = {
			beforeShowDay: function( fullDate ) {
				var date = $.datepicker.formatDate( 'yy-mm-dd', fullDate );

				var checkinDate = form.checkin.datepicker( "getDate" );

				if( checkinDate !== null ) {
					checkinDate.setDate( checkinDate.getDate() + 1 );
					checkinDate = $.datepicker.formatDate( 'yy-mm-dd', checkinDate );
				}
				else {
					checkinDate = '0000-00-00';
				}
				if( checkout_reached_unavailable !== 0 && checkinDate !== '0000-00-00' && date > checkout_reached_unavailable ) {
					return [false, 'past', 'Range Contains Unavailables'];
				}
				if( date < form.now || date < checkinDate ) {
					return [false, 'past', 'Passed Date'];
				}
				else if ( $.inArray( date, form.config.unavailable ) != -1 ) {
					checkout_reached_unavailable = date;
					return [false, 'unavailable', 'Unavailable'];
				}
				else {
					return [true, 'available', 'Available'];
				}

			},
			onClose: function() {
				checkout_reached_unavailable = 0;
			}
		};

		form.checkout.datepicker( checkout_options );

		/***********************************************/
		/*                Submit Check                 */
		/***********************************************/

		form.on( 'submit', function() {
			var returns = [];
			returns.push( check_apartment( form.apartment ) );
			returns.push( check_dates() );
			returns.push( check_contact() );
			returns.push( check_range() );
			returns.push( is_valid_minimum_stay() );

			form.submit = 1;
			if( $.inArray( false, returns ) == -1 )  {
				return true;
			}
			else {
				return false;
			}

		});


		function check_minimum_stay() {
		 	if( is_valid_minimum_stay() ) {
				hide_error( 'custom' );
		 		return true;
		 	}
		 	else {
				var minimum_stay = form.minimum_stay;
				var minimum_stay_text = '';
				if (minimum_stay == 1 ) {
					minimum_stay_text = '1 day';
				}
				else {
					minimum_stay_text = minimum_stay + ' days';
				}
				if( minimum_stay_text != null && minimum_stay_text != '' && typeof minimum_stay_text != 'undefined' && minimum_stay_text != ' days') {
					show_error( 'custom', 'The minimum stay is ' + minimum_stay_text  );
				}
		 		return false;
		 	}
		}

		function is_valid_minimum_stay() {
			var checkinDate = form.checkin.datepicker( "getDate" );
			var checkoutDate = form.checkout.datepicker( "getDate" );
			var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds

			if( checkinDate == '' || typeof checkinDate == 'undefined' || checkoutDate == '' || typeof checkoutDate == 'undefined' || checkinDate == null || checkoutDate == null) {
				return false;
			}
			else {
				var days = Math.round(Math.abs((checkinDate.getTime() - checkoutDate.getTime())/(oneDay)));

				if( days >= form.minimum_stay) {
					return true;
				}
				else {
					return false;
				}
			}
		}


		/***********************************************/
		/*               Contact Details               */
		/***********************************************/

		form.find( 'input[name="name"], input[name="email"]' ).on( 'change', function() {
			if (form.submit === 1) {
				check_contact();
			}
		});

		function check_contact() {
			var name = form.find( 'input[name="name"]' ).val();
			var email = form.find( 'input[name="email"]' ).val();

			if( name === '' || email === '' || validateEmail( email ) === false )  {
				show_error( 'contact_details' );
				return false;
			}
			else {
				hide_error( 'contact_details' );
				return true;
			}
		}

		function validateEmail( email ) {
			var atpos=email.indexOf("@");
			var dotpos=email.lastIndexOf(".");
			if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
				return false;
			}
			else {
				return true;
			}
		}
		/***********************************************/
		/*                Custom Loader                */
		/***********************************************/


		$(form).bind( 'startLoad', function(){
			show_loader();
		});


		$(form).bind( 'finishLoad', function(){
			hide_loader();
		});

		/***********************************************/
		/*                   Guests                    */
		/***********************************************/

		function set_guest_options() {
			var guests = form.config.maximum_guests;
			form.guests = form.find('select[name="guests"]');
			var current_guests = form.guests.val();
			form.guests.find('option').remove();
			for( var i = 1; i <= guests; i++ ) {
				form.guests.append( '<option value=' + i + '>' + i + '</option>' );
			}
			newCurrent = form.guests.find('option[value="' + current_guests + '"]');

			if( newCurrent.length > 0 ) {
				newCurrent.attr('selected', 'selected');
				hide_error( 'invalid_guests' );
			}
			else if ( current_guests !== null ){
				show_error( 'invalid_guests' );
			}
			$.uniform.update(form.guests);
		}

		form.find('select[name="guests"]').on( 'focus', function() {
			hide_error( 'invalid_guests' );
		});


		/***********************************************/
		/*               Unavailability                */
		/***********************************************/

		function check_dates() {
			var checkinDate = form.checkin.datepicker( "getDate" );
			var checkoutDate = form.checkout.datepicker( "getDate" );

			if( checkinDate === null || checkoutDate === null ) {
				show_error( 'select_date' );
				return false;
			}
			else {
				hide_error( 'select_date' );
				return true;
			}
		}

		function set_apartment_data( apartment_id ) {
			var apartment = get_apartment_data( apartment_id );
			form.config.unavailable = apartment.unavailable ;
			form.config.maximum_guests = parseInt( apartment.maximum_guests, 10 );
			form.minimum_stay = apartment.minimum_stay;
		}

		function get_apartment_data( apartment_id ) {
			var apartment_data = {};
			$.ajax({
				url: tvr.ajaxurl,
				type: 'post',
				dataType: 'json',
				async: false,
				data: {
					apartment_id : apartment_id,
					action: 'action_get_apartment_data'
				},
				success: function( response ) {
					apartment_data = response;
				}
			});
			return apartment_data;
		}


		function is_valid_range() {
			var checkinDate = form.checkin.datepicker( "getDate" );
			var checkoutDate = form.checkout.datepicker( "getDate" );

			var valid = true;
			if( checkinDate !== null && checkoutDate !== null ) {
				var dates = getDates( checkinDate, checkoutDate );
				$.each( dates, function() {
					var date = $.datepicker.formatDate( 'yy-mm-dd', this );
					if ( $.inArray( date, form.config.unavailable ) !== -1 ) {
						valid = false;
					}
				});
			}

			return valid;
		}

		function check_range() {
			if( is_valid_range() === false ) {
				show_error( 'invalid_range' );
				return false;
			}
			else {
				hide_error( 'invalid_range' );
				return true;
			}
		}

		Date.prototype.addDays = function(days) {
			var dat = new Date(this.valueOf());
			dat.setDate(dat.getDate() + days);
			return dat;
		};


		function getDates(startDate, stopDate) {
			var dateArray = [];
			var currentDate = startDate;
			while (currentDate <= stopDate) {
				dateArray.push(currentDate);
				currentDate = currentDate.addDays(1);
			}
			return dateArray;
		}


		/***********************************************/
		/*            Show/Hide Of Elements            */
		/***********************************************/

		function hide_error( type ) {
			if( typeof type === 'undefined' || type === '' || type === 'all' ) {
				form.find( '.form-message' ).remove();
			}
			else {
				form.find( '.form-message[data-type="' + type + '"]' ).remove();
			}
		}

		function show_error( error_type, custom_message ) {
			if( form.find( '.form-message[data-type="' + error_type + '"]' ).length === 0 ) {
				var error = form.messageTemplate.clone();
				error.removeAttr( 'id' ).attr( 'data-type', error_type );
				message = form.config.messages[error_type];
				if( error_type == 'custom' ) {
					message = custom_message;
				}
				error.find( '.message' ).html( message );
				form.prepend( error );
				error.show();
			}
		}

		function show_loader() {
			form.find( '.loader' ).stop(true, true).fadeIn();
		}

		function hide_loader() {
			form.find( '.loader' ).stop(true, true).fadeOut();
		}

		function enable_inputs() {
			form.find(':disabled').removeAttr( 'disabled' );
		}

		function disable_inputs( type ) {
			if( typeof type === 'undefined' || type === '' || type === 'all' ) {
				form.find( 'input,textarea,select' ).not('#apartment_id').attr( 'disabled', 'disabled' );
			}
			else {
				form.find( 'input[type=' + type + '],textarea[type=' + type + '],select[type=' + type + ']' ).not('#apartment_id').attr( 'disabled', 'disabled' );
			}
		}

	};

})( jQuery, window, document );