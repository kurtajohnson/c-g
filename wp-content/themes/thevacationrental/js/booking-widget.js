;( function( $, window, document, undefined ) {

	var defaults = {
		unavailable: []
	};

	function tvr_booking_widget( element, options ) {

		element.config = $.extend( {}, defaults, options );
		element.now = $.datepicker.formatDate( 'yy-mm-dd', new Date() );
		element.checkout = element.find( 'input[name="checkout"]' );
		element.checkin = element.find( 'input[name="checkin"]' );
		element.apartment = element.find( 'select[name="apartment_id"]' );
		element.dates = element.find( 'input[name="checkout"], input[name="checkin"]' );
		element.guests = element.find( 'input[name="guests"]' );
		element.price = element.find('.section-total .price')

		element.config.unavailable = $.parseJSON( element.find( '.unavailable' ).html() );
		element.config.maximum_guests = parseInt( element.find( '.maximum_guests' ).html() );


	}

	$.fn.tvr_booking_widget = function( options ) {

		new tvr_booking_widget( this, options );
		var form = this;


		// Apartment Selection

		form.apartment.on( 'change', function(){
			form.trigger('startLoad');


			set_apartment_data( $(this).val() );
			set_guest_options();


			form.trigger('finishLoad');
		});


		form.find( '.per_time' ).on( 'change', function() {
			var price = parseInt( form.find( '.section-price' ).attr('data-price'), 10 ) ;
			var multiplier = parseInt( $( this ).val(), 10 )
			var newprice = price * multiplier;
			form.find( '.section-price .price' ).html( newprice )
		})

		function set_apartment_data( apartment_id ) {
			var apartment = get_apartment_data( apartment_id );
			console.log(apartment)
			form.config.unavailable = apartment.unavailable ;
			form.config.maximum_guests = parseInt( apartment.maximum_guests, 10 );
			form.find( '.section-price').attr( 'data-price', apartment.current_price );
			form.find( '.section-price .price' ).html( apartment.current_price );
		}

		function get_apartment_data( apartment_id ) {
			var apartment_data = {};
			$.ajax({
				url: tvr.ajaxurl,
				type: 'post',
				dataType: 'json',
				async: false,
				data: {
					apartment_id : apartment_id,
					action: 'action_get_apartment_data'
				},
				success: function( response ) {
					apartment_data = response;
				}
			});
			return apartment_data;
		}



		function set_guest_options() {
			var guests = form.config.maximum_guests;
			form.guests = form.find('select[name="guests"]');
			var current_guests = form.guests.val();
			form.guests.find('option').remove();
			for( var i = 1; i <= guests; i++ ) {
				form.guests.append( '<option value=' + i + '>' + i + '</option>' );
			}
			newCurrent = form.guests.find('option[value="' + current_guests + '"]');

			$.uniform.update(form.guests);
		}


		/***********************************************/
		/*               Update Price                  */
		/***********************************************/

		$( form.find('input[name="checkin"], input[name="checkout"], select[name="apartment_id"]') ).on( 'change', function() {
			if( form.checkin.val() !== '' && form.checkout.val() !== '' ) {
				form.trigger('startLoad');
				$.ajax({
					url: tvr.ajaxurl,
					type: 'post',
					dataType: 'json',
					async: false,
					data: {
						apartment_id : form.apartment.val(),
						checkin: form.checkin.val(),
						checkout: form.checkout.val(),
						action: 'action_get_apartment_daterange_price'
					},
					success: function( response ) {
						form.price.html( response );
						form.trigger('finishLoad');
					}
				});

			}

		})


		/***********************************************/
		/*                 Apartment                   */
		/***********************************************/


		checkin_options = {
			beforeShowDay: function( fullDate ) {
				var date = $.datepicker.formatDate( 'yy-mm-dd', fullDate );

				var checkoutDate = form.checkout.datepicker( "getDate" );

				if( checkoutDate !== null ) {
					checkoutDate.setDate( checkoutDate.getDate() - 1 );
					checkoutDate = $.datepicker.formatDate( 'yy-mm-dd', checkoutDate );
				}

				if( checkoutDate !== null && date > checkoutDate ) {
					return [false, 'past', 'After Checkout Date'];
				}
				else if( date  < form.now ) {
					return [false, 'past', 'Passed Date'];
				}
				else if ( $.inArray( date, form.config.unavailable ) != -1 ) {
					return [false, 'unavailable', 'Unavailable'];
				}
				else {
					return [true, 'available', 'Available'];
				}

			},
			beforeShow: function() {
				var id = form.checkin.attr('data-id');
				$( '#ui-datepicker-div' ).addClass( 'ui-datepicker-' + id );
			},
			onClose: function() {
				var id = form.checkin.attr('data-id');
				$( '#ui-datepicker-div' ).removeClass( 'ui-datepicker-' + id );
			}
		};

		form.checkin.datepicker( checkin_options );
		var checkout_reached_unavailable = 0;

		checkout_options = {
			beforeShowDay: function( fullDate ) {
				var date = $.datepicker.formatDate( 'yy-mm-dd', fullDate );

				var checkinDate = form.checkin.datepicker( "getDate" );

				if( checkinDate !== null ) {
					checkinDate.setDate( checkinDate.getDate() + 1 );
					checkinDate = $.datepicker.formatDate( 'yy-mm-dd', checkinDate );
				}
				else {
					checkinDate = '0000-00-00';
				}
				if( checkout_reached_unavailable !== 0 && checkinDate !== '0000-00-00' && date > checkout_reached_unavailable ) {
					return [false, 'past', 'Range Contains Unavailables'];
				}
				if( date < form.now || date < checkinDate ) {
					return [false, 'past', 'Passed Date'];
				}
				else if ( $.inArray( date, form.config.unavailable ) != -1 ) {
					checkout_reached_unavailable = date;
					return [false, 'unavailable', 'Unavailable'];
				}
				else {
					return [true, 'available', 'Available'];
				}

			},
			onClose: function() {
				var id = form.checkout.attr('data-id');
				$( '#ui-datepicker-div' ).removeClass( 'ui-datepicker-' + id );

				checkout_reached_unavailable = 0;
			},
			beforeShow: function() {
				var id = form.checkout.attr('data-id');
				$( '#ui-datepicker-div' ).addClass( 'ui-datepicker-' + id );
			},
		};

		form.checkout.datepicker( checkout_options );


		/***********************************************/
		/*                Custom Loader                */
		/***********************************************/


		$(form).bind( 'startLoad', function(){
			show_loader();
		});


		$(form).bind( 'finishLoad', function(){
			hide_loader();
		});



		function show_loader() {
			form.find( '.loader' ).stop(true, true).fadeIn();
		}

		function hide_loader() {
			form.find( '.loader' ).stop(true, true).fadeOut();
		}




	};

})( jQuery, window, document );