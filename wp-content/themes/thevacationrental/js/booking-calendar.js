jQuery( document ).ready( function() {

	jQuery( '.rf-datepicker-monthday' ).datepicker({
		dateFormat: "MM dd",
	})
	
	
	jQuery( '.add_seasonal_pricing' ).click( function() {
		jQuery( '.rf-datepicker-monthday' ).datepicker( 'destroy' );
		
		container = jQuery( this ).parents( '.control:first' );
		element = jQuery( '.seasonal_pricing:first' ).clone();
		element.find( 'input' ).val( '' );
		container.find( '.control-subgroups' ).append( element )

		element.find( '.rf-datepicker' ).removeAttr( 'id' )
		jQuery( '.rf-datepicker-monthday' ).datepicker({
			dateFormat: "mm-dd",
		})
	})
	

	jQuery( '.remove_seasonal_pricing' ).live( 'click', function() {
		container = jQuery( this ).parents( '.control:first' );
		items = container.find( '.control-subgroup' ).length
		group = jQuery( this ).parents( '.control-subgroup:first' );
		
		if( items > 1 ) {
			group.slideUp( function() {
				group.remove();
			})	
		}
	})




	
})