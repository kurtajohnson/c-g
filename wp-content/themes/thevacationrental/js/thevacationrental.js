jQuery(window).load(function() {

	menu_width = 0;
	jQuery.each( jQuery('#site-header .navigation-menu ul:first > li'), function() {
		menu_width = menu_width + jQuery(this).width()
	} )

	jQuery('.imageslider .slides').show();
	jQuery('.imageslider .loader').remove();
	if( jQuery( jQuery('.imageslider').length > 0 )) {
		jQuery.each( jQuery('.imageslider'), function( i ) {
			jQuery(this).attr( 'id', 'imageslider-' + i )

			animation = jQuery(this).attr('data-animation')
			direction = jQuery(this).attr('data-direction')
			slideshowSpeed = jQuery(this).attr('data-slideshow_speed')
			animationSpeed = jQuery(this).attr('data-animation_speed')
			pauseOnHover = jQuery(this).attr('data-pause_on_hover')
			smoothHeight = jQuery(this).attr('data-smooth_height')

			controlNav = true,
			control = jQuery(this).attr('data-controls');
			if( control == 'no' ) {
				controlNav = false;
			}

			pauseOnHover = true;
			if( pauseOnHover == 'no' ) {
				pauseOnHover = false;
			}

			smoothHeight = false;
			if( smoothHeight == 'yes' ) {
				smoothHeight = true;
			}


			jQuery( '#imageslider-' + i ).flexslider({
				animation: animation,
				direction : direction,
				slideshowSpeed: slideshowSpeed,
				animationSpeed : animationSpeed,
				pauseOnHover: pauseOnHover,
				controlNav: controlNav,
				directionNav: false,
				smoothHeight: smoothHeight,
			});
		})
	}


	show_navigation();


});

jQuery( document ).ready( function() {


	if( jQuery( '.availability-calendar' ).length > 0 ) {
		var options = {};
		var unavailable = [];
		var form;
		var booknow;
		var apartment_id;

		jQuery.each( jQuery( '.availability-calendar' ), function(){
			unavailable = jQuery.parseJSON( jQuery( this ).prev().html() ),
			form = jQuery(this).parents('.tvr_widget_calendar');
			booknow = form.attr('data-booknow_page');
			apartment_id = form.attr('data-apartment_id');
			options = {
				beforeShowDay: function( fullDate ) {
					var date = jQuery.datepicker.formatDate( 'yy-mm-dd', fullDate );
					var now = jQuery.datepicker.formatDate( 'yy-mm-dd', new Date() );

					if( date < now ) {
						return [false, 'past', 'Passed Date'];
					}
					else if ( jQuery.inArray( date, unavailable ) != -1 ) {
						return [false, 'unavailable', 'Unavailable'];
					}
					else {
						return [true, 'available', 'Available'];
					}

				},
				onSelect: function( date ) {
					window.location = booknow + '?apartment_id=' + apartment_id + '&checkin=' + encodeURIComponent(date)
				}
			};

			jQuery(this).datepicker( options )
		})
	}

	jQuery( '.tvr_widget_calendar select[name="apartment_id"]' ).on('change', function(){
		var form = jQuery(this).parents('.tvr_widget_calendar');
		form.find( '.full-loader' ).stop(true, true).fadeIn();
		var apartment_id = jQuery(this).val()
		jQuery.ajax({
			url: tvr.ajaxurl,
			type: 'post',
			dataType: 'json',
			async: false,
			data: {
				apartment_id : apartment_id,
				action: 'action_get_apartment_data'
			},
			success: function( response ) {
				var apartment_data = response;

				var new_options = {
					beforeShowDay: function( fullDate ) {
						var date = jQuery.datepicker.formatDate( 'yy-mm-dd', fullDate );
						var now = jQuery.datepicker.formatDate( 'yy-mm-dd', new Date() );

						if( date < now ) {
							return [false, 'past', 'Passed Date'];
						}
						else if ( jQuery.inArray( date, response.unavailable ) != -1 ) {
							return [false, 'unavailable', 'Unavailable'];
						}
						else {
							return [true, 'available', 'Available'];
						}

					},
				onSelect: function( date ) {
					window.location = booknow + '?apartment_id=' + apartment_id + '&checkin=' + encodeURIComponent(date)
				}

				};
				form.attr('data-apartment_id', apartment_id )
				form.find('.availability-calendar').datepicker( "option", new_options );
				form.find('.availability-calendar').datepicker( "refresh" );
				form.find( '.full-loader' ).stop(true, true).fadeOut();

			}
		});


	})


	options = {
		unavailable: jQuery.parseJSON( jQuery( '#booknow-form #unavailable' ).html() ),
		maximum_guests: parseInt( jQuery( '#booknow-form #maximum_guests' ).html() ),
		messages: jQuery.parseJSON( jQuery( '#booknow-form #messages' ).html() )
	}

	if( jQuery( '#booknow-form' ).length > 0 ) {
		jQuery( '#booknow-form' ).tvr_booknow( options );
	}

	if( jQuery( '.booking-widget' ).length > 0 ) {
		jQuery( '.booking-widget' ).tvr_booking_widget();
	}


	jQuery('.imageslider .slides').hide();

	jQuery( '.navigation-select select' ).change( function() {
		window.location = jQuery( this ).val()
	})

	jQuery( '.hoverlink' ).mouseenter( function() {
		jQuery(this).find( 'img' ).stop().animate({ opacity: '0.4' })
	})

	jQuery( '.hoverlink' ).mouseleave( function() {
		jQuery(this).find( 'img' ).stop().animate({ opacity: '1' })
	})

	jQuery( '.tabs nav li' ).click( function() {
		var id = jQuery( this ).attr( 'data-tab' );
		jQuery( this ).parent().find( 'li' ).removeClass( 'current' );
		jQuery( this ).addClass( 'current' );

		jQuery( this ).parents( '.tabs:first' ).find( '.tab' ).hide();
		jQuery( this ).parents( '.tabs:first' ).find( '.tab[data-tab="' + id + '"]' ).show();

	})

	jQuery( '.toggle-title' ).click( function() {
		var toggle = jQuery( this ).parent()
		var animation = toggle.attr('data-animation');
		var animation_speed = toggle.attr( 'data-animation_speed' );

		if( animation == 'slide' ) {
			jQuery(this).next().slideToggle( animation_speed, function(){
				if( toggle.hasClass( 'open' ) ) {
					toggle.removeClass( 'open' ).addClass( 'closed' )
				}
				else {
					toggle.addClass( 'open' ).removeClass( 'closed' )
				}

			} )
		}
		else {
			jQuery(this).next().toggle()
			if( toggle.hasClass( 'open' ) ) {
				toggle.removeClass( 'open' ).addClass( 'closed' )
			}
			else {
				toggle.addClass( 'open' ).removeClass( 'closed' )
			}

		}
	})

	jQuery( '.mashup-page .pagination .page-numbers' ).live( 'click', function() {
		var posts = jQuery( this ).parents( '.posts:first' )
		var args = posts.attr( 'data-args' )
		var id = posts.attr( 'data-id' )
		var type = posts.attr( 'data-type' )
		var layout = posts.attr( 'data-layout' )
		var page = parseInt( jQuery( this ).text() )
		var container = posts.parent();

		jQuery.scrollTo( posts, 200, {
			offset: -200,
			axis: 'y',
			onAfter: function() {
				container.css( 'height', container.outerHeight() + 'px' );
				posts.fadeOut( function() {
					container.prepend( '<div class="loader"></div>' );
					jQuery.ajax({
						url: tvr.ajaxurl,
						type: 'post',
						data: {
							args: args,
							page: page,
							layout: layout,
							action: 'load_posts',
							type : type,
						},
						success: function( response ) {
							container.find( '.loader' ).remove()
							posts.replaceWith( response );
							container.css( 'height', 'auto' );
							posts.fadeIn()
						}
					})

				})
			}
		});

		return false;
	})



	jQuery("select, input:not([type='submit']), button, textarea").uniform();




	jQuery('.gallery').imagesLoaded( function() {
		jQuery( '.gallery .loader' ).fadeOut( function() {
			jQuery( '.gallery .inner' ).show();

			jQuery( '.gallery' ).isotope({
				itemSelector : '.gallery-item',
				layoutMode: 'fitRows',
			});
		})
	})


	jQuery(window).smartresize( function(){

			jQuery( '.gallery' ).isotope({
				itemSelector : '.gallery-item',
				layoutMode: 'fitRows',
			});




		apartment_list.isotope({
			masonry: {
				columnWidth: jQuery('.content-col').width(),
			}
		});


		show_navigation();
	});


	jQuery( '.gallery-filter-link' ).click( function() {
		gallery_filter = jQuery( this ).parents( '.gallery-filter' );
		gallery_filter.find( '.gallery-filter-link' ).addClass( 'destroyed' )
		jQuery( this ).removeClass( 'destroyed' )
		cat_id = jQuery( this ).attr( 'data-filter' )

		var gallery = gallery_filter.next()
		gallery.isotope({ filter:  cat_id });
		return false;
	})


	/***********************************************/
	/*              Apartment Filter               */
	/***********************************************/

	jQuery( window ).load( function() {
		amount_input = jQuery( ".apartment-filters .amount")
		range_min = parseFloat( amount_input.attr( 'data-min' ) );
		range_max = parseFloat( amount_input.attr( 'data-max' ) );
		currency = amount_input.attr('data-currency');
		currency_position = amount_input.attr( 'data-currency_position' )

		if( jQuery( ".apartment-filters" ).length > 0 ) {
			jQuery( '.apartment-filter-form .loader' ).fadeOut( function() {;
				jQuery( '.apartment-filter-form .inner' ).fadeIn();
			    apartment_price_range = jQuery( ".apartment-filters .slider-range" ).slider({
			        range: true,
			        min: range_min,
			        max: range_max,
			        values: [ range_min, range_max ],
			        slide: function( event, ui ) {
			        	if( currency_position == 'before' ) {
				            jQuery( ".amount" ).val( currency + ui.values[ 0 ] + " - " + currency + ui.values[ 1 ] );
			        	}
			        	else {
				            jQuery( ".amount" ).val(  ui.values[ 0 ] + currency + " - " + ui.values[ 1 ] + currency );
			        	}
			        },
			        stop: function( e, data ) {
				        filter_apartments()
			        }
			    });

	        	if( currency_position == 'before' ) {
			    	jQuery( ".amount" ).val( currency + jQuery( ".slider-range" ).slider( "values", 0 ) +
			        " - " + currency + jQuery( ".slider-range" ).slider( "values", 1 ) );
	        	}
	        	else {
			   	 jQuery( ".amount" ).val( jQuery( ".slider-range" ).slider( "values", 0 ) + currency +
			        " - " + jQuery( ".slider-range" ).slider( "values", 1 ) + currency );
	        	}

				sorters = {};

				fields = jQuery( '.apartment-list' ).attr( 'data-sort_fields' );
				fields = fields.split( ',' )

				jQuery.each( fields, function() {
					var field = this;
					sorters[field] = function( element ) {
						value = element.find('.' + field).text();
						if( (typeof(value) === 'number' || typeof(value) === 'string') && value !== '' && !isNaN(value) ) {
							return parseFloat( element.find('.' + field).text() );
						}
						else {
							return element.find('.' + field).text() ;
						}
					};
				})

				apartment_list = jQuery( '.apartment-list' ).isotope({
					itemSelector : '.apartment',
					getSortData : sorters
				});
			})

		}
	})

	jQuery('.apartment-filter-form .orderby').change( function() {
		var orderby = jQuery( this ).val();
		var order = jQuery('.apartment-filter-form .order option:selected').val()
		sort_apartments( orderby, order )
	})

	jQuery('.apartment-filter-form .order').change( function() {
		var order = jQuery( this ).val();
		var orderby = jQuery('.apartment-filter-form .orderby option:selected').val()
		sort_apartments( orderby, order )
	})

	jQuery( '.tvr_apartment_type, .tvr_amenity' ).click( function() {
		filter_apartments();
	})

	jQuery( ".apartment-filters .search input" ).keyup( function() {
		filter_apartments();
	})

	jQuery( "select.tvr_apartment_detail").change( function() {
		filter_apartments();
	})





})


/***********************************************/
/*               Apartment Slider              */
/***********************************************/


rfas_current = 0;

jQuery(window).load(function() {
	var speed = jQuery( '#rfas-container' ).attr('data-speed')
	if( jQuery( '.flexslider' ).length > 0 ) {
		rfas_count = jQuery( '#rfas-nav li' ).length;
		rfas_nav_width = jQuery( '#rfas-nav' ).width();
		rfas_item_width = jQuery( '#rfas-thumb-0' ).outerWidth() - 2 + 11;
		rfas_slides_width = rfas_item_width * rfas_count

		rfas = jQuery('.flexslider').flexslider({
			animation: "slide",
			direction: 'horizontal',
			slideshow: false,
			itemWidth: 142,
			itemMargin: 11,
			directionNav: false,
			start: function(slider) {
				if (slider.pagingCount == 1) slider.addClass('centered');
			}
		});

		sliderLoop = setInterval( 'rfas_sliderLoop()', speed );
	}

});

jQuery( document ).ready( function() {
	jQuery(window).smartresize( function() {
		rfas_nav_width = jQuery( '#rfas-nav' ).width();
		var total_pages = jQuery( '#rfas-nav .flex-control-paging li').length;
		if( total_pages === 0 ) {
			jQuery( '#rfas-nav' ).addClass('centered')
		}
		else {
			jQuery( '#rfas-nav' ).removeClass('centered')
		}
	})

	jQuery('#rfas-windows, #rfas-nav').mouseenter( function() {
		clearInterval( sliderLoop )
	})

	jQuery('#rfas-windows, #rfas-nav').mouseleave( function() {
		var speed = jQuery( '#rfas-container' ).attr('data-speed')
		sliderLoop = setInterval( 'rfas_sliderLoop()', speed );
	})

	jQuery( '#rfas-nav li' ).click( function() {
		id = parseFloat( jQuery( this ).attr('data-id') );
		jQuery( '#rfas-nav li' ).removeClass( 'current' );
		jQuery(this).addClass( 'current' )
		rfas_advanceSlider( id )
	})


	jQuery( '#rfas-next' ).click( function() {
		next = rfas_get_next_item_id()
		rfas_advanceSlider( next )
	})

	jQuery( '#rfas-prev' ).click( function() {
		prev = rfas_get_previous_item_id()
		rfas_advanceSlider( prev )
	})

})

function rfas_advanceSlider( next ) {
	var current = rfas_current;
	var animateTo = rfas_get_animate_to_target( next )

	rfas_switch_image( next )

	jQuery( '#rfas-thumb-' + current ).removeClass( 'current' );
	jQuery( '#rfas-thumb-' + next ).addClass( 'current' );

	rfas.flexslider( animateTo )
	rfas_current = next;
}

function rfas_sliderLoop() {
	var next = rfas_get_next_item_id();
	rfas_advanceSlider( next )
}

function rfas_switch_image( element ) {


	var transitionSpeed = parseFloat( jQuery( '#rfas-container' ).attr( 'data-transition_speed' ) );
	current_next = jQuery( '#rfas-current .invisible' );
	current_current = jQuery( '#rfas-current .visible' );

	current_src = jQuery( '#rfas-image-cache-' + element ).attr( 'src' );
	current_next.attr( 'src', current_src );


	prev_element = rfas_get_previous_item_id( element )
	prev_next = jQuery( '#rfas-prev .invisible' );
	prev_current = jQuery( '#rfas-prev .visible' );

	prev_src = jQuery( '#rfas-image-cache-' + prev_element ).attr( 'src' );
	prev_next.attr( 'src', prev_src );


	next_element = rfas_get_next_item_id( element )
	next_next = jQuery( '#rfas-next .invisible' );
	next_current = jQuery( '#rfas-next .visible' );

	next_src = jQuery( '#rfas-image-cache-' + next_element ).attr( 'src' );
	next_next.attr( 'src', next_src );

	current_current.fadeOut( transitionSpeed, function() {
		current_current.removeClass( 'visible' ).addClass( 'invisible' )
		current_next.removeClass( 'invisible' ).addClass( 'visible' )
		current_current.remove();
		current_next.after( '<img src="" class="invisible">' );
	});

	prev_current.fadeOut( transitionSpeed, function() {
		prev_current.removeClass( 'visible' ).addClass( 'invisible' )
		prev_next.removeClass( 'invisible' ).addClass( 'visible' )
		prev_current.remove();
		prev_next.after( '<img src="" class="invisible">' );
	});

	next_current.fadeOut( transitionSpeed, function() {
		next_current.removeClass( 'visible' ).addClass( 'invisible' )
		next_next.removeClass( 'invisible' ).addClass( 'visible' )
		next_current.remove();
		next_next.after( '<img src="" class="invisible">' );
	});


}

function rfas_get_animate_to_target( element ) {
	var position = jQuery( '#rfas-thumb-' + element ).position();
	var edge = position.left + rfas_item_width;

	var total_pages = jQuery( '#rfas-nav .flex-control-paging li').length;
	var current_page = jQuery( '#rfas-nav .flex-control-paging li a.flex-active').parent().index()
	if( current_page == -1) {
		current_page = 0;
	}

	if( edge > rfas_nav_width || edge < 0 ) {
		if( current_page >= ( total_pages - 1 ) ) {
			page = 0;
		}
		else {
			page = current_page + 1;
		}
	}
	else {
		page = current_page;
	}



	return page;
}

function rfas_get_next_item_id( element ) {
	var next;

	if( element == '' || typeof element == 'undefined' ) {
		var current = rfas_current;
	}
	else {
		current = element;
	}
	if( element == 0 ) {
		next = 1;
	}
	else {
		var next = current + 1;
		if( next > ( rfas_count - 1 ) ) {
			next = 0;
		}
	}
	return next;
}

function rfas_get_previous_item_id( element ) {
	var previous;

	if( element == '' || typeof element == 'undefined' ) {
		var current = rfas_current;
	}
	else {
		current = element;
	}
	if( element == 0 ) {
		previous = rfas_count - 1;
	}
	else {
	var previous = current - 1;
	if( previous < 0 ) {
		previous = rfas_count - 1;
	}
	}
	return previous;
}











jQuery.expr[':'].Contains = function(a, i, m) {
  return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
};
jQuery.expr[':'].contains = function(a, i, m) {
  return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
};


function filter_apartments() {

	var apartments = [];

	apartments[0] = get_type_filter_apartments();

	apartments[1] = get_amenity_filter_apartments();

	apartments[2] = get_price_filter_apartments();

	apartments[3] = get_search_filter_apartments();

	apartments[4] = get_detail_filter_apartments();

	final_apartments = []
	count = 0
	jQuery.each( apartments, function( filter ) {
		if( this != '' ) {
			if( this != null ) {
				final_apartments = final_apartments.concat( this )
			}
			count++;
		}
	})

	apartments = final_apartments
	frequencies = get_frequencies( apartments )
	apartments = frequencies[0];
	frequencies = frequencies[1]

	final_apartments = [];
	for( i=0; i<frequencies.length; i++ ) {
		if( frequencies[i] >= count ) {
			final_apartments.push( '.apartment-' + apartments[i] );
		}
	}

	final_selectors = final_apartments.join(',');
	if( final_selectors == '' ) {
		final_selectors = '#impossible-selection.impossible';
	}
	apartment_list.isotope({ filter:  final_selectors })
}


function get_frequencies(arr) {
    var a = [], b = [], prev;

    arr.sort();
    for ( var i = 0; i < arr.length; i++ ) {
        if ( arr[i] !== prev ) {
            a.push(arr[i]);
            b.push(1);
        } else {
            b[b.length-1]++;
        }
        prev = arr[i];
    }

    return [a, b];
}


function get_detail_filter_apartments() {
	var apartments = [];
	var apartment = {};

	var details = {};
	var target_count = 0;
	jQuery.each( jQuery( '.tvr_apartment_detail' ), function(){
		name = jQuery(this).attr('name');
		details[name] = parseFloat( jQuery(this).val() );
		target_count++;
	})


	jQuery.each( jQuery('.apartment'), function( i, element ) {
		item = jQuery(this)
		apartment_id = element.getAttribute( 'data-id' );
		apartment[apartment_id] = {}
		jQuery.each( details, function( key ) {
			value = parseFloat( item.attr('data-' + key ) );
			if( typeof value == 'undefined' || isNaN( value ) ){
				value = 0;
			}
			apartment[apartment_id][key] = value
		})
	})

	var count = 0;
	jQuery.each( apartment, function( apartment_id, data ) {
		count = 0;
		jQuery.each( details, function( name, value )  {
			if( data[name] >= value ) {
				count++;
			}
		})
		if( count >= target_count ) {
			apartments.push( apartment_id )
		}
	})

	if( apartments.length == 0 ) {
		apartments = null;
	}

	return apartments;
}

function get_search_filter_apartments() {
	var search = jQuery( '.apartment-filters .search input' ).val();
	var selectors = '';
	var apartments = [];

	if( search.length > 0 ) {
		selectors = [];
		var results = jQuery( '.apartment .title:contains("' + search + '"), .apartment .content:contains("' + search + '")' );
		temp_array = []
		jQuery.each( jQuery( results ), function( index ) {
			id =  jQuery( this ).parents( '.apartment' ).attr('data-id');
			if( jQuery.inArray( id, temp_array ) == -1 ) {
				apartments.push( id );
			}
			temp_array[index] = id;
		})
		if( apartments.length == 0 ) {
			apartments = null;
		}

	}
	return apartments
}

function get_price_filter_apartments() {
	var price_range = apartment_price_range.slider( 'values' )
	var price_selectors = []
	var apartments = [];

	jQuery.each( jQuery( '.apartment' ), function( index ){
		current_price = parseFloat( jQuery(this).find( '.current_price:first' ).text() )
		if( current_price >= price_range[0] && current_price <= price_range[1] ){
			 apartments.push( jQuery(this).attr('data-id') );
		}
	})

	if( apartments.length == 0 ) {
		apartments = null;
	}

	return apartments

}

function get_type_filter_apartments() {

	var type = [];
	var apartments = [];
	var checked = jQuery('input[name="apartment_type[]"]:checked');
	if( checked.length > 0 ) {
		apartments = [];
		checked.each(function(i){
			type[i] = '.type-' + jQuery(this).val();
		});

		var types = '.apartment' + type.join( ', .apartment' );
		jQuery( types ).each(function(){ apartments.push( jQuery(this).attr('data-id') ) })

		if( apartments.length == 0 ) {
			apartments = null;
		}
	}

	return apartments
}

function get_amenity_filter_apartments() {

	var amenity = [];
	var apartments = [];
	var checked = jQuery('input[name="apartment_amenity[]"]:checked');
	if( checked.length > 0 ) {
		checked.each(function(i){
			amenity[i] = '.amenity-' + jQuery(this).val();
		});
		var amenities = amenity.join( '' );

		jQuery( '.apartment' + amenities ).each(function() { apartments.push( jQuery(this).attr('data-id') ) })

		if( apartments.length == 0 ) {
			apartments = null;
		}
	}
	return apartments

}




function sort_apartments( orderby, order ){
	var args = {
		sortBy: orderby ,
		sortAscending : true
	};
	if( order == 'desc') {
		args['sortAscending'] = false;
	}
	apartment_list.isotope(args);
}




function show_navigation() {
	var available_menu_width = jQuery( '#site-header .navigation:visible' ).width() - jQuery( '#site-header .site-logo' ).width() - 44;

	if( available_menu_width < menu_width ) {
		jQuery('#site-header .navigation-menu').hide()
		jQuery('#site-header .navigation-select').show()
	}
	else {
		jQuery('#site-header .navigation-menu').show()
		jQuery('#site-header .navigation-select').hide()
	}

}