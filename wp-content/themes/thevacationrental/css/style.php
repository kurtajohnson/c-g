<?php
	global $framework;
	$styler = new EB_Styler();
	$primary_color = $framework->options['primary_color'];
	$secondary_color = $framework->options['secondary_color'];

	// Determine Images Used More Than Once
	$site_content_background_image = $framework->options[ 'site_content_background_image' ];
	if( !empty( $site_content_background_image ) ) {
	$site_content_background_image = ( is_numeric( $site_content_background_image ) )
		? wp_get_attachment_image_src( $site_content_background_image )
		: array( $site_content_background_image );
	}

	$site_footer_background_image = $framework->options['site_footer_background_image' ];
	if( !empty( $site_footer_background_image ) ) {
	$site_footer_background_image = ( is_numeric( $site_footer_background_image ) )
		? wp_get_attachment_image_src( $site_footer_background_image )
		: array( $site_footer_background_image );
	}

?>
html {
	font-family: <?php echo $framework->get_font_display( 'body_font' ) ?>;
	background-color: <?php echo $framework->options['site_footer_background_color'] ?>;
	<?php
		if( !empty( $site_footer_background_image[0] ) ) :
	?>
	background-image: url( <?php echo $site_footer_background_image[0] ?> )
	<?php endif ?>
}

.post h1.title a:hover, .post h1.post-title a:hover {
		color: <?php echo $primary_color ?>

}

.button input {
	font-family: <?php echo $framework->get_font_display( 'body_font' ) ?>;
	color: <?php echo $framework->defaults['shortcode_defaults']['button']['color']  ?>

}

.gallery-layout-default a.read-more {
		color: <?php echo $primary_color ?>

}

h1,h2,h3,h4,h5,h6 {
	font-family: <?php echo $framework->get_font_display( 'heading_font' ) ?>;
}

.tabs nav li {
	font-family: <?php echo $framework->get_font_display( 'heading_font' ) ?>;
}

.content a, .comments a {
	color: <?php echo $primary_color ?>
}

#site-header {
	color: <?php echo $framework->options['site_header_text_color'] ?>;
	height: <?php echo $framework->options['site_header_height'] ?>;
	<?php echo $styler->gradient( $framework->options['site_header_background_color'] ) ?>;
	<?php
		$background_image = $framework->options['site_header_background_image' ];
		if( !empty( $background_image ) ) :
		$image = ( is_numeric( $background_image ) )
			? wp_get_attachment_image_src( $background_image )
			: array( $background_image );
	?>
	background-image: url( <?php echo $image[0] ?> )
	<?php endif ?>
}

	@media (max-width: 768px) {
		#site-header {
			<?php echo $styler->gradient( $framework->options['site_header_background_color'] ) ?>;
			border-bottom:1px solid <?php echo $styler->lightness( $framework->options['site_header_background_color'], '-', '22' ) ?>
		}
	}
	#site-header .navigation a{
		color: <?php echo $framework->options['site_header_link_color'] ?>;
	}

	#site-header .navigation a:hover{
		color: <?php echo $primary_color ?>;
	}


	<?php if( !empty( $framework->options['site_header_navigation_separator'] ) ) : ?>
	#site-header .navigation > div > ul > li {
		background: url(<?php echo $framework->options['site_header_navigation_separator'] ?>) no-repeat left center;
	}
	#site-header .navigation > div > ul > li:first-child {
		background:none;
	}

	<?php endif ?>


	#site-header .navigation > div > ul > li {
		<?php
			$background_image = $framework->options( 'site_header_navigation_separator' );
			if( !empty( $background_image ) ) :
			$image = ( is_numeric( $background_image ) )
				? wp_get_attachment_image_src( $background_image )
				: array( $background_image );
		?>
		background-image: url( <?php echo $image[0] ?> );
		<?php endif ?>
	}

	#site-header .navigation > div > ul > li.current_page_ancestor > a, #site-header .navigation > div > ul > li.current-menu-ancestor > a, #site-header .navigation > div > ul > li.current_page_item > a, #site-header .navigation > div > ul > li.current-menu-item > a{
		color: <?php echo $primary_color ?>
	}

	#site-header .navigation > div > ul > li ul li.current_page_item > a, #site-header .navigation > div > ul > li ul li.current-menu-item > a{
		color: <?php echo $primary_color ?>
	}


	.widget > div > ul > li.current_page_ancestor > a, .widget > div > ul > li.current-menu-ancestor > a, .widget > div > ul > li.current_page_item > a, .widget > div > ul > li.current-menu-item > a{
		color: <?php echo $primary_color ?> !important;
	}

	.widget > div > ul > li ul li.current_page_item > a, .widget > div > ul > li ul li.current-menu-item > a{
		color: <?php echo $primary_color ?> !important
	}



.link-pages a {
	color:inherit;
}




.post-layout-binder a {
	color: <?php echo $primary_color ?>
}

.post-layout-thumbnail a {
	color: <?php echo $primary_color ?>
}

.post-layout-simple a {
	color: <?php echo $primary_color ?>
}

#site-container {
	background-color: <?php echo $framework->options['site_content_background_color'] ?>;
	<?php
		if( !empty( $site_content_background_image[0] ) ) :
	?>
	background-image: url( <?php echo $site_content_background_image[0] ?> )
	<?php endif ?>
}

#site-footer {
	color: <?php echo $framework->options['site_footer_text_color'] ?>;
}
	#site-footer a{
		color: <?php echo $framework->options['site_footer_link_color'] ?>;
	}

	#site-footer a:hover{
		color: <?php echo $primary_color ?>;
	}

	#site-footer h1, #site-footer h2, #site-footer h3, #site-footer h4, #site-footer h5, #site-footer h6 {
		color: <?php echo $framework->options['site_footer_heading_color'] ?>;
	}


	.widget.scheme-footer {
		background-color: <?php echo $framework->options['site_footer_background_color'] ?>;
		<?php
			if( !empty( $site_footer_background_image[0] ) ) :
		?>
		background-image: url( <?php echo $site_footer_background_image[0] ?> )
		<?php endif ?>
	}

	#site-footer .widget.scheme-footer {
		background:none;
	}

	#site-footer .widget ul li {
		border-color: <?php echo $framework->options['site_footer_list_separator_color'] ?>;
	}


#site-container .widget {
	color: <?php echo $framework->options['site_sidebar_text_color'] ?>;
	background-color: <?php echo $framework->options['site_sidebar_background_color'] ?>;
}

	#site-container .widget h1,#site-container .widget h2, #site-container .widget h3,#site-container .widget h4,#site-container .widget h5, #site-container .widget h6 {
		color: <?php echo $framework->options['site_sidebar_heading_color'] ?>;
	}

	#site-container .widget a {
		color: <?php echo $framework->options['site_sidebar_link_color'] ?>;
	}

	#site-container .widget a:hover {
		color: <?php echo $primary_color ?>
	}

	#site-container .widget ul li {
		border-color: <?php echo $framework->options['site_sidebar_list_separator_color'] ?>;
	}


.read-more-arrow {
	background-color: <?php echo $primary_color ?>
}



.hoverlink {
	<?php
		$background_image = $framework->options['hoverlink'];
		if( !empty( $background_image ) ) :
		$image = ( is_numeric( $background_image ) )
			? wp_get_attachment_image_src( $background_image )
			: array( $background_image );
	?>
	background-image: url( <?php echo $image[0] ?> );
	<?php endif ?>
}
