<?php
/** Blueprint
  *
  * The blueprint is the theme framrwork's utility for generating
  * consistent and reliable pages. It is used to display common
  * elements for all pages (title, featured image, comments) and
  * it determines which specific content it needs to include.
  *
  * It also handles sidebar details and other display specific
  * things.
  *
  * @package The Vacation Rental
  *
  */

get_header();

if( !empty( $framework->options['home_apartment_id'] ) AND is_numeric($framework->options['home_apartment_id']) AND is_home() ) {
	$apartment = get_post( $framework->options['home_apartment_id'] );
	query_posts( array(
		'page' => '',
		'tvr_apartment' => $apartment->post_name,
		'post_type' => 'tvr_apartment',
		'name' => $apartment->post_name,
	));
}


$no_sidebar = array( 'template-apartmentlist' );
global $blueprint, $wp_query;
	if( have_posts() AND !is_archive() AND !is_search() AND !is_home() ) {
		while ( have_posts() ) { the_post(); $blueprint->add_data(); $blueprint->master_page = $post;
			echo '<div class="row blueprint" id="' . $blueprint->blueprint_template( 'name' ) . '">';
			if( $blueprint->blueprint_has_header() ) {
				echo '<div class="row"><div class="twelvecol">';

					if( $blueprint->has_title() ) {
						echo do_shortcode( '[title style="margin:11px 0 44px 0" text="' . the_title( '', '', false ) . '"]' );
					}

					if( $blueprint->has_thumbnail() ) {
						echo '<div class="featured-image">';
						the_post_thumbnail( 'rf_col_1', array( 'container_class' => 'box smallpadding' ) );
						echo '</div>';
					}

					if( $blueprint->has_post_content() AND !in_array( $framework->get_post_type(), array( 'post', 'page', 'tvr_apartment' ) ) ) {
						echo '<div ' . $blueprint->boxed_class( 'content' ) . '>';
						the_content();
						echo '</div>';
					}

				echo '</div></div>';
			}

			echo '<div class="row">';
				$blueprint->blueprint_content( $no_sidebar );
			echo "</div>";


		}

		echo '</div>';

	}
	elseif( have_posts() AND ( is_archive() OR is_search() OR is_home() ) ) {
		echo '<div class="row blueprint">';

			echo do_shortcode( '[title style="margin:0 0 44px 0" text="' . $blueprint->get_page_title() . '"]' );

			$blueprint->blueprint_content();

		echo '</div>';
	}
	else {
		$blueprint->show_no_posts();
	}

get_footer();

?>
