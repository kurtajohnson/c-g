<?php
/** Gallery Template
  *
  * This file is use to create a galllery page. Gallery pages
  * are much like post lists in that a user can specify what
  * posts to use, in what order, etc. They differ in look and
  * handling.
  *
  * Gallery pages can be filtered using controls at the top.
  * By default the layout and filtering is handed via
  * Isotope.
  *
  * @package Elderberry
  *
  */

global $blueprint, $post, $paged, $parent;

$parent = $post;
$args = $blueprint->get_postlist_args( $parent->postmeta );
$postlist = new WP_Query( $args );


if( $postlist->have_posts() ) {


	$category = @unserialize( $post->postmeta['category'] );

	if( !empty( $post->postmeta['category'] ) AND is_array( $category ) AND count( $category ) > 1 ) {
		echo '<div class="gallery-filter">';
		echo do_shortcode('[button url="#" parameters="data-filter=*" classes="gallery-filter-link" text="Show All" background="secondary" gradient="yes" radius="22px" ]');

		foreach( $category as $cat ) {
			echo do_shortcode('[button url="#" parameters="data-filter=.category-' . $cat . '" classes="destroyed gallery-filter-link" text="' . get_cat_name( $cat ) . '" background="secondary" gradient="yes" radius="22px" ]');
		}

		echo '</div>';
	}


	echo '<div data-col="' . $post->postmeta['columns'] . '" class="gallery col-' . $post->postmeta['columns'] . ' posts-container" ><div class="loader"></div>
		<div class="inner posts"
			data-paged="' . $paged . '"
			data-type="postlist"
			data-layout="' . $parent->postmeta['layout'] . '"
			data-args="' . urlencode( serialize( $args ) ) . '"
		>';

	while( $postlist->have_posts() ) {
		$postlist->the_post(); $blueprint->add_data();
		$blueprint->layout_template( 'gallery', $parent->postmeta['layout'] );
	}


	echo '</div></div>';

}
else {
	$blueprint->show_no_posts();
}


$post = $parent;
?>