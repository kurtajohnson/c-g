<?php
/** Single Page
  *
  * This file is used to display the contents of
  * single pages
  *
  * @package Elderberry
  *
  */

  global $blueprint;
?>
<div <?php echo $blueprint->boxed_class( 'content', 'content' ) ?>>
<?php the_content() ?>
</div>