<?php
	global $framework, $theme_options, $blueprint, $post;
?>
<div <?php post_class( 'single' ) ?>>

	<?php
		if( ( isset( $post->postmeta['show_slider'] ) AND $post->postmeta['show_slider'] == 'yes' ) OR !isset( $post->postmeta['show_slider'] ) ) {
			get_template_part( 'modules/slider', 'apartment' );
	}
	?>

	<?php ob_start() ?>
		<div <?php echo $blueprint->boxed_class( 'content' ) ?> >
			<?php the_content() ?>
		</div>
	<?php $content = ob_get_clean() ?>

	<div class='row'>
		<?php $blueprint->generate_pushed_content( $content ) ?>
	</div>



</div>
