<?php
/** Book Now Page
  *
  * This file is used to display a book now form. The form
  * enables the user to send a booking request.
  *
  * @package The Vacation Rental
  *
  */
	global $framework, $blueprint, $post, $apartments;

?>



<?php if( !empty( $_GET['complete'] ) AND $_GET['complete'] == 'true') : ?>



<?php else :
	$apartment_id = ( empty( $_GET['apartment_id'] ) ) ? '' : $_GET['apartment_id'];

	$apartment = false;
	$unavailable_dates = '';
	$maximum_guests = '';
	$minimum_stay = '';
	if( !empty( $apartment_id ) ) {
		$apartment = new TVR_Apartment( $apartment_id );
		$apartment_id = ( !empty( $apartment->ID ) ) ? $apartment->ID : '';
		$unavailable_dates = json_encode( $apartment->get_unavailable_dates() );
		$maximum_guests = $apartment->maximum_guests;
		$minimum_stay = ( !empty( $apartment->data->minimum_stay ) ) ? $apartment->data->minimum_stay : 1;
	}

	$checkin['value'] = ( empty( $_GET['checkin'] ) ) ? '' : $_GET['checkin'];
	$checkin['disabled'] = empty( $apartment_id ) ? 'disabled="disabled"' : '';

	$checkout['value'] = ( empty( $_GET['checkout'] ) ) ? '' : $_GET['checkout'];
	$checkout['disabled'] = empty( $apartment_id ) ? 'disabled="disabled"' : '';

	$guests['value'] = ( empty( $_GET['guests'] ) ) ? '' : $_GET['guests'];
	$guests['disabled'] = empty( $apartment_id ) ? 'disabled="disabled"' : '';

	$name['value'] = ( empty( $_GET['name'] ) ) ? '' : $_GET['name'];
	$name['disabled'] = empty( $apartment_id ) ? 'disabled="disabled"' : '';

	$email['value'] = ( empty( $_GET['email'] ) ) ? '' : $_GET['email'];
	$email['disabled'] = empty( $apartment_id ) ? 'disabled="disabled"' : '';

	$phone['value'] = ( empty( $_GET['phone'] ) ) ? '' : $_GET['phone'];
	$phone['disabled'] = empty( $apartment_id ) ? 'disabled="disabled"' : '';

	$message['value'] = ( empty( $_GET['message'] ) ) ? '' : $_GET['message'];
	$message['disabled'] = empty( $apartment_id ) ? 'disabled="disabled"' : '';


	$messages = array(
		'select_apartment'    => $post->postmeta['select_apartment'],
		'contact_details'     => $post->postmeta['contact_details'],
		'select_date'         => $post->postmeta['select_date'],
		'invalid_range'       => $post->postmeta['invalid_range'],
		'invalid_guests'      => $post->postmeta['invalid_guests'],
	);




?>




	<form id='booknow-form' method='post' data-minimum_stay='<?php echo $minimum_stay ?>' action='<?php echo admin_url( 'admin-ajax.php' ) ?>'>
		<div class='loader'></div>

		<div id='messages' class='hidden'><?php echo json_encode( $messages ) ?></div>

		<div id="unavailable" class='hidden'><?php echo $unavailable_dates ?></div>
		<div id="maximum_guests" class='hidden'><?php echo $maximum_guests ?></div>

		<div id="message" class='hidden form-message'>
			<?php echo do_shortcode('[message background="red"][/message]') ?>
		</div>

		<?php if( empty( $apartment_id ) ) : ?>
		<div class="form-message" data-type='select_apartment'>
			<?php echo do_shortcode('[message background="red"]' . $post->postmeta['select_apartment'] . '[/message]') ?>
		</div>
		<?php endif ?>

		<div class='row'>
			<div class='threecol'>
				<p>
				<label>Apartment</label>
				<select id='apartment_id' name='apartment_id'>
			<?php
				$apartment_list = $apartments->get_apartment_dropwdown_list();
				echo '<option value="">-- Select an Apartment --</option>';
				foreach( $apartment_list as $title => $value ) {
					$selected = ( $value == $apartment_id ) ? 'selected="selected"' :'';
					echo '<option ' . $selected . ' value="' . $value . '">' . $title . '</option>';
				}
			?>
				</select>
				</p>
			</div>
			<div class='threecol'>
				<p>
				<label>Check In</label>
				<input type='text' name='checkin' value='<?php echo $checkin['value'] ?>' <?php echo $checkin['disabled'] ?> >
				</p>
			</div>
			<div class='threecol'>
				<p>
				<label>Check Out</label>
				<input type='text' name='checkout' value='<?php echo $checkout['value'] ?>' <?php echo $checkout['disabled'] ?> >
				</p>
			</div>
			<div class='threecol last guests-selector'>
				<p>
				<label>Guests</label>

					<select <?php echo $guests['disabled'] ?> name='guests'>
						<?php for( $i=1; $i<= get_post_meta( $apartment_id, 'maximum_guests', true); $i++ ) :
							$selected = ( $i == $guests['value'] ) ? 'selected="selected"' : '';
						?>
							<option <?php echo $selected ?> value='<?php echo $i ?>'><?php echo $i ?></option>
						<?php endfor ?>
					</select>
				</p>
			</div>

		</div>



		<div class='row'>
			<div class='threecol'>
				<p>
					<label>Name</label>
					<input type='text' name='name' <?php echo $name['disabled'] ?> value='<?php echo $name['value'] ?>' placeholder='Enter your name'>
				</p>
			</div>
			<div class='threecol'>
				<p>
					<label>Email</label>
					<input type='text' name='email' <?php echo $email['disabled'] ?> value='<?php echo $email['value'] ?>' placeholder='Enter your Email'>
				</p>
			</div>
			<div class='threecol'>
				<p>
					<label>Phone Number</label>
					<input type='text' name='phone' <?php echo $phone['disabled'] ?> value='<?php echo $phone['value'] ?>' placeholder='Enter your Phone Number'>
				</p>
			</div>

			<div class='threecol last'></div>
		</div>

		<div class='row'>
			<div class='twelvecol'>
				<p>
				<label>Message</label>
				<textarea name='message' <?php echo $message['disabled'] ?> ><?php echo $message['value'] ?></textarea>
				</p>
			</div>
		</div>

		<input type='hidden' name='action' value='action_insert_booking'>
		<input type='hidden' name='redirect' value='<?php the_permalink() ?>'>

		<?php echo do_shortcode('[button background="secondary" text="<input type=\'submit\' value=\'Book Now\'>"]') ?>


	</form>

<?php endif ?>