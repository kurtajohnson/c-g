<?php
/** Apartment List
  *
  * This file is used to display a filterable and searchable
  * list of apartments.
  *
  * Sidebars are disabled by default because this page has its own
  * sidebar for filtering and searching. You can filter by price,
  * apartment type, amenities, custom fields and search terms.
  *
  * Apartment types, price, amenities, custom fields can be set up
  * in each apartment's page.
  *
  * @package The Vacation Rental
  *
  */

  global $framework, $blueprint, $post, $apartments;

  if( !empty( $apartments->apartments ) ) :

  $filter_position = $blueprint->get_sidebar_position();
  $content_position = ( $blueprint->get_sidebar_position() == 'right' ) ? 'left' : 'right';

?>


<div class='row'><div class='twelvecol'>
<form class='apartment-filter-form'><div class='loader'></div><div class='inner'>
	<input type='hidden' name='apartment_data' value='<?php echo json_encode( $apartments->apartment_data ) ?>'>

			<div class='threecol filter-col <?php echo $filter_position ?>'>
				<div class='apartment-filters'>
					<div class='filterbox'>
						<div class='filterbox-header'>
							<h2 class='filterbox-title'>Price</h2>
							<div class='filterbox-controls'>
							</div>
						</div>
						<div class='filterbox-content'>
							<?php
								$price_range = $apartments->get_price_range();
							?>
							<p>
							    <label for="amount">Price range:</label>
							    <input type="text" data-currency="<?php echo $framework->options['currency_symbol'] ?>" data-currency_position="<?php echo $framework->options['currency_position'] ?>" class="amount" data-min='<?php echo $price_range[0] ?>' data-max='<?php echo $price_range[1] ?>'>
							</p>

							<div class="slider-range"></div>
						</div>
					</div>
					<div class='filterbox'>
						<div class='filterbox-header'>
							<h2 class='filterbox-title'>Apartment Type</h2>
							<div class='filterbox-controls'>
							</div>
						</div>
						<div class='filterbox-content'>
							<?php
								$types = get_terms( 'tvr_apartment_type' );
								foreach( $types as $type ) :
							?>
								<p>
								<input type='checkbox' name="apartment_type[]" value='<?php echo $type->term_id ?>' class='tvr_apartment_type'> <?php echo $type->name ?>
								</p>
							<?php endforeach ?>
						</div>
					</div>
					<div class='filterbox'>
						<div class='filterbox-header'>
							<h2 class='filterbox-title'>Amenities</h2>
							<div class='filterbox-controls'>
							</div>
						</div>
						<div class='filterbox-content'>
							<?php
								$types = get_terms( 'tvr_amenity' );
								foreach( $types as $type ) :
							?>
								<p>
								<input type='checkbox' name="apartment_amenity[]" value='<?php echo $type->term_id ?>' class='tvr_amenity'> <?php echo $type->name ?>
								</p>
							<?php endforeach ?>
						</div>
					</div>


					<div class='filterbox'>
						<div class='filterbox-header'>
							<h2 class='filterbox-title'>Additional Filters</h2>
							<div class='filterbox-controls'>
							</div>
						</div>
						<div class='filterbox-content'>

							<?php
								$additional_fields = unserialize( $post->postmeta['filter_and_order'] );
								if( !empty( $additional_fields ) ) {
									$details = array();
									foreach( $apartments->apartments as $apartment ) {
										foreach( $apartment->apartment_details as $detail => $value ) {
											$details[$detail][] = $value;
										}
									}
									foreach( $additional_fields as $field ) {
										$field_name = str_replace( ' ', '_', strtolower( $field ) );

										if( !empty( $details[$field] ) ) :
										$details[$field] = array_unique( $details[$field] );
										sort( $details[$field] )
									?>

									<label>Minimum <?php echo $field ?></label>
									<p>
									<select name="<?php echo $field_name ?>" class='tvr_apartment_detail'> <?php echo $field ?>
										<option>0</option>
										<?php foreach( $details[$field] as $value ) : ?>
											<option value='<?php echo $value ?>'><?php echo $value ?></option>
										<?php endforeach ?>
									</select>
									</p>

									<?php
									endif;
									}
								}
							?>
						</div>
					</div>



					<div class='filterbox'>
						<div class='filterbox-header'>
							<h2 class='filterbox-title'>Search</h2>
							<div class='filterbox-controls'>
							</div>
						</div>
						<div class='filterbox-content'>
							<p class='search'>
								<input type='text' placeholder='Enter Keywords'>
							</p>
						</div>
					</div>
				</div>
			</div>

			<div class='content-col ninecol <?php echo $content_position ?>'>
				<?php
					$sort_fields = array(
						'Price'           => 'current_price',
						'Apartment Name'  => 'post_title'
					);
					$additional_fields = @unserialize( $post->postmeta['filter_and_order'] );
					if( !empty( $additional_fields ) ) {
						foreach( $additional_fields as $field ) {
							$field_value = str_replace( ' ', '_', strtolower( $field ) );
							$sort_fields[$field] = $field_value;
						}
					}
				?>

				<select class='orderby'>
					<?php foreach( $sort_fields as $name => $value ) : ?>
					<option value="<?php echo $value ?>"><?php echo $name ?></option>
					<?php endforeach ?>
				</select>

				<select class='order'>
					<option value='asc'>Ascending</option>
					<option value='desc'>Descending</option>
				</select>

				<div class='apartment-list row' data-sort_fields='<?php echo implode( ',', array_values( $sort_fields ) ) ?>'>
				<?php
					global $apartment_id, $apartment;

					foreach( $apartments->apartments as $apartment ) {
						$blueprint->layout_template( 'apartmentlist', $post->postmeta['layout'] );
					}
				?>
				</div>

			</div>


		</div>
		<div class='clear'></div>
</form></div></div>

<?php else : ?>
	<?php $blueprint->show_no_posts() ?>
<?php endif ?>