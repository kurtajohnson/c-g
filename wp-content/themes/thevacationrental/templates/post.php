<?php
/** Single Post
  *
  * This file is used to display the contents of
  * single posts
  *
  * @package Elderberry
  *
  */

  global $blueprint;
?>

<div <?php echo $blueprint->boxed_class( 'content' ) ?>>
<?php the_content() ?>

<div class='single-meta'>
<?php if( has_category() ) : ?>
	Categories: <?php the_category(', ') ?> &nbsp;
<?php endif ?>
<?php if( has_tag() ) : ?>
	Tags: <?php the_tags( '' ) ?>
<?php endif ?>
</div>

</div>

<div class='link-pages'>
<?php wp_link_pages(  ); ?>
</div>