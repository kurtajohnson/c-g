<?php
/** Default Template
  *
  * This file shows the default WordPress archive-type
  * and other similar pages. It is used for loops which
  * contain multiple items.
  *
  * It uses the post template defined by the user.
  *
  * @package Elderberry
  *
  */
global $blueprint, $wp_query, $framework;

while( have_posts() ) {
	the_post();
	$blueprint->layout_template( 'post', $framework->options['post_layout'] );

}
	$blueprint->page_navigation( $wp_query );

?>