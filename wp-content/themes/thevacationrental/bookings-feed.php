<?php

$root = str_replace( '/thevacationrental/bookings-feed.php', '', $_SERVER['SCRIPT_FILENAME'] );
$pos = strrpos( $root, '/' );
$root = substr( $root, 0, $pos );
$pos = strrpos( $root, '/' );
$root = substr( $root, 0, $pos );
include( $root . '/wp-load.php' );

$args = array(
	'post_type' => 'tvr_booking',
	'post_status' => array( 'publish', 'draft' ),
	'posts_per_page' => -1,
);

if( !empty( $_POST['apartment'] ) ) {
	$args['meta_query'] = array(
		array(
			'key' => 'apartment',
			'value' => $_POST['apartment'],
			'compare' => '='
		)
	);
}

$bookings_data = array();

$bookings = new WP_Query( $args );
if( $bookings->have_posts() ) {
	while( $bookings->have_posts() ) {
		$bookings->the_post();
		$stay_interval = get_post_meta( get_the_ID(), 'stay_interval', true );
		$color = ( $post->post_status == 'publish' ) ? '#97b55c' : '#B55D5C';
		$bookings_data[] = array(
			'id' => get_the_ID(),
			'title'=> get_the_title(),
			'start' => date( 'Y-m-d H:i', strtotime( $stay_interval['start_date'] ) ),
			'end' => date( 'Y-m-d H:i', strtotime( $stay_interval['end_date'] ) ),
			'url' => get_edit_post_link( get_the_ID(), array( '' => '&' ) ),
			'color' => $color
		);
	}
}


echo json_encode( $bookings_data );

?>