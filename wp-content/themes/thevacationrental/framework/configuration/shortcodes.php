<?php
/** Elderberry Shortcodes
  *
  * Add code for your shortcodes here
  *
  * @package Elderberry
  * @subpackage The Vacation Rental Admin
  *
  **/


function tvr_shortcode_title( $atts, $content ) {
	global $framework;
	extract( shortcode_atts( array(
		'sample'    => false,
		'margin'    => $framework->defaults['shortcode_defaults']['title']['margin'],
		'text'      => 'My Title',
		'style'     => $framework->defaults['shortcode_defaults']['title']['style'],
	 ), $atts ) );


	// Prepare the style and class display and the output
	$style_array = array(); $class_array = array(); $output = '';

	// Display Documentation
	if ( isset( $sample ) AND !empty( $sample ) ) {
		return $framework->shortcodes->shortcode_documentation( 'title' );
	}

	// Prepare Styles
	$style_array['margin'] = 'margin: ' . $framework->shortcodes->prepare_margin( $margin );
	$style_string = implode( '; ', $style_array );

	$style_string .= "; " . $style;

	// Prepare Classes
	$class_array[] = 'title-box';
	$class_string = implode( ' ', $class_array );

	// Create Output

	$output .= "
		<div class='" . $class_string . "' style='" . $style_string . "'>
			<div class='box nomargin'>
				<div class='binder-left'></div>
				<div class='binder-right'></div>
				<h1>" . $text . "</h1>
			</div>
		</div>
	";

	return $output;

}


		add_shortcode( 'booking_text', 'tvr_shortcode_booking_text' );
		add_shortcode( 'booking_complete', 'tvr_shortcode_booking_complete' );


	function tvr_shortcode_booking_text( $atts, $content ) {
		if( empty( $_GET['complete'] ) ) {
			return do_shortcode( $content );
		}
	}

	function tvr_shortcode_booking_complete( $atts, $content ) {
		if( !empty( $_GET['complete'] ) ) {
			return do_shortcode( $content );
		}
	}



?>