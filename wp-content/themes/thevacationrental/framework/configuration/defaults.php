<?php
/** Elderberry Defaults
  *
  * This file enables you to set up all the defaults,
  * shortcodes, widgets and so on in your theme. See
  * the config.sample.php file in the samples directory
  * for more info and examples.
  *
  * @package Elderberry
  * @subpackage The Vacation Rental Admin
  *
  **/


/** Color Definitions
  *
  * These colors will be available to users wherever
  * they can modify a color for shortcodes and other
  * elements.
  *
  **/
$eb_defaults['colors'] = array(
	'primary'      => '#0C90CC',
	'secondary'    => '#C7E885',
	'red'          => '#E23455',
	'cyan'         => '#139BC1',
	'yellow'       => '#F4D248',
	'black'        => '#4D4D4D',
	'blue'         => '#569CD1',
	'green'        => '#88BF53',
	'purple'       => '#C355BD',
	'orange'       => '#EC9F5F',
);



/** Available Shortcodes
  *
  * Add the name of your shortcode to this array to make it available
  * in the backend. We have a few built-in ones as well:
  * - line
  * - linelink
  * - highlight
  * - state
  * - postlist
  * - postslider
  * - imageslider
  * - button
  * - message
  * - toggle
  * - map
  * - tabs
  *
  * For a description of each, take a look at the documentation.
  *
  **/
$eb_defaults['shortcodes'] = array( 'line', 'linelink', 'highlight', 'state', 'button', 'message', 'imageslider', 'postslider', 'toggle', 'map', 'tabs', 'title', 'page', 'box' );


/** Sidebar Location
  *
  * The default location of the sidebar. Possible values are:
  * - left
  * - right
  *
  **/
$eb_defaults['sidebar_location'] = 'right';

/** Post Types
  *
  * Define the post types you would like to use.
  * You will need to define these post types in the post_types.php
  * file. See post_types.php or post_types.sample.php in the
  * samples directory for more information.
  *
  **/
$eb_defaults['post_types'] = array();

/** Menus
  *
  * Define the menus you would like to use.
  *
  **/
$eb_defaults['menus'] =  array(
	'site_header' => 'Site Header',
	'site_footer' => 'Site Footer',
);

/** Taxonomies
  *
  * Define the post taxonomies you would like to use.
  * You will need to define these taxonomies in the taxonomies.php
  * file. See taxonomies.php or taxonomies.sample.php in the
  * samples directory for more information.
  *
  **/
$eb_defaults['taxonomies'] = array();

/** Sidebars
  *
  * Define the built-in sidebars you would like to use.
  * Sidebars must be given in the following format:
  * 'sidebar_id' => array( 'scheme' => 'your_scheme' )
  *
  * Individual widgets will receive the 'your_scheme' value
  * as a class, enabling you to create different color schemes
  * for different sidebars
  *
  **/
$eb_defaults['sidebars'] = array(
	'Sidebar' => array( 'scheme' => 'content' ),
	'Footerbar' => array( 'scheme' => 'footer' ),
);




/** Field Definitions
  *
  * More friequently used fields are separated out for
  * ease of use and brevity here in this file.
  *
  **/
include( 'defaults-fields.php' );


/** Shortcode Defaults
  *
  * Add the default values for shortcode attributes. Defining
  * a shortcode and its attributes here will not automatically
  * create the shortcode or make it available. To make a shortcode
  * available for use you must add it to the $eb_defaults['shortcodes']
  * array.
  *
  **/
$eb_defaults['shortcode_defaults'] = array(
	'title'                => array(
		'margin'              => 'normal',
		'text'                => 'My Title',
		'style'               => '',
	),
	'line'                 => array(
		'margin'              => 'normal',
		'color'               => '#dddddd',
		'style'               => 'solid',
		'height'              => '1px',
		'style'               => '',
	),
	'linelink'             => array(
		'margin'              => 'normal',
		'color'               => '#dddddd',
		'style'               => 'solid',
		'height'              => '1px',
		'linktext'            => 'top',
		'url'                 => '#',
		'text_align'          => 'right',
		'style'               => '',
	),
	'highlight'            => array(
		'color'               => '#ffffff',
		'background'          => 'primary',
		'style'               => '',
	),
	'state'                => array(
		'type'                => 'loggedin',
	),
	'button'               => array(
		'margin'              => 'normal',
		'color'               => 'auto',
		'background'          => 'secondary',
		'radius'              => '5px',
		'arrow'               => 'no',
		'border_style'        => 'solid',
		'border_width'        => '1px',
		'border_color'        => 'auto',
		'border_auto_color'   => array( '-', '33' ),
		'outline_style'       => 'solid',
		'outline_width'       => '0px',
		'outline_color'       => 'auto',
		'outline_auto_color'  => array( '-', '22' ),
		'gradient'            => 'yes',
		'shadow'              => 'yes',
		'shadow_value'        => '0px 1px 0px 0px rgba(255,255,255,0.3) inset, 0px 1px 1px 0px rgba(0,0,0,0.2)',
		'url'                 => '',
		'style'               => '',
	),
	'message'              => array(
		'margin'              => 'normal',
		'color'               => 'auto',
		'background'          => 'primary',
		'radius'              => '5px',
		'border_style'        => 'solid',
		'border_width'        => '1px',
		'border_color'        => 'auto',
		'border_auto_color'   => array( '+', '22' ),
		'outline_style'       => 'solid',
		'outline_width'       => '0px',
		'outline_color'       => 'auto',
		'outline_auto_color'  => array( '-', '22' ),
		'gradient'            => 'no',
		'shadow'              => 'yes',
		'shadow_value'        => '0px 1px 1px 0px rgba(0,0,0,0.3) ',
		'style'               => '',
	),
	'imageslider'          => array(
		'margin'              => 'normal',
		'animation'           => 'slide',
		'direction'           => 'horizontal',
		'slideshow_speed'     => 7000,
		'animation_speed'     => 600,
		'pause_on_hover'      => 'yes',
		'height'              => '400px',
		'controls'            => 'yes',
		'show_title'          => 'no',
		'show_description'    => 'no',
		'images'              => 'all',
		'layout'              => 'default',
		'style'               => '',
	),
	'postslider'           => array(
		'margin'              => 'normal',
		'animation'           => 'slide',
		'direction'           => 'horizontal',
		'slideshow_speed'     => 7000,
		'animation_speed'     => 600,
		'pause_on_hover'      => 'yes',
		'controls'            => 'yes',
		'show_title'          => 'no',
		'show_description'    => 'no',
		'smooth_height'       => 'no',
		'images'              => 'all',
		'layout'              => 'default',
		'post_type'           => 'post',
		'post_status'         => 'publish',
		'posts_per_page'      => 10,
		'author'              => '',
		'cat'                 => '',
		'tag'                 => '',
		'offset'              => 0,
		'order'               => 'DESC',
		'orderby'             => 'date',
		'only_thumbnailed'    => 'yes',
		'query_vars'          => '',
		'style'               => '',
	),
	'toggle'               => array(
		'margin'              => 'normal',
		'title'               => 'Toggle This Section',
		'title_level'         => 3,
		'default'             => 'open',
		'animation'           => 'slide',
		'animation_speed'     => 500,
		'style'               => '',
	),
	'map'                  => array(
		'margin'              => 'normal',
		'location'            => 'New York, USA',
		'zoom'                => '10',
		'maptype'             => 'ROADMAP',
		'marker'              => 'yes',
		'height'              => 400,
		'style'               => '',
	),
	'tabs'                 => array(
		'margin'              => 'normal',
		'example'             => '[tabs] [tab title="My Tab"] The Content of this tab [/tab] [tab title="My Second Tab"] The content of the second tab [/tab] [/tabs]',
		'style'               => '',
	),
	'page'                  => array(
		'margin'              => 'none',
		'id'                  => '',
	),
	'box'                  => array(
		'margin'              => 'normal',
		'style'               => '',
	),
);



/** Widgets
  *
  * Define the widgets and the settings you would like to use for them.
  * You will need to extend the WP_Widget class with your new widgets.
  * see the sample eb-custom_widgets.sample.php file for more information,
  * examples and default settings for all available built-in widgets
  *
  **/

$eb_defaults['widgets'] = array(
	'groups' => array (
		'search' => array(
			'tabs' => array(
				'main_settings' => array(
					'guid'        => 'search',
					'id'          => 'search',
					'unregister'  => 'WP_Widget_Search',
					'tab_title'   => 'Search',
					'name'        => 'Search',
					'description' => 'A customizable search widget',
					'width'       => 600,
					'items'       => array(
						'title' => array(
							'guid' => 'title',
							'id' => 'title',
							'control' => array(
								'type' => 'text',
								'default' => 'Search',
								'allow_empty' => true,
								'empty_value' => '',
							),
							'label' => 'Widget Title',
							'help'  => 'The widget title will be shown above the widget'
						),
						'placeholder' => array(
							'guid' => 'placeholder',
							'id' => 'placeholder',
							'control' => array(
								'type' => 'text',
								'default' => 'Enter your search terms',
								'allow_empty' => true,
							),
							'label' => 'Placeholder Text',
							'help'  => 'The text shown in the search input before the user enters a value'
						),
						'background_color' => array(
							'guid' => 'background_color',
							'id' => 'background_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#f1f1f1',
								'allow_empty' => false
							),
							'label' => 'Field Background Color',
							'help'  => 'The background color of the search field'
						),
						'border_color' => array(
							'guid' => 'border_color',
							'id' => 'border_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#dddddd',
								'allow_empty' => false
							),
							'label' => 'Field Border Color',
							'help'  => 'The border color of the search field'
						),
						'icon' => array(
							'guid' => 'icon',
							'id' => 'icon',
							'control' => array(
								'type' => 'upload',
								'default' => get_template_directory_uri() . '/img/defaults/search.png',
								'required' => true,
								'allow_empty' => false
							),
							'label' => 'Search Icon',
							'help'  => 'This image will be used as the icon in the field'
						),
					)
				)
			)
		),


		'apartment_data' => array(
			'tabs' => array(
				'main_settings' => array(
					'guid'        => 'apartment_data',
					'id'          => 'apartment_data',
					'tab_title'   => 'Apartment Data',
					'name'        => 'Customizable Apartment Data',
					'description' => 'A Customisable widget to show apartment data. It will only show up on single apartment pages',
					'width'       => 600,
					'items'       => array(
						'title' => array(
							'guid' => 'title',
							'id' => 'title',
							'control' => array(
								'type' => 'text',
								'default' => 'Apartment Details',
								'allow_empty' => true
							),
							'label' => 'Widget Title',
							'help'  => 'The widget title will be shown above the widget'
						),
						'show_amenities' => array(
							'guid' => 'show_amenities',
							'id' => 'show_amenities',
							'control' => array(
								'type' => 'checkbox',
								'boxes' => array(
									'Show amenities in the widget' => 'yes',
								),
								'default' => 'yes',
								'allow_empty' => true,
								'empty_value' => 'no'
							),
							'label' => 'Show Amenities',
							'help'  => 'Select weather to show the amenities in the widget'
						),
						'show_details' => array(
							'guid' => 'show_details',
							'id' => 'show_details',
							'control' => array(
								'type' => 'checkbox',
								'boxes' => array(
									'Sho the custom apartment details' => 'yes',
								),
								'default' => 'yes',
								'allow_empty' => true,
								'empty_value' => 'no'
							),
							'label' => 'Show Details',
							'help'  => 'Select weather to show the details in the widget'
						),
					)
				)
			)
		),
		'seasonal_pricing' => array(
			'tabs' => array(
				'main_settings' => array(
					'guid'        => 'seasonal_pricing',
					'id'          => 'seasonal_pricing',
					'tab_title'   => 'Seasonal Pricing',
					'name'        => 'Seasonal Pricing Widget',
					'description' => 'A Customisable widget the seasonal pricing for an apartment. It will only show up on apartment pages.',
					'width'       => 600,
					'items'       => array(
						'title' => array(
							'guid' => 'title',
							'id' => 'title',
							'control' => array(
								'type' => 'text',
								'default' => 'Seasonal Pricing',
								'allow_empty' => true
							),
							'label' => 'Widget Title',
							'help'  => 'The widget title will be shown above the widget'
						),
					)
				)
			)
		),

		'booking' => array(
			'tabs' => array(
				'main_settings' => array(
					'guid'        => 'booking',
					'id'          => 'booking',
					'tab_title'   => 'Booking Widget',
					'name'        => 'Booking Widget',
					'description' => 'A widget which allows users to book a stay at an apartment. It detects the apartment it should show automatically',
					'width'       => 600,
					'items'       => array(
						'title' => array(
							'guid' => 'title',
							'id' => 'title',
							'control' => array(
								'type' => 'text',
								'default' => 'Book Now',
								'allow_empty' => true
							),
							'label' => 'Widget Title',
							'help'  => 'The widget title will be shown above the widget'
						),
						'apartment_id' => array(
							'guid' => 'apartment_id',
							'id' => 'apartment_id',
							'control' => array(
								'type' => 'select',
								'options' => 'function',
								'function' => array( 'apartments',  'get_apartment_dropwdown_list' ),
								'search' => true,
								'initial' => array(
									'-- Let the User Select --' => '',
								),
								'default' => '',
								'allow_empty' => true,
							),
							'label' => 'Apartment',
							'help'  => 'Select the apartment this widget is for'
						),
						'autodetect' => array(
							'guid' => 'autodetect',
							'id' => 'autodetect',
							'control' => array(
								'type' => 'checkbox',
								'boxes' => array(
									'Autodetect on single pages?' => 'yes',
								),
								'default' => 'yes',
								'allow_empty' => true,
								'empty_value' => 'no'
							),
							'label' => 'Auto-detect Apartment',
							'help'  => 'If selected and the apartment selector is set to "Let the user select" then this widget will enable the user to select the apartment on any page other than a single apartment page. On a single apartment page this widget will only allow bookings for that apartment'
						),
						'book_now_page' => array(
							'guid' => 'book_now_page',
							'id' => 'book_now_page',
							'control' => array(
								'type' => 'select',
								'options' => 'function',
								'function' =>  array( 'framework', 'get_pages_array' ),
								'function_args' => array( 'exclude' => array(), 'include' => array('template-booknow.php') ),
								'search' => false,
								'initial' => array(
									'-- Select a Book Now Page --' => '',
								),
								'default' => '',
								'allow_empty' => true,
							),
							'label' => 'Book Now Page',
							'help'  => 'Select a "Book Now" page the form should lead to'
						),
						'availability_background_color' => array(
							'guid' => 'availability_background_color',
							'id' => 'availability_background_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#C1DDA6',
								'allow_empty' => false
							),
							'label' => 'Available Background',
							'help'  => 'The background color of the available dates'
						),
						'availability_text_color' => array(
							'guid' => 'availability_text_color',
							'id' => 'availability_text_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#28440D',
								'allow_empty' => false
							),
							'label' => 'Available Text Color',
							'help'  => 'The text color of the available dates'
						),
						'unavailability_background_color' => array(
							'guid' => 'unavailability_background_color',
							'id' => 'unavailability_background_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#B22E27',
								'allow_empty' => false
							),
							'label' => 'Unavailable Background',
							'help'  => 'The background color of the unavailable dates'
						),
						'unavailability_text_color' => array(
							'guid' => 'unavailability_text_color',
							'id' => 'unavailability_text_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#ffffff',
								'allow_empty' => false
							),
							'label' => 'Unavailable Text Color',
							'help'  => 'The text color of the unavailable dates'
						),
						'past_background_color' => array(
							'guid' => 'past_background_color',
							'id' => 'past_background_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#f1f1f1',
								'allow_empty' => false
							),
							'label' => 'Past Dates Background',
							'help'  => 'The background color of past dates'
						),
						'past_text_color' => array(
							'guid' => 'past_text_color',
							'id' => 'past_text_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#4C4841',
								'allow_empty' => false
							),
							'label' => 'Past Dates Text Color',
							'help'  => 'The text color of past dates'
						),

					)
				)
			)
		),

		'calendar' => array(
			'tabs' => array(
				'main_settings' => array(
					'guid'        => 'calendar',
					'id'          => 'calendar',
					'unregister'  => 'WP_Widget_Calendar',
					'tab_title'   => 'Calendar',
					'name'        => 'Calendar',
					'description' => 'A customizable calendar widget',
					'width'       => 600,
					'items'       => array(
						'title' => array(
							'guid' => 'title',
							'id' => 'title',
							'control' => array(
								'type' => 'text',
								'default' => 'Availability Calendar',
								'required' => false,
								'classes' => 'widefat',
							),
							'label' => 'Widget Title',
							'help'  => 'The widget title will be shown above the widget'
						),
						'apartment_id' => array(
							'guid' => 'apartment_id',
							'id' => 'apartment_id',
							'control' => array(
								'type' => 'select',
								'options' => 'function',
								'function' => array( 'apartments',  'get_apartment_dropwdown_list' ),
								'search' => true,
								'initial' => array(
									'-- Let the User Select --' => '',
								),
								'default' => '',
								'allow_empty' => true,
							),
							'label' => 'Apartment',
							'help'  => 'Select the apartment this widget is for'
						),
						'autodetect' => array(
							'guid' => 'autodetect',
							'id' => 'autodetect',
							'control' => array(
								'type' => 'checkbox',
								'boxes' => array(
									'Autodetect on single pages?' => 'yes',
								),
								'default' => 'yes',
								'allow_empty' => true,
								'empty_value' => 'no'
							),
							'label' => 'Auto-detect Apartment',
							'help'  => 'If selected and the apartment selector is set to "Let the user select" then this widget will enable the user to select the apartment on any page other than a single apartment page. On a single apartment page this widget will only allow bookings for that apartment'
						),
						'show_title' => array(
							'guid' => 'show_title',
							'id' => 'show_title',
							'control' => array(
								'type' => 'checkbox',
								'boxes' => array(
									'Show the currently show apartment title?' => 'yes',
								),
								'default' => 'yes',
								'allow_empty' => true,
								'empty_value' => 'no'
							),
							'label' => 'Show Apartment Title',
							'help'  => 'Control weather or not the apartment title should be shown'
						),
						'book_now_page' => array(
							'guid' => 'book_now_page',
							'id' => 'book_now_page',
							'control' => array(
								'type' => 'select',
								'options' => 'function',
								'function' =>  array( 'framework', 'get_pages_array' ),
								'function_args' => array( 'exclude' => array(), 'include' => array('template-booknow.php') ),
								'search' => false,
								'initial' => array(
									'-- Select a Book Now Page --' => '',
								),
								'default' => '',
								'allow_empty' => true,
							),
							'label' => 'Book Now Page',
							'help'  => 'Select a "Book Now" page the form should lead to'
						),

						'text' => array(
							'guid' => 'text',
							'id' => 'text',
							'control' => array(
								'type' => 'textarea',
								'default' => '',
								'required' => false,
								'classes' => 'widefat',
							),
							'label' => 'Widget Text',
							'help'  => 'Text which will be shown above the calendar'
						),
						'text_color' => array(
							'guid' => 'text_color',
							'id' => 'text_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#4C4841',
								'allow_empty' => false
							),
							'label' => 'General Text Color',
							'help'  => 'The text color of the miscellaneous text of the calendar'
						),
						'availability_background_color' => array(
							'guid' => 'availability_background_color',
							'id' => 'availability_background_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#C1DDA6',
								'allow_empty' => false
							),
							'label' => 'Available Background',
							'help'  => 'The background color of the available dates'
						),
						'availability_text_color' => array(
							'guid' => 'availability_text_color',
							'id' => 'availability_text_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#28440D',
								'allow_empty' => false
							),
							'label' => 'Available Text Color',
							'help'  => 'The text color of the available dates'
						),
						'unavailability_background_color' => array(
							'guid' => 'unavailability_background_color',
							'id' => 'unavailability_background_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#B22E27',
								'allow_empty' => false
							),
							'label' => 'Unavailable Background',
							'help'  => 'The background color of the unavailable dates'
						),
						'unavailability_text_color' => array(
							'guid' => 'unavailability_text_color',
							'id' => 'unavailability_text_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#ffffff',
								'allow_empty' => false
							),
							'label' => 'Unavailable Text Color',
							'help'  => 'The text color of the unavailable dates'
						),
						'past_background_color' => array(
							'guid' => 'past_background_color',
							'id' => 'past_background_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#f1f1f1',
								'allow_empty' => false
							),
							'label' => 'Past Dates Background',
							'help'  => 'The background color of past dates'
						),
						'past_text_color' => array(
							'guid' => 'past_text_color',
							'id' => 'past_text_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#4C4841',
								'allow_empty' => false
							),
							'label' => 'Past Dates Text Color',
							'help'  => 'The text color of past dates'
						),
					)
				)
			)
		),

		'contact' => array(
			'tabs' => array(
				'main_settings' => array(
					'guid'        => 'contact',
					'id'          => 'rf_widget_contact',
					'tab_title'   => 'Contact Settings',
					'name'        => 'Customizable Contact Sheet',
					'description' => 'A widget that displays social icons and contact information',
					'width'       => 600,
					'items'       => array(
						'title' => array(
							'guid' => 'title',
							'id' => 'title',
							'control' => array(
								'type' => 'text',
								'default' => 'Get in touch',
								'required' => false,
								'classes' => 'widefat',
							),
							'label' => 'Widget Title',
							'help'  => 'The widget title will be shown above the widget'
						),
						'rss' => array(
							'guid' => 'rss',
							'id' => 'rss',
							'control' => array(
								'type' => 'checkbox',
								'boxes' => array(
									'Show Rss Icon' => 'yes',
								),
								'default' => 'yes',
								'allow_empty' => true,
								'empty_value' => 'no'
							),
							'label' => 'Display RSS Icon',
							'help'  => 'An icon linking to your site\'s RSS feed will be shown'
						),
						'twitter' => array(
							'guid' => 'twitter',
							'id' => 'twitter',
							'control' => array(
								'type' => 'text',
								'default' => '',
								'required' => false,
								'classes' => 'widefat',
							),
							'label' => 'Twitter Username',
							'help'  => 'A twitter icon linking to the given Twitter user will be shown'
						),
						'facebook' => array(
							'guid' => 'facebook',
							'id' => 'facebook',
							'control' => array(
								'type' => 'text',
								'default' => '',
								'required' => false,
								'classes' => 'widefat',
							),
							'label' => 'Facebook Link',
							'help'  => 'A Facebook icon linking to the given Facebook link will be shown'
						),
						'linkedin' => array(
							'guid' => 'linkedin',
							'id' => 'linkedin',
							'control' => array(
								'type' => 'text',
								'default' => '',
								'required' => false,
								'classes' => 'widefat',
							),
							'label' => 'Linkedin Link',
							'help'  => 'A Linkedin icon linking to the given Linkedin link will be shown'
						),
						'flickr' => array(
							'guid' => 'flickr',
							'id' => 'flickr',
							'control' => array(
								'type' => 'text',
								'default' => '',
								'required' => false,
								'classes' => 'widefat',
							),
							'label' => 'Flickr Link',
							'help'  => 'A flickr icon linking to the given Flickr link will be shown'
						),
						'phone' => array(
							'guid' => 'phone',
							'id' => 'phone',
							'control' => array(
								'type' => 'text',
								'default' => '',
								'required' => false,
								'classes' => 'widefat',
							),
							'label' => 'Phone Number',
							'help'  => 'The given phone number will be shown next to a phone icon'
						),
						'phone_icon' => array(
							'guid' => 'phone_icon',
							'id' => 'phone_icon',
							'control' => array(
								'type' => 'upload',
								'default' => get_template_directory_uri() . '/img/defaults/contact-phone.png',
								'allow_empty' => true
							),
							'label' => 'Phone Icon',
							'help'  => 'This icon will be used in the widget'
						),
						'email' => array(
							'guid' => 'email',
							'id' => 'email',
							'control' => array(
								'type' => 'text',
								'default' => '',
								'required' => false,
								'classes' => 'widefat',
							),
							'label' => 'Email',
							'help'  => 'The given email address will be shown next to an email icon (use at your own risk!)'
						),
						'email_icon' => array(
							'guid' => 'email_icon',
							'id' => 'email_icon',
							'control' => array(
								'type' => 'upload',
								'default' => get_template_directory_uri() . '/img/defaults/contact-email.png',
								'allow_empty' => true
							),
							'label' => 'Email Icon',
							'help'  => 'This icon will be used in the widget'
						),
						'location' => array(
							'guid' => 'location',
							'id' => 'location',
							'control' => array(
								'type' => 'text',
								'default' => '',
								'required' => false,
								'classes' => 'widefat',
							),
							'label' => 'Location',
							'help'  => 'The given address will be shown next to a location icon'
						),
						'location_icon' => array(
							'guid' => 'location_icon',
							'id' => 'location_icon',
							'control' => array(
								'type' => 'upload',
								'default' => get_template_directory_uri() . '/img/defaults/contact-location.png',
								'allow_empty' => true
							),
							'label' => 'Location Icon',
							'help'  => 'This icon will be used in the widget'
						),
					)
				)
			)
		),
		'twitter' => array(
			'tabs' => array(
				'main_settings' => array(
					'guid'        => 'twitter',
					'id'          => 'rf_widget_twitter',
					'tab_title'   => 'Twitter Settings',
					'name'        => 'Customisable Twitter Box',
					'description' => 'A widget that displays tweets from a certain user',
					'width'       => 600,
					'items'       => array(
						'title' => array(
							'guid' => 'title',
							'id' => 'title',
							'control' => array(
								'type' => 'text',
								'default' => 'My Tweets',
								'required' => false,
								'classes' => 'widefat',
							),
							'label' => 'Widget Title',
							'help'  => 'The widget title will be shown above the widget'
						),
						'username' => array(
							'guid' => 'username',
							'id' => 'username',
							'control' => array(
								'type' => 'text',
								'default' => 'redfactory',
								'required' => true,
								'classes' => 'widefat',
							),
							'label' => 'Twitter Username',
							'help'  => 'Tweets will be shown from this user'
						),
						'count' => array(
							'guid' => 'count',
							'id' => 'count',
							'control' => array(
								'type' => 'text',
								'default' => 3,
								'required' => true
							),
							'label' => 'Tweets to show',
							'help'  => 'The number of tweets to show from the given user'
						),
						'followme' => array(
							'guid' => 'followme',
							'id' => 'followme',
							'control' => array(
								'type' => 'text',
								'default' => 'follow me',
								'required' => false
							),
							'label' => 'Follow Link Text',
							'help'  => 'The text to show on the follow link under the tweets. If left blank, no link will be shown.'
						),
					)
				)
			)
		),
		'featured' => array(
			'tabs' => array(
				'main_settings' => array(
					'guid'        => 'featured',
					'id'          => 'rf_widget_featured',
					'tab_title'   => 'Featured Item Settings',
					'name'        => 'Featured Item Display',
					'description' => 'Feature an arbitrary image and text',
					'width'       => 600,
					'items'       => array(
						'title' => array(
							'guid' => 'title',
							'id' => 'title',
							'control' => array(
								'type' => 'text',
								'default' => 'Featured Content',
								'required' => false,
								'classes' => 'widefat',
							),
							'label' => 'Widget Title',
							'help'  => 'The widget title will be shown above the widget'
						),
						'image' => array(
							'guid' => 'image',
							'id' => 'image',
							'control' => array(
								'type' => 'upload',
								'default' => '',
								'required' => false,
							),
							'label' => 'Featured Image',
							'help'  => 'This image will be used'
						),
						'text' => array(
							'guid' => 'text',
							'id' => 'text',
							'control' => array(
								'type' => 'textarea',
								'default' => '',
								'required' => false
							),
							'label' => 'Text',
							'help'  => 'The text displayed under the featured image'
						),
						'link_text' => array(
							'guid' => 'link_text',
							'id' => 'link_text',
							'control' => array(
								'type' => 'text',
								'default' => '',
								'required' => false
							),
							'label' => 'Link Text',
							'help'  => 'The text of the link below the featured image and text. If left blank, no link will be shown'
						),
						'link_url' => array(
							'guid' => 'link_url',
							'id' => 'link_url',
							'control' => array(
								'type' => 'text',
								'default' => '',
								'required' => false
							),
							'label' => 'Link URL',
							'help'  => 'The URL where the link should point'
						),

					)
				)
			)
		),
		'map' => array(
			'tabs' => array(
				'main_settings' => array(
					'guid'        => 'map',
					'id'          => 'rf_widget_map',
					'tab_title'   => 'Map Settings',
					'name'        => 'Customisable Map Display',
					'description' => 'Add any location shown on a Google Map to the sidebar',
					'width'       => 600,
					'items'       => array(
						'title' => array(
							'guid' => 'title',
							'id' => 'title',
							'control' => array(
								'type' => 'text',
								'default' => 'My Location',
								'required' => false,
								'classes' => 'widefat',
							),
							'label' => 'Widget Title',
							'help'  => 'The widget title will be shown above the widget'
						),
						'location' => array(
							'guid' => 'location',
							'id' => 'location',
							'control' => array(
								'type' => 'text',
								'default' => 'New York',
								'required' => true
							),
							'label' => 'Location',
							'help'  => 'Any valid string which you can use in Google Maps or Google Earth can be used here'
						),
						'height' => array(
							'guid' => 'height',
							'id' => 'height',
							'control' => array(
								'type' => 'text',
								'default' => 300,
								'required' => true
							),
							'label' => 'Map Height',
							'help'  => 'Input a fixed map height'
						),
						'maptype' => array(
							'guid' => 'maptype',
							'id' => 'maptype',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Road map' => 'ROADMAP',
									'Satellite map' => 'SATELLITE',
									'Hybrid map' => 'HYBRID',
									'Terrain map' => 'TERRAIN',
								),
								'default' => 'ROADMAP',
								'required' => true
							),
							'label' => 'Map Type',
							'help'  => 'The selected map type will be used to display your location'
						),
						'marker' => array(
							'guid' => 'marker',
							'id' => 'marker',
							'control' => array(
								'type' => 'checkbox',
								'boxes' => array(
									'Add a marker to the map?' => 'yes',
								),
								'default' => 'yes',
								'required' => false
							),
							'label' => 'Marker',
							'help'  => 'If checked, a pin will be added to the map'
						),
						'zoom' => array(
							'guid' => 'zoom',
							'id' => 'zoom',
							'control' => array(
								'type' => 'text',
								'default' => '12',
								'required' => true
							),
							'label' => 'Zoom',
							'help'  => 'How close the map should zoom to the given location. For street maps, 12-ish works best'
						),
						'text_above' => array(
							'guid' => 'text_above',
							'id' => 'text_above',
							'control' => array(
								'type' => 'textarea',
								'default' => '',
								'required' => true
							),
							'label' => 'Text Above Map',
							'help'  => 'Write some text to display above the map'
						),
						'text_below' => array(
							'guid' => 'text_below',
							'id' => 'text_below',
							'control' => array(
								'type' => 'textarea',
								'default' => '',
								'required' => true
							),
							'label' => 'Text Below Map',
							'help'  => 'Write some text to display below the map'
						),
					)
				)
			)
		),
	)
);


/** Meta Boxes
  *
  * Define the meta boxes you would like to use.
  * You will need to define these meta boxes in the metaboxes.php
  * file. See metaboxes.php or metaboxes.sample.php in the
  * samples directory for more information.
  *
  **/
$eb_defaults['metaboxes'] = array();

/** Custom Fields
  *
  * Define the custom fields you would like to use. The outermost
  * array's key must be the same as the WordPress page template
  * name. For post types this is the name of the post type, for
  * page templates this is the filename which handles the template
  * (don't forget that the .php at the end is also needed).
  *
  * This array must contain another array with the key of 'items'.
  * The members of this array create the tabs in the post settings.
  * each tab must be an array with the following members:
  * guid, tab_title, items.
  *
  * The guid stores an identifier for the tab, the tab_title
  * is used to show the tab's title, while the items show
  * the relevant controls. See the controls section of the
  * documentation for more info.
  *
  **/
$eb_defaults['custom_fields'] = array(
	'groups' => array(
		'page' => array(
			'disable_meta' => 'yes',
			'tabs' => array (
				'page_structure' => array(
					'guid' => 'page_structure',
					'tab_title' => 'Page Structure',
					'header' => array(
						'title' => 'Page Structure',
						'description' => 'Modify the general structure of this page',
					),
					'items' => array(
						'sidebar' => array(
							'guid' => 'sidebar',
							'control' => array(
								'type' => 'select',
								'options' => 'function',
								'function' => array('framework', 'get_sidebars_array'),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default' => 'default',
								'default_label' => 'current default: ' . eb_get_default_option( 'sidebar', 'page' ),
								'allow_empty' => false
							),
							'label' => 'Sidebar to Show',
							'help'  => 'Select which sidebar you would like to show on this page. You can create multiple sidebars in the theme settings.'
						),
						'sidebar_position' => array(
							'guid' => 'sidebar_position',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Left' => 'left',
									'Right' => 'right',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'sidebar_position', 'page' ),
								'default' => 'default',
								'allow_empty' => false
							),
							'label' => 'Sidebar Position',
							'help'  => 'Modify the placement of the sidebar on this page'
						),
						'show_sidebar' => array(
							'guid' => 'show_sidebar',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'show_sidebar', 'page' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Show Sidebar?',
							'help'  => 'If checked the page will be shown without a sidebar'
						),
						'show_title' => array(
							'guid' => 'show_title',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'show_title', 'page' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Show Title',
							'help'  => 'Specify weather you would like the title to be shown'
						),
						'show_thumbnail' => array(
							'guid' => 'show_thumbnail',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'show_thumbnail', 'page' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Show Featured Image',
							'help'  => 'Specify weather you would like the featured image to be shown'
						),
						'boxed_content' => array(
							'guid' => 'boxed_content',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'boxed_content', 'page' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Boxed Content',
							'help'  => 'Specify weather you would like the content to be boxed or not'
						),
						'boxed_comments' => array(
							'guid' => 'boxed_comments',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'boxed_comments', 'page' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Boxed Comments',
							'help'  => 'Specify weather you would like the comments to be boxed or not'
						),
					)
				),
			)
		),
		'post' => array(
			'disable_meta' => 'yes',
			'tabs' => array (
				'page_structure' => array(
					'guid' => 'page_structure',
					'tab_title' => 'Page Structure',
					'header' => array(
						'title' => 'Page Structure',
						'description' => 'Modify the general structure of this page',
					),
					'items' => array(
						'sidebar' => array(
							'guid' => 'sidebar',
							'control' => array(
								'type' => 'select',
								'options' => 'function',
								'function' => array('framework', 'get_sidebars_array'),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default' => 'default',
								'default_label' => 'current default: ' . eb_get_default_option( 'sidebar', 'post' ),
								'allow_empty' => false
							),
							'label' => 'Sidebar to Show',
							'help'  => 'Select which sidebar you would like to show on this page. You can create multiple sidebars in the theme settings.'
						),
						'sidebar_position' => array(
							'guid' => 'sidebar_position',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Left' => 'left',
									'Right' => 'right',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'sidebar_position', 'post' ),
								'default' => 'default',
								'allow_empty' => false
							),
							'label' => 'Sidebar Position',
							'help'  => 'Modify the placement of the sidebar on this page'
						),
						'show_sidebar' => array(
							'guid' => 'show_sidebar',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'show_sidebar', 'post' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Show Sidebar?',
							'help'  => 'If checked the page will be shown without a sidebar'
						),
						'show_title' => array(
							'guid' => 'show_title',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'show_title', 'post' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Show Title',
							'help'  => 'Specify weather you would like the title to be shown'
						),
						'show_thumbnail' => array(
							'guid' => 'show_thumbnail',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'show_thumbnail', 'post' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Show Featured Image',
							'help'  => 'Specify weather you would like the featured image to be shown'
						),
						'boxed_content' => array(
							'guid' => 'boxed_content',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'boxed_content', 'post' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Boxed Content',
							'help'  => 'Specify weather you would like the content to be boxed or not'
						),
						'boxed_comments' => array(
							'guid' => 'boxed_comments',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'boxed_comments', 'post' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Boxed Comments',
							'help'  => 'Specify weather you would like the comments to be boxed or not'
						),
					)
				),
			)
		),
		'template-postlist' => array(
			'disable_meta' => 'yes',
			'tabs' => array (
				'page_structure' => array(
					'guid' => 'page_structure',
					'tab_title' => 'Page Structure',
					'header' => array(
						'title' => 'Page Structure',
						'description' => 'Modify the general structure of this page',
					),
					'items' => array(
						'sidebar' => array(
							'guid' => 'sidebar',
							'control' => array(
								'type' => 'select',
								'options' => 'function',
								'function' => array('framework', 'get_sidebars_array' ),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default' => 'default',
								'default_label' => 'current default: ' . eb_get_default_option( 'sidebar', 'template-postlist' ),
								'allow_empty' => false
							),
							'label' => 'Sidebar to Show',
							'help'  => 'Select which sidebar you would like to show on this page. You can create multiple sidebars in the theme settings.'
						),
						'sidebar_position' => array(
							'guid' => 'sidebar_position',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Left' => 'left',
									'Right' => 'right',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'sidebar_position', 'template-postlist' ),
								'default' => 'default',
								'allow_empty' => false
							),
							'label' => 'Sidebar Position',
							'help'  => 'Modify the placement of the sidebar on this page'
						),
						'show_sidebar' => array(
							'guid' => 'show_sidebar',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'show_sidebar', 'template-postlist' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Show Sidebar?',
							'help'  => 'If checked the page will be shown without a sidebar'
						),
						'show_title' => array(
							'guid' => 'show_title',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'show_title', 'template-postlist' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Show Title',
							'help'  => 'Specify weather you would like the title to be shown'
						),
						'show_content' => array(
							'guid' => 'show_content',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'show_content', 'template-postlist' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Show Content',
							'help'  => 'Specify weather you would like the content to be shown'
						),
						'show_thumbnail' => array(
							'guid' => 'show_thumbnail',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'show_thumbnail', 'template-postlist' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Show Featured Image',
							'help'  => 'Specify weather you would like the featured image to be shown'
						),
						'boxed_content' => array(
							'guid' => 'boxed_content',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'boxed_content', 'template-postlist' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Boxed Content',
							'help'  => 'Specify weather you would like the content to be boxed or not'
						),
						'boxed_comments' => array(
							'guid' => 'boxed_comments',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'boxed_comments', 'template-postlist' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Boxed Comments',
							'help'  => 'Specify weather you would like the comments to be boxed or not'
						),
					)
				),
				'postlist_content' => array(
					'guid' => 'postlist_content',
					'tab_title' => 'Postlist Content',
					'header' => array(
						'title' => 'Postlist Content',
						'description' => 'Modify what is shown on this page',
					),
					'items' => array(
						'posts_per_page' => array(
							'guid' => 'posts_per_page',
							'control' => array(
								'type' => 'text',
								'default' => get_option( 'posts_per_page' ),
								'allow_empty' => false
							),
							'label' => 'Items Per Page',
							'help'  => 'Select how many items you want to show on each page. Adding 0 or -1 will retrieve all the posts without using nn'
						),
						'category' => array(
							'guid' => 'category',
							'control' => array(
								'fullwidth' => true,
								'type' => 'dualboxes',
								'options' => 'function',
								'function' => array( 'framework', 'get_taxonomies_array'),
								'function_args' => array( 'taxonomy' => 'category' ),
								'default' => '',
								'allow_empty' => true
							),
							'label' => 'Categories',
							'help'  => 'Select the categories to use on this page'
						),
						'orderby' => array(
							'guid' => 'orderby',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Date Published' => 'date',
									'Title' => 'title',
									'Date Modified' => 'modified',
									'Random' => 'rand',
									'Comment Count' => 'comment_count',
									'Menu Order' => 'menu_order',
								),
								'default' => 'date',
								'allow_empty' => false
							),
							'label' => 'Order By',
							'help'  => 'Select how you want to order the posts'
						),
						'order' => array(
							'guid' => 'order',
							'control' => array(
								'type' => 'radio',
								'boxes' => array(
									'Ascending' => 'ASC',
									'Descending' => 'DESC',
								),
								'default' => 'DESC',
								'allow_empty' => false
							),
							'label' => 'Order Direction',
							'help'  => 'Select the direction of the item order'
						),
						'only_thumbnailed' => array(
							'guid' => 'only_thumbnailed',
							'control' => array(
								'type' => 'checkbox',
								'boxes' => array(
									'Only show items which have thumbnails' => 'yes',
								),
								'default' => 'no',
								'allow_empty' => true,
								'empty_value' => 'no'
							),
							'label' => 'Only With Images',
							'help'  => 'Check the box to make sure only items with featured images are shown'
						),
					)
				),
				'postlist_items' => array(
					'guid' => 'postlist_items',
					'tab_title' => 'Postlist Items',
					'header' => array(
						'title' => 'Postlist Items',
						'description' => 'Modify how each item is displayed',
					),
					'items' => array(
						'layout' => array(
							'guid' => 'layout',
							'control' => array(
								'type' => 'select',
								'options' => 'function',
								'function' => array( 'framework', 'get_layouts_array' ),
								'function_args' => array( 'type' => 'post', 'default' => true ),
								'default' => '',
								'default_label' => 'current default: ' . eb_get_default_layout( 'post' ),
								'allow_empty' => false
							),
							'label' => 'Layout',
							'help'  => 'Select the layout for posts on this page'
						),
					)
				)

			)
		),
		'template-gallery' => array(
			'disable_meta' => 'yes',
			'tabs' => array (
				'page_structure' => array(
					'guid' => 'page_structure',
					'tab_title' => 'Page Structure',
					'header' => array(
						'title' => 'Page Structure',
						'description' => 'Modify the general structure of this page',
					),
					'items' => array(
						'sidebar' => array(
							'guid' => 'sidebar',
							'control' => array(
								'type' => 'select',
								'options' => 'function',
								'function' => array( 'framework', 'get_sidebars_array'),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default' => 'default',
								'default_label' => 'current default: ' . eb_get_default_option( 'sidebar', 'template-postlist' ),
								'allow_empty' => false
							),
							'label' => 'Sidebar to Show',
							'help'  => 'Select which sidebar you would like to show on this page. You can create multiple sidebars in the theme settings.'
						),
						'sidebar_position' => array(
							'guid' => 'sidebar_position',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Left' => 'left',
									'Right' => 'right',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'sidebar_position', 'template-postlist' ),
								'default' => 'default',
								'allow_empty' => false
							),
							'label' => 'Sidebar Position',
							'help'  => 'Modify the placement of the sidebar on this page'
						),
						'show_sidebar' => array(
							'guid' => 'show_sidebar',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'show_sidebar', 'template-postlist' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Show Sidebar?',
							'help'  => 'If checked the page will be shown without a sidebar'
						),
						'show_title' => array(
							'guid' => 'show_title',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'show_title', 'template-postlist' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Show Title',
							'help'  => 'Specify weather you would like the title to be shown'
						),
						'show_content' => array(
							'guid' => 'show_content',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'show_content', 'template-postlist' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Show Content',
							'help'  => 'Specify weather you would like the content to be shown'
						),
						'show_thumbnail' => array(
							'guid' => 'show_thumbnail',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'show_thumbnail', 'template-postlist' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Show Featured Image',
							'help'  => 'Specify weather you would like the featured image to be shown'
						),
						'boxed_content' => array(
							'guid' => 'boxed_content',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'boxed_content', 'template-postlist' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Boxed Content',
							'help'  => 'Specify weather you would like the content to be boxed or not'
						),
						'boxed_comments' => array(
							'guid' => 'boxed_comments',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'boxed_comments', 'template-postlist' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Boxed Comments',
							'help'  => 'Specify weather you would like the comments to be boxed or not'
						),
					)
				),
				'gallery_content' => array(
					'guid' => 'gallery_content',
					'tab_title' => 'Gallery Content',
					'header' => array(
						'title' => 'Gallery Content',
						'description' => 'Modify what is shown on this page',
					),
					'items' => array(
						'columns' => array(
							'guid' => 'columns',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'2' => '2',
									'3' => '3',
									'4' => '4',
									'5' => '5',
									'6' => '6',
								),
								'default' => '3',
								'allow_empty' => false
							),
							'label' => 'Number of Columns',
							'help'  => 'Select the number of columns to organize content in on this page'
						),
						'posts_per_page' => array(
							'guid' => 'posts_per_page',
							'control' => array(
								'type' => 'text',
								'default' => get_option( 'posts_per_page' ),
								'allow_empty' => false
							),
							'label' => 'Items Per Page',
							'help'  => 'Select how many items you want to show on each page. Adding 0 or -1 will retrieve all the posts without using nn'
						),
						'category' => array(
							'guid' => 'category',
							'control' => array(
								'fullwidth' => true,
								'type' => 'dualboxes',
								'options' => 'function',
								'function' => array( 'framework', 'get_taxonomies_array'),
								'function_args' => array( 'taxonomy' => 'category' ),
								'default' => '',
								'allow_empty' => true
							),
							'label' => 'Categories',
							'help'  => 'Select the categories to use on this page'
						),
						'orderby' => array(
							'guid' => 'orderby',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Date Published' => 'date',
									'Title' => 'title',
									'Date Modified' => 'modified',
									'Random' => 'rand',
									'Comment Count' => 'comment_count',
									'Menu Order' => 'menu_order',
								),
								'default' => 'date',
								'allow_empty' => false
							),
							'label' => 'Order By',
							'help'  => 'Select how you want to order the posts'
						),
						'order' => array(
							'guid' => 'order',
							'control' => array(
								'type' => 'radio',
								'boxes' => array(
									'Ascending' => 'ASC',
									'Descending' => 'DESC',
								),
								'default' => 'DESC',
								'allow_empty' => false
							),
							'label' => 'Order Direction',
							'help'  => 'Select the direction of the item order'
						),
						'only_thumbnailed' => array(
							'guid' => 'only_thumbnailed',
							'control' => array(
								'type' => 'checkbox',
								'boxes' => array(
									'Only show items which have thumbnails' => 'yes',
								),
								'default' => 'yes',
								'allow_empty' => true,
								'empty_value' => 'no'
							),
							'label' => 'Only With Images',
							'help'  => 'Check the box to make sure only items with featured images are shown'
						),
					)
				),
				'gallery_items' => array(
					'guid' => 'gallery_items',
					'tab_title' => 'Gallery Items',
					'header' => array(
						'title' => 'Gallery Items',
						'description' => 'Modify how each item is displayed',
					),
					'items' => array(
						'layout' => array(
							'guid' => 'layout',
							'control' => array(
								'type' => 'select',
								'options' => 'function',
								'function' => array( 'framework', 'get_layouts_array' ),
								'function_args' => array( 'type' => 'gallery', 'default' => true ),
								'default' => '',
								'default_label' => 'current default: ' . eb_get_default_layout( 'gallery' ),
								'allow_empty' => false
							),
							'label' => 'Layout',
							'help'  => 'Select the layout for posts on this page'
						),
						'show_item_title' => array(
							'guid' => 'show_item_title',
							'control' => array(
								'type' => 'checkbox',
								'boxes' => array(
									'Show gallery item titles?' => 'yes',
								),
								'default' => 'yes',
								'allow_empty' => true,
								'empty_value' => 'no',
							),
							'label' => 'Show Title',
							'help'  => 'If checked the title will be shown on gallery items'
						),
						'show_item_date' => array(
							'guid' => 'show_item_date',
							'control' => array(
								'type' => 'checkbox',
								'boxes' => array(
									'Show gallery item dates?' => 'yes',
								),
								'default' => 'yes',
								'allow_empty' => true,
								'empty_value' => 'no',
							),
							'label' => 'Show Dates',
							'help'  => 'If checked the dates will be shown on gallery items'
						),
						'show_item_excerpt' => array(
							'guid' => 'show_item_excerpt',
							'control' => array(
								'type' => 'checkbox',
								'boxes' => array(
									'Show gallery item excerpts?' => 'yes',
								),
								'default' => 'yes',
								'allow_empty' => true,
								'empty_value' => 'no',
							),
							'label' => 'Show Excerpt',
							'help'  => 'If checked the excerpts will be shown on gallery items'
						),
						'show_item_link' => array(
							'guid' => 'show_item_link',
							'control' => array(
								'type' => 'checkbox',
								'boxes' => array(
									'Show gallery item read more links?' => 'yes',
								),
								'default' => 'yes',
								'allow_empty' => true,
								'empty_value' => 'no',
							),
							'label' => 'Show Read More Links',
							'help'  => 'If checked the read more links will be shown on gallery items'
						),
						'show_item_image' => array(
							'guid' => 'show_item_image',
							'control' => array(
								'type' => 'checkbox',
								'boxes' => array(
									'Hide gallery item image?' => 'yes',
								),
								'default' => 'yes',
								'allow_empty' => true,
								'empty_value' => 'no',
							),
							'label' => 'Show Image',
							'help'  => 'If checked the image will be shown on gallery items'
						),

					)
				)

			)
		),
		'template-apartmentlist' => array(
			'tabs' => array (
				'page_structure' => array(
					'guid' => 'page_structure',
					'tab_title' => 'Page Structure',
					'header' => array(
						'title' => 'Page Structure',
						'description' => 'Modify the general structure of this page',
					),
					'items' => array(
						'sidebar_position' => array(
							'guid' => 'sidebar_position',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Left' => 'left',
									'Right' => 'right',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'sidebar_position', 'template-apartmentlist' ),
								'default' => 'default',
								'allow_empty' => false
							),
							'label' => 'Sidebar Position',
							'help'  => 'Modify the placement of the sidebar on this page'
						),
						'show_title' => array(
							'guid' => 'show_title',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'show_title', 'template-postlist' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Show Title',
							'help'  => 'Specify weather you would like the title to be shown'
						),
						'show_content' => array(
							'guid' => 'show_content',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'show_content', 'template-postlist' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Show Content',
							'help'  => 'Specify weather you would like the content to be shown'
						),
						'show_thumbnail' => array(
							'guid' => 'show_thumbnail',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'show_thumbnail', 'template-postlist' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Show Featured Image',
							'help'  => 'Specify weather you would like the featured image to be shown'
						),
						'boxed_content' => array(
							'guid' => 'boxed_content',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'boxed_content', 'template-postlist' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Boxed Content',
							'help'  => 'Specify weather you would like the content to be boxed or not'
						),
					)
				),
				'filter_and_order' => array(
					'guid' => 'filter_and_order',
					'tab_title' => 'Filter and Order',
					'header' => array(
						'title' => 'Filter and Order',
						'description' => 'Select your settings for how items are filtered and ordered',
					),
					'items' => array(
						'filter_and_order' => array(
							'guid' => 'filter_and_order',
							'control' => array(
								'fullwidth' => true,
								'type' => 'dualboxes',
								'options' => 'function',
								'function' => array( 'apartments','get_details_array' ),
								'default' => '',
								'allow_empty' => true
							),
							'label' => 'Ordering Options',
							'help'  => 'By default, apartments can be ordered by name and price. With the box you can add any of your custom details to the ordering options'
						),
					)
				),
				'content_settings' => array(
					'guid' => 'content_settings',
					'tab_title' => 'Content Settings',
					'header' => array(
						'title' => 'Content Settings',
						'description' => 'Select options for content display',
					),
					'items' => array(
						'layout' => array(
							'guid' => 'layout',
							'control' => array(
								'type' => 'select',
								'options' => 'function',
								'function' => array( 'framework', 'get_layouts_array'),
								'function_args' => array( 'type' => 'apartmentlist', 'default' => true ),
								'default' => '',
								'default_label' => 'current default: ' . eb_get_default_layout( 'apartmentlist' ),
								'allow_empty' => false
							),
							'label' => 'Layout',
							'help'  => 'Select the layout for posts on this page'
						),
					)
				),

			)
		),

		'tvr_apartment' => array(
			'tabs' => array (
				'page_structure' => array(
					'guid' => 'page_structure',
					'tab_title' => 'Page Structure',
					'header' => array(
						'title' => 'Page Structure',
						'description' => 'Modify the general structure of this page',
					),
					'items' => array(
						'sidebar_push' => array(
							'guid' => 'sidebar_push',
							'control' => array(
								'type' => 'checkbox',
								'boxes' => array(
									'Push sidebar under slider?' => 'yes',
								),
								'default' => 'yes',
								'allow_empty' => true,
								'empty_value' => 'no'
							),
							'label' => 'Sidebar Under Slider?',
							'help'  => 'Check the box to push the sidebar under the slider'
						),
						'sidebar' => array(
							'guid' => 'sidebar',
							'control' => array(
								'type' => 'select',
								'options' => 'function',
								'function' => array('framework', 'get_sidebars_array'),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default' => 'default',
								'default_label' => 'current default: ' . eb_get_default_option( 'sidebar', 'tvr_apartment' ),
								'allow_empty' => false
							),
							'label' => 'Sidebar to Show',
							'help'  => 'Select which sidebar you would like to show on this page. You can create multiple sidebars in the theme settings.'
						),
						'sidebar_position' => array(
							'guid' => 'sidebar_position',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Left' => 'left',
									'Right' => 'right',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'sidebar_position', 'tvr_apartment' ),
								'default' => 'default',
								'allow_empty' => false
							),
							'label' => 'Sidebar Position',
							'help'  => 'Modify the placement of the sidebar on this page'
						),
						'show_sidebar' => array(
							'guid' => 'show_sidebar',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'show_sidebar', 'tvr_apartment' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Show Sidebar?',
							'help'  => 'If checked the page will be shown without a sidebar'
						),
						'show_title' => array(
							'guid' => 'show_title',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'show_title', 'tvr_apartment' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Show Title',
							'help'  => 'Specify weather you would like the title to be shown'
						),
						'show_thumbnail' => array(
							'guid' => 'show_thumbnail',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'show_thumbnail', 'tvr_apartment' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Show Featured Image',
							'help'  => 'Specify weather you would like the featured image to be shown'
						),
						'boxed_content' => array(
							'guid' => 'boxed_content',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'boxed_content', 'tvr_apartment' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Boxed Content',
							'help'  => 'Specify weather you would like the content to be boxed or not'
						),
						'boxed_comments' => array(
							'guid' => 'boxed_comments',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'boxed_comments', 'tvr_apartment' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Boxed Comments',
							'help'  => 'Specify weather you would like the comments to be boxed or not'
						),
					)
				),
				'pricing_options' => array(
					'guid' => 'pricing_options',
					'tab_title' => 'Booking',
					'header' => array(
						'title' => 'Pricing',
						'description' => 'Set up the pricing scheme for this apartment',
					),
					'items' => array(

						'book_now_page' => array(
							'guid' => 'book_now_page',
							'control' => array(
								'type' => 'select',
								'options' => 'function',
								'function' =>  array( 'framework', 'get_pages_array' ),
								'function_args' => array( 'exclude' => array(), 'include' => array('template-booknow.php') ),
								'search' => false,
								'initial' => array(
									'-- Select a Book Now Page --' => '',
								),
								'default' => '',
								'allow_empty' => true,

							),
							'label' => 'Book Now Page',
							'help'  => 'Select the booking page to use'
						),

						'maximum_guests' => array(
							'guid' => 'maximum_guests',
							'control' => array(
								'type' => 'text',
								'default' => 0,
								'allow_empty' => false,
								'empty_value' => 0,
								'classes' => 'small'
							),
							'label' => 'Maximum Guests',
							'help'  => 'Enter the maximum number of guests you can accomodate in this'
						),
						'minimum_stay' => array(
							'guid' => 'minimum_stay',
							'control' => array(
								'type' => 'text',
								'default' => 1,
								'allow_empty' => false,
								'empty_value' => 1,
							),
							'label' => 'Minimum Stay (days)',
							'help'  => 'The number of days you allow for a minimum stay'
						),
						'default_price' => array(
							'guid' => 'default_price',
							'control' => array(
								'type' => 'price',
								'default' => 0,
								'allow_empty' => false,
								'empty_value' => 0,
								'classes' => 'small'
							),
							'label' => 'Default Price',
							'help'  => 'The price of the apartment when there is no special seasonal pricing in effect'
						),
						'seasonal_pricing' => array(
							'guid' => 'seasonal_pricing',
							'control' => array(
								'type' => 'custom',
								'function' => 'tvr_generate_control_seasonal_pricing',
								'default' => '',
								'allow_empty' => true,
							),
							'label' => 'Seasonal Pricing',
							'help'  => 'Add seasonal pricing details for any period you\'d like'
						),

					)
				),
				'apartment_details' => array(
					'guid' => 'apartment_details',
					'tab_title' => 'Details',
					'header' => array(
						'title' => 'Apartment Details',
						'description' => 'Fill out the details of your apartment to show to users on the site',
					),
					'items' => array(
						'apartment_details' => array(
							'guid' => 'apartment_details',
							'control' => array(
								'type' => 'custom_fields',
								'required' => false,
								'name' => 'Detail',
								'button_text' => 'add another',
								'remove_text' => 'remove apartment detail',
								'default' => '',
								'allow_empty' => true
							),
							'label' => 'Apartment Details',
							'help'  => 'Use the controls to add any detail you\'d like. Use the left hand box to add a detail type (eg: rooms) and use the right hand box to add a value (eg: 3)'
						),

					)
				),
				'apartment_slider' => array(
					'guid' => 'apartment_slider',
					'tab_title' => 'Apartment Slider',
					'header' => array(
						'title' => 'Apartment Slider',
						'description' => 'Set up the large apartment image gallery',
					),
					'items' => array(
						'show_slider' => array(
							'guid' => 'show_slider',
							'control' => array(
								'type' => 'checkbox',
								'boxes' => array(
									'Show the large slider?' => 'yes',
								),
								'default' => 'yes',
								'allow_empty' => true,
								'empty_value' => 'no'
							),
							'label' => 'Show Slider',
							'help'  => 'Enable or disable the large image slider on top of the apartment pages'
						),
						'slider_speed' => array(
							'guid' => 'slider_speed',
							'control' => array(
								'type' => 'text',
								'default' => '4500',
								'allow_empty' => false,
								'classes' => 'small',
							),
							'label' => 'Slider Speed',
							'help'  => 'Enter the amount of time to wait between items'
						),
						'slider_transition_speed' => array(
							'guid' => 'slider_transition_speed',
							'control' => array(
								'type' => 'text',
								'default' => '2000',
								'allow_empty' => false,
								'classes' => 'small'
							),
							'label' => 'Fade Speed',
							'help'  => 'Enter the amount of time it takes for the images to fade into each other'
						),
						'apartment_slider_images' => array(
							'guid' => 'apartment_slider_images',
							'control' => array(
								'fullwidth' => true,
								'type' => 'image_selector',
								'default' => '',
								'allow_empty' => 'yes',
							),
							'label' => 'Select Images For The Gallery <small>(you\'ll need to upload at least 3)</small>',
							'help'  => 'Click on images to mark/unmark them for inclusion in the gallery. At least 3 images must be selected to form a gallery'
						),

					)
				),
			)
		),
		'tvr_booking' => array(
			'tabs' => array (
				'booking_details' => array(
					'guid' => 'booking_details',
					'tab_title' => 'Booking Details',
					'header' => array(
						'title' => 'Booking Details',
						'description' => 'Add notes and other details about this booking',
					),
					'items' => array(
						'apartment' => array(
							'guid' => 'apartment',
							'control' => array(
								'type' => 'select',
								'options' => 'function',
								'function' => array( 'apartments',  'get_apartment_dropwdown_list' ),
								'search' => true,
								'initial' => array(
									'-- Select an Apartment --' => '',
								),
								'default' => ''
							),
							'label' => 'Apartment',
							'help'  => 'Select the apartment the booking was made for'
						),
						'guests_number' => array(
							'guid' => 'guests_number',
							'control' => array(
								'type' => 'text',
								'default' => 0,
							),
							'label' => 'Guests',
							'help'  => 'The number of guests arriving'
						),
						'stay_interval' => array(
							'guid' => 'stay_interval',
							'control' => array(
								'type' => 'custom',
								'function' => 'tvr_generate_control_stay_interval',
								'default' => ''
							),
							'label' => 'Time Of Stay',
							'help'  => 'Add the start date and end date of their stay'
						),
						'contact_name' => array(
							'guid' => 'contact_name',
							'control' => array(
								'type' => 'text',
								'default' => '',
							),
							'label' => 'Contact Name',
							'help'  => 'Add the name of the person you are in contact with for this booking'
						),
						'contact_email' => array(
							'guid' => 'contact_email',
							'control' => array(
								'type' => 'text',
								'default' => '',
							),
							'label' => 'Contact Email',
							'help'  => 'Add the email address of the person you are in contact with for this booking'
						),
						'contact_phone' => array(
							'guid' => 'contact_phone',
							'control' => array(
								'type' => 'text',
								'default' => '',
							),
							'label' => 'Contact Phone',
							'help'  => 'Add the phone number of the person you are in contact with for this booking'
						),
					)
				),

			)
		),
		'template-booknow' => array(
			'disable_meta' => 'yes',
			'tabs' => array (
				'page_structure' => array(
					'guid' => 'page_structure',
					'tab_title' => 'Page Structure',
					'header' => array(
						'title' => 'Page Structure',
						'description' => 'Modify the general structure of this page',
					),
					'items' => array(
						'sidebar' => array(
							'guid' => 'sidebar',
							'control' => array(
								'type' => 'select',
								'options' => 'function',
								'function' => array('framework', 'get_sidebars_array'),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default' => 'default',
								'default_label' => 'current default: ' . eb_get_default_option( 'sidebar', 'template-booknow' ),
								'allow_empty' => false
							),
							'label' => 'Sidebar to Show',
							'help'  => 'Select which sidebar you would like to show on this page. You can create multiple sidebars in the theme settings.'
						),
						'sidebar_position' => array(
							'guid' => 'sidebar_position',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Left' => 'left',
									'Right' => 'right',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'sidebar_position', 'template-booknow' ),
								'default' => 'default',
								'allow_empty' => false
							),
							'label' => 'Sidebar Position',
							'help'  => 'Modify the placement of the sidebar on this page'
						),
						'show_sidebar' => array(
							'guid' => 'show_sidebar',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'show_sidebar', 'template-booknow' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Show Sidebar?',
							'help'  => 'If checked the page will be shown without a sidebar'
						),
						'show_title' => array(
							'guid' => 'show_title',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'show_title', 'template-booknow' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Show Title',
							'help'  => 'Specify weather you would like the title to be shown'
						),
						'show_thumbnail' => array(
							'guid' => 'show_thumbnail',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'show_thumbnail', 'template-booknow' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Show Featured Image',
							'help'  => 'Specify weather you would like the featured image to be shown'
						),
						'boxed_content' => array(
							'guid' => 'boxed_content',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'boxed_content', 'template-booknow' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Boxed Content',
							'help'  => 'Specify weather you would like the content to be boxed or not'
						),
						'boxed_comments' => array(
							'guid' => 'boxed_comments',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Yes' => 'yes',
									'No' => 'no',
								),
								'initial' => array(
									'-- Default Setting --' => 'default',
								),
								'default_label' => 'current default: ' . eb_get_default_option( 'boxed_comments', 'template-booknow' ),
								'default_value' => 'default',
								'allow_empty' => false,
							),
							'label' => 'Boxed Comments',
							'help'  => 'Specify weather you would like the comments to be boxed or not'
						),
					)
				),
				'booking_text' => array(
					'guid' => 'booking_text',
					'tab_title' => 'Booking Text',
					'header' => array(
						'title' => 'Booking Text',
						'description' => 'Information about post booking procedures',
					),
					'items' => array(
						'select_apartment' => array(
							'guid' => 'select_apartment',
							'control' => array(
								'type' => 'text',
								'default' => 'Please select an apartment to continue',
								'allow_empty' => false,
							),
							'label' => 'No Apartment Selected',
							'help'  => 'This text will be shown when no apartment is selected in the form'
						),
						'contact_details' => array(
							'guid' => 'contact_details',
							'control' => array(
								'type' => 'text',
								'default' => 'Make sure to tell us your name and enter a valid email address',
								'allow_empty' => false,
							),
							'label' => 'Contact Details Missing',
							'help'  => 'This text will be shown when the user leaves out the name or the email field'
						),
						'select_date' => array(
							'guid' => 'select_date',
							'control' => array(
								'type' => 'text',
								'default' => 'Please select dates before you continue',
								'allow_empty' => false,
							),
							'label' => 'No Dates Selected',
							'help'  => 'Shown if dates are not selected'
						),
						'invalid_range' => array(
							'guid' => 'invalid_range',
							'control' => array(
								'type' => 'text',
								'default' => 'The range you have selected is not valid for this apartment',
								'allow_empty' => false,
							),
							'label' => 'Invalid Dates Selected',
							'help'  => 'Shown when an invalid date range is selected'
						),
						'invalid_guests' => array(
							'guid' => 'invalid_guests',
							'control' => array(
								'type' => 'text',
								'default' => 'Shown when the user changes apartments, but wants more guests than the new apartment can hold',
								'allow_empty' => false,
							),
							'label' => 'Invalid Guest Count',
							'help'  => 'This text will be shown when no apartment is selected in the form'
						),
						'booking_text' => array(
							'guid' => 'booking_text',
							'control' => array(
								'type' => 'info',
								'nohelp' => true,
								'text' => '
								<p>To further customize the text and make sure nice post-booking text is shown you can use shortcodes</p>

								<p>Use the <strong>[booking_complete]</strong> shortcode to write text which only shows up when the user has completed the booking process.</p>
								<p>Use the <strong>[booking_text]</strong> shortcode to add text which does NOT show up when the user has booked successfully.
								</p>
								<p>In practice, write the directions or additional information inside the [booking_text] shortcode and write the success message in the [booking_complete] shortcode.</p>
								<p>
									Text written outside of the shortocdes will show up in all cases.
								</p>

								',
								'allow_empty' => false,
							),
							'label' => 'Booking Text Shortcodes',
							'help' => ''
						),

					)
				)
			)
		),
	)
);

/** Theme Options
  *
  * Define the theme options you would like to use. This array is
  * set up in much the same way as the custom fields array, except
  * it accomodates for the large groups as well (controled via the
  * sidebar in the admin).
  *
  * The initial members of this array must be arrays themselves. The
  * key must be an identifier for each group. Each array must contain
  * the following members: title, items. The title will be shown
  * in the group list, the items hold the tabs for the setting group.
  *
  * The members of the items array create the tabs in the theme
  * settings. Each tab must be an array with the following members:
  * guid, tab_title, items.
  *
  * The guid stores an identifier for the tab, the tab_title
  * is used to show the tab's title, while the items show
  * the relevant controls. See the controls section of the
  * documentation for more info.
  *
  **/
$eb_defaults['option'] = array(
	'groups' => array(
		'general' => array(
			'title' => 'General',
			'icon' =>  EB_ADMIN_THEME_URL . '/img/groups/general.png',
			'tabs' => array (
				'global' => array(
					'guid' => 'global',
					'tab_title' => 'Global Settings',
					'items' => array(
						'sidebar' => array(
							'guid' => 'sidebar',
							'control' => array(
								'type' => 'select',
								'options' => 'function',
								'function' => array('framework', 'get_sidebars_array'),
								'search' => false,
								'default' => 'sidebar',
								'allow_empty' => false
							),
							'label' => 'Default Sidebar to Show',
							'help'  => 'Select which sidebar you would like to show on all pages, except where a different one is specified.'
						),
						'sidebar_position' => array(
							'guid' => 'sidebar_position',
							'control' => array(
								'type' => 'select',
								'options' => array(
									'Left' => 'left',
									'Right' => 'right',
								),
								'search' => false,
								'default' => $eb_defaults['sidebar_location'],
								'allow_empty' => false
							),
							'label' => 'Default Sidebar Position',
							'help'  => 'Select the default location of the sidebar'
						),
						'show_sidebar' => array(
							'guid' => 'show_sidebar',
							'control' => array(
								'type' => 'checkbox',
								'boxes' => array(
									'Show the sidebar by default?' => 'yes',
								),
								'default' => 'yes',
								'allow_empty' => true,
								'empty_value' => 'no',
							),
							'label' => 'Show the sidebar by default?',
							'help'  => 'Disable the sidebar, except on pages where otherwise specified '
						),
						'show_title' => array(
							'guid' => 'show_title',
							'control' => array(
								'type' => 'checkbox',
								'boxes' => array(
									'Show the page title by default?' => 'yes',
								),
								'default' => 'yes',
								'allow_empty' => true,
								'empty_value' => 'no',
							),
							'label' => 'Show the title by default?',
							'help'  => 'Each page has a title which can be enabled or disabled'
						),
						'show_content' => array(
							'guid' => 'show_content',
							'control' => array(
								'type' => 'checkbox',
								'boxes' => array(
									'Show the page content by default?' => 'yes',
								),
								'default' => 'yes',
								'allow_empty' => true,
								'empty_value' => 'no',
							),
							'label' => 'Show the content by default?',
							'help'  => 'Each page has its content which can be enabled or disabled. While this will work on single pages it doesn\'t make a lot of sense to disable it there. However on special pages like the gallery or postlist pages you may want to disable the content'
						),
						'show_thumbnail' => array(
							'guid' => 'show_thumbnail',
							'control' => array(
								'type' => 'checkbox',
								'boxes' => array(
									'Show the featured image by default?' => 'yes',
								),
								'default' => 'yes',
								'allow_empty' => true,
								'empty_value' => 'no',
							),
							'label' => 'Show the featured image by default?',
							'help'  => 'Select weather the featured image should be shown'
						),
						'boxed_content' => array(
							'guid' => 'boxed_content',
							'control' => array(
								'type' => 'checkbox',
								'boxes' => array(
									'Put content in a white box by default?' => 'yes',
								),
								'default' => 'yes',
								'allow_empty' => true,
								'empty_value' => 'no',
							),
							'label' => 'Boxed Content',
							'help'  => 'Specify weather you would like the content to be boxed or not'
						),
						'boxed_comments' => array(
							'guid' => 'boxed_comments',
							'control' => array(
								'type' => 'checkbox',
								'boxes' => array(
									'Put comments in a white box by default?' => 'yes',
								),
								'default' => 'yes',
								'allow_empty' => true,
								'empty_value' => 'no',
							),
							'label' => 'Boxed Comments',
							'help'  => 'Specify weather you would like the comments to be boxed or not'
						),
						'custom_sidebars' => array(
							'guid' => 'custom_sidebars',
							'control' => array(
								'type' => 'tagsinput',
								'default' => '',
								'allow_empty' => true,
							),
							'label' => 'Custom Sidebars',
							'help'  => 'Add a list of additional sidebars you would like to be available in the admin'
						),
						'footer_page' => array(
							'guid' => 'footer_page',
							'control' => array(
								'type' => 'select',
								'options' => 'function',
								'function' => array('framework', 'get_pages_array'),
								'function_args' => array( 'show_empty' => '-- Don\'t include a page --' ),
								'search' => false,
								'default' => '',
								'allow_empty' => true
							),
							'label' => 'Include Page Above Footer',
							'help'  => 'Select a page to be shown on every page of the website.'
						),
						'pagination_type' => array(
							'guid' => 'pagination_type',
							'control' => array(
								'type' => 'radio',
								'boxes' => array(
									'Full pagination' => 'pagination',
									'Previous and next links only' => 'links',
								),
								'default' => 'pagination',
								'allow_empty' => false
							),
							'label' => 'Pagination Type',
							'help'  => 'Select between full pagination (previous and next links and page numbers), or only previous and next links'
						),
						'analytics' => array(
							'guid' => 'analytics',
							'control' => array(
								'type' => 'textarea',
								'default' => '',
								'allow_empty' => true
							),
							'label' => 'Analytics Code',
							'help'  => 'Add any analytics code here, it will be added just before the closing body tag'
						),
					)
				),
				'home_page' => array(
					'guid' => 'home_page',
					'tab_title' => 'Home Page',
					'items' => array(
						'home_apartment_id' => array(
							'guid' => 'home_apartment_id',
							'control' => array(
								'type' => 'select',
								'options' => 'function',
								'function' => array( 'apartments',  'get_apartment_dropwdown_list' ),
								'search' => true,
								'initial' => array(
									'-- Use the WordPress Settings --' => '',
								),
								'default' => '',
								'allow_empty' => true,
							),
							'label' => 'Select Apartment for Front Page',
							'help'  => 'Select the apartment this widget is for'
						),
					)
				),
				'page_types' => array(
					'guid' => 'page_types',
					'tab_title' => 'Page Types',
					'items' => array(
						'page_settings' => array(
							'guid' => 'page_settings',
							'control' => array(
								'default' => array(
									'sidebar' => 'default',
									'sidebar_position' => 'default',
									'show_sidebar' => 'default',
									'show_title' => 'default',
									'show_thumbnail' => 'default',
									'boxed_content' => 'default',
									'boxed_comments' => 'default'
								),
								'type' => 'page_settings',
								'allow_empty' => array(
									'sidebar' => false,
									'sidebar_position' => false,
									'show_title' => false,
									'show_thumbnail' => false,
									'show_sidebar' => false,
									'boxed_content' => false,
									'boxed_comments' => false
								),
							),
							'label' => 'Single Pages',
							'help'  => 'Select the settings for the page post type.'
						),
						'post_settings' => array(
							'guid' => 'post_settings',
							'control' => array(
								'default' => array(
									'sidebar' => 'default',
									'sidebar_position' => 'default',
									'show_sidebar' => 'default',
									'show_title' => 'default',
									'show_thumbnail' => 'default',
									'boxed_content' => 'default',
									'boxed_comments' => 'default'
								),
								'type' => 'page_settings',
								'allow_empty' => array(
									'sidebar' => false,
									'sidebar_position' => false,
									'show_title' => false,
									'show_thumbnail' => false,
									'show_sidebar' => false,
									'boxed_content' => false,
									'boxed_comments' => false
								),
							),
							'label' => 'Single Posts',
							'help'  => 'Select the settings for the post post type.'
						),
						'template-postlist_settings' => array(
							'guid' => 'template-postlist_settings',
							'control' => array(
								'default' => array(
									'sidebar' => 'default',
									'sidebar_position' => 'default',
									'show_sidebar' => 'default',
									'show_title' => 'default',
									'show_content' => 'default',
									'show_thumbnail' => 'default',
									'boxed_content' => 'default',
									'boxed_comments' => 'default'
								),
								'type' => 'page_settings',
								'allow_empty' => array(
									'sidebar' => false,
									'sidebar_position' => false,
									'show_title' => false,
									'show_thumbnail' => false,
									'show_content' => false,
									'show_sidebar' => false,
									'boxed_content' => false,
									'boxed_comments' => false
								),
							),
							'label' => 'Post List Pages',
							'help'  => 'Select the settings for post list templates.'
						),
						'template-gallery_settings' => array(
							'guid' => 'template-gallery_settings',
							'control' => array(
								'default' => array(
									'sidebar' => 'default',
									'sidebar_position' => 'default',
									'show_sidebar' => 'default',
									'show_title' => 'default',
									'show_content' => 'default',
									'show_thumbnail' => 'default',
									'boxed_content' => 'default',
									'boxed_comments' => 'default'
								),
								'type' => 'page_settings',
								'allow_empty' => array(
									'sidebar' => false,
									'sidebar_position' => false,
									'show_title' => false,
									'show_thumbnail' => false,
									'show_content' => false,
									'show_sidebar' => false,
									'boxed_content' => false,
									'boxed_comments' => false
								),
							),
							'label' => 'Gellery Pages',
							'help'  => 'Select the settings for gallery templates.'
						),
						'tvr_apartment_settings' => array(
							'guid' => 'tvr_apartment_settings',
							'control' => array(
								'default' => array(
									'sidebar' => 'default',
									'sidebar_position' => 'default',
									'show_sidebar' => 'default',
									'show_title' => 'default',
									'show_thumbnail' => 'no',
									'boxed_content' => 'default',
									'boxed_comments' => 'default'
								),
								'type' => 'page_settings',
								'allow_empty' => array(
									'sidebar' => false,
									'sidebar_position' => false,
									'show_title' => false,
									'show_thumbnail' => false,
									'show_sidebar' => false,
									'boxed_content' => false,
									'boxed_comments' => false
								),
							),
							'label' => 'Single Apartments',
							'help'  => 'Select the settings for the apartment post type.'
						),
						'template-apartmentlist_settings' => array(
							'guid' => 'template-apartmentlist_settings',
							'control' => array(
								'default' => array(
									'sidebar_position' => 'default',
									'show_title' => 'default',
									'show_content' => 'default',
									'show_thumbnail' => 'default',
									'boxed_content' => 'default',
									'boxed_comments' => 'default'
								),
								'type' => 'page_settings',
								'allow_empty' => array(
									'sidebar_position' => false,
									'show_title' => false,
									'show_thumbnail' => false,
									'show_content' => false,
									'boxed_content' => false,
									'boxed_comments' => false
								),
							),
							'label' => 'Apartment List Pages',
							'help'  => 'Select the settings for apartment list templates.'
						),
						'template-booknow_settings' => array(
							'guid' => 'template-booknow_settings',
							'control' => array(
								'default' => array(
									'sidebar' => 'default',
									'sidebar_position' => 'default',
									'show_sidebar' => 'no',
									'show_title' => 'default',
									'show_content' => 'default',
									'show_thumbnail' => 'default',
									'boxed_content' => 'default',
									'boxed_comments' => 'default'
								),
								'type' => 'page_settings',
								'allow_empty' => array(
									'sidebar' => false,
									'sidebar_position' => false,
									'show_title' => false,
									'show_thumbnail' => false,
									'show_content' => false,
									'show_sidebar' => false,
									'boxed_content' => false,
									'boxed_comments' => false
								),
							),
							'label' => 'Apartment List Pages',
							'help'  => 'Select the settings for apartment list templates.'
						),

					)
				),
				'layouts' => array(
					'guid' => 'layouts',
					'tab_title' => 'Layouts',
					'items' => array(
						'post_layout' => array(
							'guid' => 'post_layout',
							'control' => array(
								'type' => 'select',
								'options' => 'function',
								'function' => array('framework', 'get_layouts_array'),
								'function_args' => array( 'type' => 'post' ),
								'default' => 'layout-binder',
								'allow_empty' => false
							),
							'label' => 'Default Post Layout',
							'help'  => 'Select a layout to use for posts by default'
						),
						'gallery_layout' => array(
							'guid' => 'gallery_layout',
							'control' => array(
								'type' => 'select',
								'options' => 'function',
								'function' => array('framework', 'get_layouts_array'),
								'function_args' => array( 'type' => 'gallery' ),
								'default' => 'layout-default',
								'allow_empty' => false
							),
							'label' => 'Default Gallery Layout',
							'help'  => 'Select a layout to use for galleries by default'
						),
						'apartmentlist_layout' => array(
							'guid' => 'apartmentlist_layout',
							'control' => array(
								'type' => 'select',
								'options' => 'function',
								'function' => array('framework', 'get_layouts_array'),
								'function_args' => array( 'type' => 'apartmentlist' ),
								'default' => 'layout-default',
								'allow_empty' => false
							),
							'label' => 'Default Apartment List Layout',
							'help'  => 'Select a layout to use for apartment lists by default'
						),
					)
				),
				'currency_settings' => array(
					'guid' => 'currency_settings',
					'tab_title' => 'Currencies',
					'header' => array(
						'title' => '',
						'description' => '',
					),
					'items' => array(
						'currency_symbol' => array(
							'guid' => 'currency_symbol',
							'control' => array(
								'type' => 'text',
								'default' => '$',
								'allow_empty' => false
							),
							'label' => 'Currency',
							'help'  => 'Type the symbol or abbreviation of the currency you would like to use. This will be shown to users when they look at prices. Good Examples are EUR, USD, $ or similar'
						),
						'currency_position' => array(
							'guid' => 'currency_position',
							'id' => 'currency_position',
							'control' => array(
								'type' => 'radio',
								'boxes' => array(
									'Before the amount' => 'before',
									'After the amount' => 'after',
								),
								'default' => 'before',
								'allow_empty' => false
							),
							'label' => 'Currency Position',
							'help'  => 'Select weather you\'d like the currency displayed before or after the amount'
						),
					)
				),

			)
		),
		'social' => array(
			'title' => 'Social',
			'icon' =>  EB_ADMIN_THEME_URL . '/img/groups/social.png',
			'tabs' => array (
				'header_buttons' => array(
					'guid' => 'header_buttons',
					'tab_title' => 'Header Buttons',
					'items' => array(
						'facebook_url' => array(
							'guid' => 'facebook_url',
							'id' => 'facebook_url',
							'control' => array(
								'type' => 'text',
								'default' => get_template_directory_uri(),
								'allow_empty' => true
							),
							'label' => 'Facebook Share URL',
							'help'  => 'Add the url you want to share in the header'
						),
						'twitter_url' => array(
							'guid' => 'twitter_url',
							'id' => 'twitter_url',
							'control' => array(
								'type' => 'text',
								'default' => get_template_directory_uri(),
								'allow_empty' => true
							),
							'label' => 'Twitter Share URL',
							'help'  => 'Add the url you want to share in the header'
						),
					)
				),
			)
		),

		'design' => array(
			'title' => 'Design',
			'icon' =>  EB_ADMIN_THEME_URL . '/img/groups/design.png',
			'tabs' => array (
				'colors' => array(
					'guid' => 'colors',
					'tab_title' => 'Colors',
					'items' => array(
						'primary_color' => array(
							'guid' => 'primary_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => $eb_defaults['colors']['primary'],
								'allow_empty' => false
							),
							'label' => 'Primary Color',
							'help'  => 'The primary color for this website. User for a number of elements.'
						),
						'secondary_color' => array(
							'guid' => 'secondary_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => $eb_defaults['colors']['secondary'],
								'allow_empty' => false
							),
							'label' => 'Secondary Color',
							'help'  => 'The secondary color for this website. User for a number of elements.'
						),
						'site_content_background_color' => array(
							'guid' => 'site_content_background_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#fafafa',
								'allow_empty' => false
							),
							'label' => 'Background Color',
							'help'  => 'The background color of the page. You can also choose an image in the images tab'
						)

					)
				),
				'header' => array(
					'guid' => 'header',
					'tab_title' => 'Header',
					'items' => array(
						'site_header_background_color' => array(
							'guid' => 'site_header_background_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#e1e1e1',
								'allow_empty' => false
							),
							'label' => 'Header Background Color',
							'help'  => 'This color will be used for the background of the header'
						),
						'site_header_background_image' => array(
							'guid' => 'site_header_background_image',
							'control' => array(
								'type' => 'upload',
								'default' => get_template_directory_uri() . '/img/defaults/site_header_background_image.png',
								'allow_empty' => true
							),
							'label' => 'Header Background Image',
							'help'  => 'This image will be tiled and used as the background for the header of the site'
						),
						'site_header_text_color' => array(
							'guid' => 'site_header_text_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#4C4841',
								'allow_empty' => false
							),
							'label' => 'Header Text Color',
							'help'  => 'This color will be used for the text in the header'
						),
						'site_header_link_color' => array(
							'guid' => 'site_header_link_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#4C4841',
								'allow_empty' => false
							),
							'label' => 'Header Link Color',
							'help'  => 'This color will be used for the links in the header'
						),
						'site_header_navigation_separator' => array(
							'guid' => 'site_header_navigation_separator',
							'control' => array(
								'type' => 'upload',
								'default' => get_template_directory_uri() . '/img/defaults/site_header_navigation_separator.png',
								'allow_empty' => true
							),
							'label' => 'Header Navigation Separators',
							'help'  => 'This image will be used to separate the navigation items'
						),
						'site_header_height' => array(
							'guid' => 'site_header_height',
							'control' => array(
								'type' => 'text',
								'default' => '43px',
								'allow_empty' => false
							),
							'label' => 'Header Height',
							'help'  => 'If your background image is higher than the default one please add the correct height here. We use this height to correctly position the text vertically as well. This can be any valid CSS value, so don\'t forget to add the unit of measurement at the end.'
						),

					)
				),
				'footer' => array(
					'guid' => 'footer',
					'tab_title' => 'Footer',
					'items' => array(
						'site_footer_background_color' => array(
							'guid' => 'site_footer_background_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#3d3d3d',
								'allow_empty' => false
							),
							'label' => 'Footer Background Color',
							'help'  => 'This color will be used as the background of the footer'
						),
						'site_footer_background_image' => array(
							'guid' => 'site_footer_background_image',
							'control' => array(
								'type' => 'upload',
								'default' => get_template_directory_uri() . '/img/defaults/site_footer_background_image.png',
								'allow_empty' => true
							),
							'label' => 'Footer Background Image',
							'help'  => 'This image will be tiled and used as the background for the footer of the site'
						),
						'site_footer_text_color' => array(
							'guid' => 'site_footer_text_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#aaaaaa',
								'allow_empty' => false
							),
							'label' => 'Footer Text Color',
							'help'  => 'This color will be used for the text in the footer'
						),
						'site_footer_heading_color' => array(
							'guid' => 'site_footer_heading_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#ffffff',
								'allow_empty' => false
							),
							'label' => 'Footer Heading Color',
							'help'  => 'This color will be used for the headings in the footer'
						),
						'site_footer_link_color' => array(
							'guid' => 'site_footer_link_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#aaaaaa',
								'allow_empty' => false
							),
							'label' => 'Footer Link Color',
							'help'  => 'This color will be used for the links in the footer'
						),
						'site_footer_copyright_text' => array(
							'guid' => 'site_footer_copyright_text',
							'control' => array(
								'type' => 'text',
								'default' => 'Copyright &copy; ' . date( 'Y' ) . ' ' . get_bloginfo(),
								'allow_empty' => false
							),
							'label' => 'Copyright Text',
							'help'  => 'The copyright text to show in the footer'
						),
							'site_footer_list_separator_color' => array(
							'guid' => 'site_footer_list_separator_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#454545',
								'allow_empty' => false
							),
							'label' => 'List Separator Color',
							'help'  => 'Adjust the color of the list separator line'
						),
					)
				),
				'sidebar' => array(
					'guid' => 'sidebar',
					'tab_title' => 'Sidebar',
					'items' => array(
						'site_sidebar_text_color' => array(
							'guid' => 'site_sidebar_text_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#4C4841',
								'allow_empty' => false
							),
							'label' => 'Sidebar Text Color',
							'help'  => 'This color will be used for the text in the sidebar'
						),
						'site_sidebar_heading_color' => array(
							'guid' => 'site_sidebar_heading_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#4d4d4d',
								'allow_empty' => false
							),
							'label' => 'Sidebar Heading Color',
							'help'  => 'This color will be used for the headings in the sidebar'
						),
						'site_sidebar_background_color' => array(
							'guid' => 'site_sidebar_background_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#ffffff',
								'allow_empty' => false
							),
							'label' => 'Sidebar Background Color',
							'help'  => 'This color will be used for the background of the sidebar'
						),
						'site_sidebar_link_color' => array(
							'guid' => 'site_sidebar_link_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#4C4841',
								'allow_empty' => false
							),
							'label' => 'Sidebar Link Color',
							'help'  => 'This color will be used for some links in the sidebar. Note that due to the amount of link-lists found in the sidebar the "usual" color for the links will be the text color'
						),
							'site_sidebar_list_separator_color' => array(
							'guid' => 'site_sidebar_list_separator_color',
							'control' => array(
								'type' => 'colorpicker',
								'default' => '#e1e1e1',
								'allow_empty' => false
							),
							'label' => 'List Separator Color',
							'help'  => 'Adjust the color of the list separator line'
						),

					)
				),
				'images' => array(
					'guid' => 'images',
					'tab_title' => 'Images',
					'items' => array(
						'logo' => array(
							'guid' => 'logo',
							'control' => array(
								'type' => 'upload',
								'default' => get_template_directory_uri() . '/img/defaults/logo.png',
								'allow_empty' => true
							),
							'label' => 'Logo',
							'help'  => 'The logo for your site, used in the header'
						),
						'site_content_background_image' => array(
							'guid' => 'site_content_background_image',
							'control' => array(
								'type' => 'upload',
								'default' => get_template_directory_uri() . '/img/defaults/site_content_background_image.png',
								'allow_empty' => true
							),
							'label' => 'Background Image',
							'help'  => 'This image will be tiled and used as the background for your site'
						),
						'favicon' => array(
							'guid' => 'favicon',
							'control' => array(
								'type' => 'upload',
								'default' => '',
								'allow_empty' => true
							),
							'label' => 'Favicon',
							'help'  => 'This 16x16 .ico file will be shown in browser bars and bookmarks'
						),
						'apple_touch_icon' => array(
							'guid' => 'apple_touch_icon',
							'control' => array(
								'type' => 'upload',
								'default' => '',
								'allow_empty' => true
							),
							'label' => 'Apple Touch Icon',
							'help'  => 'This 144x144 .png file will be shown on apple devices on the home screen'
						),
						'website_image' => array(
							'guid' => 'website_image',
							'control' => array(
								'type' => 'upload',
								'default' => '',
								'allow_empty' => true,
							),
							'label' => 'Website Image',
							'help' => 'The Website Image is an image associated to with your website. Facebook uses this image when users share links. For your posts the featured image is used. The image specified here is used for all pages which don\'t have featured images specified.'
						),
						'hoverlink' => array(
							'guid' => 'hoverlink',
							'control' => array(
								'type' => 'upload',
								'default' => get_template_directory_uri() . '/img/defaults/link.png',
								'allow_empty' => true
							),
							'label' => 'Image Hover Link',
							'help'  => 'This image will show up when you hover over an image link'
						),
					)
				),
				'fonts' => array(
					'guid' => 'fonts',
					'tab_title' => 'Fonts',
					'items' => array(
						'heading_font' => array(
							'guid' => 'heading_font',
							'control' => array(
								'type' => 'font',
								'default' => array(
									'name' => 'Helvetica Neue',
									'type' => 'Sans-serif',
									'fallback' => 'Helvetica, Arial'
								),
								'allow_empty' => array(
									'name' => false,
									'type' => false,
									'fallback' => false
								)
							),
							'label' => 'Heading Fonts',
							'help'  => 'The font to use for all headings on the site'
						),
						'body_font' => array(
							'guid' => 'body_font',
							'control' => array(
								'type' => 'font',
								'default' => array(
									'name' => 'Helvetica Neue',
									'type' => 'Sans-serif',
									'fallback' => 'Helvetica, Arial'
								),
								'allow_empty' => array(
									'name' => false,
									'type' => false,
									'fallback' => false
								)
							),
							'label' => 'Body Fonts',
							'help'  => 'The font to use for all body text on the site'
						),

					)
				),

			)
		),
		'text' => array(
			'title' => 'Text',
			'icon' =>  EB_ADMIN_THEME_URL . '/img/groups/text.png',
			'tabs' => array (
				'messages' => array(
					'guid' => 'messages',
					'tab_title' => 'Messages',
					'items' => array(
						'404_title' => array(
							'guid' => '404_title',
							'control' => array(
								'type' => 'text',
								'default' => 'Oh Noes!',
								'allow_empty' => false
							),
							'label' => '404 Error Title',
							'help'  => 'The title shown on the 404 error page'
						),
						'404_message' => array(
							'guid' => '404_message',
							'control' => array(
								'type' => 'textarea',
								'default' => 'Sorry, there seems to be no content on this page.',
								'allow_empty' => true
							),
							'label' => '404 Error Message',
							'help'  => 'The message shown on the 404 error page'
						),
						'no_posts_title' => array(
							'guid' => 'no_posts_title',
							'control' => array(
								'type' => 'text',
								'default' => 'There are no posts here',
								'allow_empty' => false
							),
							'label' => 'No Posts Title',
							'help'  => 'This is the title of the message shown when a page exists, but there are no posts on it. An example is a gallery page which contains a list of posts from the "News" category. If the "News" category doesn\'t have posts the gallery page still exists, but it has no posts to show.'
						),
						'no_posts_message' => array(
							'guid' => 'no_posts_message',
							'control' => array(
								'type' => 'text',
								'default' => 'You are in the right place, but it seems that there are no posts that we can show here :(',
								'allow_empty' => true
							),
							'label' => 'No Posts Message',
							'help'  => 'This is the message shown when a page exists, but there are no posts on it. An example is a gallery page which contains a list of posts from the "News" category. If the "News" category doesn\'t have posts the gallery page still exists, but it has no posts to show.'
						),
						'no_search_results_title' => array(
							'guid' => 'no_search_results_title',
							'control' => array(
								'type' => 'text',
								'default' => 'Ooops',
								'allow_empty' => false
							),
							'label' => 'No Search Results Title',
							'help'  => 'The title shown on the page if there are no search results'
						),
						'no_search_results_message' => array(
							'guid' => 'no_search_results_message',
							'control' => array(
								'type' => 'textarea',
								'default' => 'There are no results for your search :(',
								'allow_empty' => true
							),
							'label' => 'No Search Results Message',
							'help'  => 'The message shown on the page if there are no search results'
						),
						'password_protected_title' => array(
							'guid' => 'password_protected_title',
							'control' => array(
								'type' => 'text',
								'default' => 'This post is password protected',
								'allow_empty' => false
							),
							'label' => 'Password Protected Post Title',
							'help'  => 'This is the title of the message shown when someone visits a post which is password protected.'
						),
						'password_protected_message' => array(
							'guid' => 'password_protected_message',
							'control' => array(
								'type' => 'textarea',
								'default' => 'This post can only be read by people who have the password. If you know it, please enter it into the form to unlock this post',
								'allow_empty' => true
							),
							'label' => 'Password Protected Post Message',
							'help'  => 'This is the message shown when someone visits a post which is password protected.'
						),
						'comments_closed_title' => array(
							'guid' => 'comments_closed_title',
							'control' => array(
								'type' => 'text',
								'default' => 'This comments for this post have been closed',
								'allow_empty' => false
							),
							'label' => 'Comments Closed Title',
							'help'  => 'This is the title of the message shown for posts which have closed comments.'
						),
						'comments_closed_message' => array(
							'guid' => 'comments_closed_message',
							'control' => array(
								'type' => 'textarea',
								'default' => 'This means the no further comments will be accepted',
								'allow_empty' => true
							),
							'label' => 'Comments Closed Message',
							'help'  => 'This is message shown for posts which have closed comments.'
						),

					)
				),
				'page_titles' => array(
					'guid' => 'page_titles',
					'tab_title' => 'Page Titles',
					'items' => array(
						'home_page_title' => array(
							'guid' => 'home_page_title',
							'control' => array(
								'type' => 'text',
								'default' => 'Home',
								'allow_empty' => false
							),
							'label' => 'Home Page Title',
							'help'  => 'This title will be shown on the home page. If you don\'t want to show a title, leave it blank',
						),
						'search_page_title' => array(
							'guid' => 'search_page_title',
							'control' => array(
								'type' => 'text',
								'default' => 'Search: %s',
								'allow_empty' => false
							),
							'label' => 'Search Page Title',
							'help'  => 'This title will be shown on the search page. If you don\'t want to show a title, leave it blank. Use %s as a placeholder for the search terms entered.',
						),
						'category_page_title' => array(
							'guid' => 'category_page_title',
							'control' => array(
								'type' => 'text',
								'default' => 'Categories: %s',
								'allow_empty' => false
							),
							'label' => 'Category Archive Title',
							'help'  => 'This title will be shown on the category archives page. If you don\'t want to show a title, leave it blank. Use %s as a placeholder for the actual category name',
						),
						'tag_page_title' => array(
							'guid' => 'tag_page_title',
							'control' => array(
								'type' => 'text',
								'default' => 'Tags: %s',
								'allow_empty' => false
							),
							'label' => 'Tag Archive Title',
							'help'  => 'This title will be shown on the tag archives page. If you don\'t want to show a title, leave it blank. Use %s as a placeholder for the actual tag name',
						),
						'yearly_page_title' => array(
							'guid' => 'yearly_page_title',
							'control' => array(
								'type' => 'text',
								'default' => 'Yearly Archives: %s',
								'allow_empty' => false
							),
							'label' => 'Yearly Archives Title',
							'help'  => 'This title will be shown on the yearly archives page. If you don\'t want to show a title, leave it blank. Use %s as a placeholder for the actual year',
						),
						'monthly_page_title' => array(
							'guid' => 'monthly_page_title',
							'control' => array(
								'type' => 'text',
								'default' => 'Monthly Archives: %s',
								'allow_empty' => false
							),
							'label' => 'Monthly Archives Title',
							'help'  => 'This title will be shown on the monthly archives page. If you don\'t want to show a title, leave it blank. Use %s as a placeholder for the actual month',
						),
						'daily_page_title' => array(
							'guid' => 'daily_page_title',
							'control' => array(
								'type' => 'text',
								'default' => 'Daily Archives: %s',
								'allow_empty' => false
							),
							'label' => 'Daily Archives Title',
							'help'  => 'This title will be shown on the daily archives page. If you don\'t want to show a title, leave it blank. Use %s as a placeholder for the actual day',
						),
						'author_page_title' => array(
							'guid' => 'author_page_title',
							'control' => array(
								'type' => 'text',
								'default' => 'Authors: %s',
								'allow_empty' => false
							),
							'label' => 'Authors Page Title',
							'help'  => 'This title will be shown on the author archives page. If you don\'t want to show a title, leave it blank. Use %s as a placeholder for the actual author',
						),
					)
				),
				'misc' => array(
					'guid' => 'misc',
					'tab_title' => 'Misc',
					'items' => array(
						'read_more' => array(
							'guid' => 'read_more',
							'control' => array(
								'type' => 'text',
								'default' => 'Read more...',
								'allow_empty' => false
							),
							'label' => 'Read More Text',
							'help'  => 'This text will be shown wherever there is a read more link'
						),
					)
				),

			)
		),
		'documentation' => array(
			'title' => 'Documentation',
			'icon' =>  EB_ADMIN_THEME_URL . '/img/groups/documentation.png',
			'tabs' => array (
				'videos' => array(
					'guid' => 'videos',
					'tab_title' => 'Videos',
					'items' => array(
						'documentation_videos' => array(
							'guid' => 'documentation_videos',
							'nolabel' => true,
							'nohelp' => true,
							'control' => array(
								'type' => 'custom',
								'function' => 'tvr_docs',
								'default' => false,
								'allow_empty' => false
							),
						),
					)
				),
				'readme' => array(
					'guid' => 'readme',
					'tab_title' => 'Readme',
					'items' => array(
						'readme' => array(
							'guid' => 'readme',
							'nolabel' => true,
							'nohelp' => true,
							'control' => array(
								'type' => 'custom',
								'function' => 'tvr_readme',
								'default' => false,
								'allow_empty' => false
							),
						),
					)
				),

			)
		),
		'reset' => array(
			'title' => 'Factory Reset',
			'icon' =>  EB_ADMIN_THEME_URL . '/img/groups/reset.png',
			'tabs' => array (
				'Factory Reset' => array(
					'guid' => 'reset',
					'tab_title' => 'Factory Reset',
					'items' => array(
						'reset' => array(
							'guid' => 'reset',
							'nolabel' => true,
							'nohelp' => true,
							'control' => array(
								'type' => 'reset',
								'default' => false,
								'allow_empty' => false
							),
						),
					)
				),
			)
		),
	)

);

?>