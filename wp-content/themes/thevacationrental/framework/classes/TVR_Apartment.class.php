<?php


class TVR_Apartment {

	public $data;
	public $ID;
	public $name;
	public $maximum_guests;
	public $default_price;
	public $apartment_details;
	public $seasonal_pricing;
	public $amenities;
	public $type;
	public $current_price;
	public $excerpt;
	public $book_now_page;

	function __construct( $apartment_id = null ) {

		$this->set_apartment( $apartment_id );

	}

	function get_apartment( $apartment_id ) {
		global $wpdb;

		$apartment = $wpdb->get_row( "SELECT * FROM $wpdb->posts WHERE ID = $apartment_id AND post_type = 'tvr_apartment' AND post_status = 'publish' " );

		if( empty( $apartment ) ) {
			return false;
		}

		$metas = $wpdb->get_results( "SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id = $apartment_id " );

		if( !empty( $metas ) ) {
			foreach( $metas as $meta ) {
				$key = $meta->meta_key;

				$value = @unserialize($meta->meta_value);
				if( !is_array( $value ) ) {
					$value = $meta->meta_value;
				}


				$apartment->$key = $value;
			}
		}

		return $apartment;

	}


	function set_apartment( $apartment_id ) {
		$apartment = $this->get_apartment( $apartment_id );

		$this->data           = false;
		$this->ID             = false;
		$this->name           = false;
		$this->maximum_guests = false;
		$this->default_price  = false;
		$this->current_price  = false;
		$this->excerpt        = false;
		$this->book_now_page  = false;

		if( !empty( $apartment ) ) {
			$this->data = $apartment;
			$this->ID = $apartment->ID;
			$this->name = $apartment->post_title;
			$this->maximum_guests = $apartment->maximum_guests;
			$this->default_price = $apartment->default_price;

			$this->seasonal_pricing = false;
			if( !empty( $apartment->seasonal_pricing ) ) {
				$seasonal_pricing = array();
				for( $i=0; $i < count( $apartment->seasonal_pricing['name'] ); $i++ ) {
					$seasonal_pricing[] = array(
						'name'         => $apartment->seasonal_pricing['name'][$i],
						'start_date'   => $apartment->seasonal_pricing['start_date'][$i],
						'end_date'     => $apartment->seasonal_pricing['end_date'][$i],
						'price'        => $apartment->seasonal_pricing['price'][$i],
					);
				}
				$this->seasonal_pricing = $seasonal_pricing;
			}

			$this->apartment_details = false;
			if( !empty( $apartment->apartment_details ) ) {
				$apartment_details = array();
				for( $i=0; $i < count( $apartment->apartment_details['name'] ); $i++ ) {
					$apartment_details[$apartment->apartment_details['name'][$i]] = $apartment->apartment_details['value'][$i];
				}
				$this->apartment_details = $apartment_details;
			}

			$this->amenities = false;
			$amenities = wp_get_object_terms( $apartment_id, 'tvr_amenity' );
			if( !empty( $amenities ) AND !( is_object( $amenities ) AND get_class( $amenities ) == 'WP_Error' ) ) {
				$apartment_amenities = array();
				foreach( $amenities as $amenity ) {
					$apartment_amenities[$amenity->term_id] = $amenity->name;
				}
				$this->amenities = $apartment_amenities;
			}

			$this->type = false;
			$types = wp_get_object_terms( $apartment_id, 'tvr_apartment_type' );

			if( !empty( $types ) AND !( is_object( $types ) AND get_class( $types ) == 'WP_Error' ) ) {
				$apartment_types = array();
				foreach( $types as $type ) {
					$apartment_types[$type->term_id] = $type->name;
				}
				$this->type = $apartment_types;
			}

			$this->current_price = $this->get_price_on_date();

			if( $apartment->post_excerpt != '' ) {
				$this->excerpt = wpautop( $apartment->post_excerpt );
			}
			else {
				$excerpt = preg_replace( '|\[(.+?)\](.+?\[/\\1\])?|s', '', $apartment->post_content );
				$excerpt = strip_tags( $excerpt );
				if( strlen( $excerpt ) > 250 ) {
					$excerpt = substr( $excerpt, 0, 250 );
					$spos = strrpos( $excerpt, ' ' );
					$excerpt = substr( $excerpt, 0, $spos ) . '...';
				}
				$this->excerpt = $excerpt;
			}

		}

		$this->book_now_page = get_post_meta( $apartment->ID, 'book_now_page', true );

	}


	function get_unavailable_dates() {
		global $wpdb;
		$bookings = $wpdb->get_col( "
			SELECT ID FROM $wpdb->posts WHERE
				post_type = 'tvr_booking' AND
				post_status IN ('publish', 'draft') AND
				ID IN ( SELECT post_id FROM $wpdb->postmeta WHERE
					meta_value = " . $this->ID . " AND
					meta_key = 'apartment' )
		" );
		$bookings = implode( ',', $bookings );

		$booking_dates = $wpdb->get_col( "
			SELECT meta_value FROM $wpdb->postmeta WHERE
				meta_key = 'stay_interval' AND
				post_id IN ($bookings)
		" );

		$dates = array();

		if( !empty( $booking_dates ) ) {
			foreach( $booking_dates as $booking_date ){

				$booking_date = unserialize( $booking_date );
				$start = date( 'Y-m-d', strtotime( $booking_date['start_date'] ) );
				$end = date( 'Y-m-d', strtotime( $booking_date['end_date'] ) );
				$unavailable = $this->create_date_range( $start, $end );
				array_pop( $unavailable );
				$dates = array_merge( $unavailable, $dates );

			}
		}

		return $dates;
	}


	function get_daterange_price( $start, $end ) {
		$start = date('Y-m-d', strtotime( $start ) );
		$end = date('Y-m-d', strtotime( $end ) );
		$range = $this->create_date_range( $start, $end );
		array_pop( $range );

		$total = 0;
		if( !empty( $range ) ) {
			foreach( $range as $date ) {
				$total = $total + $this->get_price_on_date( $date );
			}
		}

		return $total;
	}


	function get_price_on_date( $date = 'now' ) {
		$price = $this->default_price;

		$time = ( $date == 'now' ) ? time() : strtotime( $date );
		$seasonal_data = $this->seasonal_pricing;

		foreach( $seasonal_data as $season ) {
			$start = strtotime( $season['start_date'] . ' ' . date( 'Y' ) );
			$end = strtotime( $season['end_date'] . ' ' . date( 'Y' ) );

			if( $end < $start ) {
				$jan1 = strtotime( date( 'Y' ) . '-01-01' );
				$dec31 = strtotime( date( 'Y' ) . '-12-31' );
				if( ( $time >= $jan1 AND $time <= $end ) OR ( $time >= $start AND $time <= $dec31 ) ) {
					$price = $season['price'];
				}
				$jan1 = strtotime( date( 'Y', mktime(0,0,0,0,0, date('Y') + 1) ) . '-01-01' );
				$dec31 = strtotime( date( 'Y', mktime(0,0,0,0,0, date('Y') + 1) ) . '-12-31' );
				if( ( $time >= $jan1 AND $time <= $end ) OR ( $time >= $start AND $time <= $dec31 ) ) {
					$price = $season['price'];
				}
				$jan1 = strtotime( date( 'Y', mktime(0,0,0,0,0, date('Y') + 2) ) . '-01-01' );
				$dec31 = strtotime( date( 'Y', mktime(0,0,0,0,0, date('Y') + 2) ) . '-12-31' );
				if( ( $time >= $jan1 AND $time <= $end ) OR ( $time >= $start AND $time <= $dec31 ) ) {
					$price = $season['price'];
				}
			}
			else {
				if( $start <= $time AND $end >= $time ) {
					$price = $season['price'];
				}
			}


		}

		return $price;
	}

	function create_date_range( $start, $end ) {
	    $range = array();

	    $start = mktime( 1, 0, 0, substr( $start, 5, 2 ), substr( $start, 8, 2), substr( $start, 0, 4 ) );
	    $end = mktime( 1, 0, 0, substr( $end, 5, 2 ), substr( $end, 8, 2 ), substr( $end, 0, 4 ) );

	    if ( $end >= $start ) {
	        array_push( $range, date( 'Y-m-d', $start ) );

	        while ( $start < $end ) {
	            $start += 86400;
	            array_push( $range, date( 'Y-m-d', $start ) );
	        }
	    }

	    return $range;
	}




}




































































?>