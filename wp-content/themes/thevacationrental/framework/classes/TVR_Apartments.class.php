<?php
class TVR_Apartments {

	public $apartments;

	function __construct( $args = array() ) {
		$defaults = array(
			'fields' => array( 'apartment_details', 'seasonal_pricing', 'default_price', '_thumbnail_id' ),
		);
		$args = wp_parse_args( $args, $defaults);

		add_action( 'init', array( $this, 'custom_post_apartment' ), 5 );
		add_filter( 'post_updated_messages', array( $this, 'custom_post_messages_apartment' ) );

		add_action( 'init', array( $this, 'custom_post_booking' ), 5 );
		add_filter( 'post_updated_messages', array( $this, 'custom_post_messages_booking' ) );

		add_action( 'init', array( $this, 'taxonomy_apartment_amenity' ), 10 );
		add_action( 'init', array( $this, 'taxonomy_apartment_type' ), 10 );

		add_action( 'init', array( $this, 'setup_apartments' ), 11 );

	}



	function setup_apartments( ) {
		$apartment_ids = $this->get_apartments();
		$apartments = array();

		if( !empty( $apartment_ids ) ) {
			foreach( $apartment_ids as $apartment_id ) {
				$apartments[] = new TVR_Apartment( $apartment_id );
			}
		}

		$this->apartments = $apartments;
		$this->detail_list = $this->get_detail_list();

	}

	function get_price_range() {
		foreach( $this->apartments as $apartment ) {
			$prices[] = $apartment->default_price;
			if( !empty( $apartment->seasonal_pricing[0]['name'] ) ) {
				foreach( $apartment->seasonal_pricing as $price ) {
					$prices[] = $price['price'];
				}
			}
		}

		foreach( $prices as $key => $value ) {
			if( empty( $value ) ) {
				unset( $prices[$key] );
			}
		}

		$price = array();
		$price[0] = min( $prices );
		$price[1] = max( $prices );

		return $price;
	}


	function taxonomy_apartment_amenity() {
		$labels = array(
			'name'              => 'Amenities',
			'singular_name'     => 'Feature',
			'search_items'      => 'Search Amenities',
			'all_items'         => 'All Amenities',
			'parent_item'       => 'Parent Amenity',
			'parent_item_colon' => 'Parent Amenity:',
			'edit_item'         => 'Edit Amenity',
			'update_item'       => 'Update Amenity',
			'add_new_item'      => 'Add New Amenity',
			'new_item_name'     => 'New Amenity',
			'menu_name'         => 'Apartment Amenities',
		);

		register_taxonomy( 'tvr_amenity', array( 'tvr_apartment' ), array(
			'hierarchical' => true,
			'labels'       => $labels,
			'show_ui'      => true,
			'query_var'    => true,
			'rewrite'      => array( 'slug' => 'tvr_amenity' ),
		));
	}

	function taxonomy_apartment_type() {
		$labels = array(
			'name'              => 'Property Types',
			'singular_name'     => 'Property Type',
			'search_items'      => 'Search Property Typess',
			'all_items'         => 'All Property Types',
			'parent_item'       => 'Parent Property Type',
			'parent_item_colon' => 'Parent Property Type:',
			'edit_item'         => 'Edit Property Type',
			'update_item'       => 'Update Property Type',
			'add_new_item'      => 'Add New Property Type',
			'new_item_name'     => 'New Property Type',
			'menu_name'         => 'Property Types',
		);

		register_taxonomy( 'tvr_apartment_type', array( 'tvr_apartment' ), array(
			'hierarchical' => true,
			'labels'       => $labels,
			'show_ui'      => true,
			'query_var'    => true,
			'rewrite'      => array( 'slug' => 'tvr_apartment_type' ),
		));
	}

	function custom_post_apartment() {
		$labels = array(
			'name'                => 'Apartments',
			'singular_name'       => 'Apartment',
			'add_new'             => 'Add New',
			'add_new_item'        => 'Add New Apartment',
			'edit_item'           => 'Edit Apartment',
			'new_item'            => 'New Apartment',
			'all_items'           => 'All Apartments',
			'view_item'           => 'View Apartment',
			'search_items'        => 'Search Apartments',
			'not_found'           => 'No apartments found',
			'not_found_in_trash'  => 'No apartments found in trash',
			'parent_item_colon'   => '',
			'menu_name'           => 'Apartments'
		);

		$args = array(
			'labels' => $labels,
			'public' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'has_archive' => true,
			'hierarchical' => false,
			'menu_position' => null,
			'taxonomies' => array('rf_apartment_feature'),
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
		);

		register_post_type( 'tvr_apartment', $args );
	}

	function custom_post_messages_apartment() {
		global $post, $post_ID;

		$messages['tvr_apartment'] = array(
			0  => '',
			1  => sprintf( 'Apartment updated. <a href="%s">View apartment</a>', esc_url( get_permalink( $post_ID ) ) ),
			2  => 'Custom field updated.',
			3  => 'Custom field updated.',
			4  => 'Apartment updated.',
			5  => isset( $_GET['revision']) ? sprintf( 'Apartment restored to revision from %s', wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
			6  => sprintf( 'Apartment published. <a href="%s">View apartment</a>', esc_url( get_permalink( $post_ID ) ) ),
			7  => 'Apartment saved.',
			8  => sprintf( 'Apartment submitted. <a target="_blank" href="%s">Preview apartment</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink( $post_ID ) ) ) ),
			9  => sprintf( 'Apartment scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview apartment</a>',
				date_i18n( __( 'M j, Y @ G:i', 'rf_themes' ), strtotime( $post->post_date ) ), esc_url( get_permalink( $post_ID ) ) ),
			10 => sprintf( 'Apartment draft updated. <a target="_blank" href="%s">Preview apartment</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink( $post_ID ) ) ) ),
		);

		return $messages;
	}



	function custom_post_booking() {
		$labels = array(
			'name'                => 'Bookings',
			'singular_name'       => 'Booking',
			'add_new'             => 'Add New',
			'add_new_item'        => 'Add New Booking',
			'edit_item'           => 'Edit Booking',
			'new_item'            => 'New Booking',
			'all_items'           => 'All Bookings',
			'view_item'           => 'View Booking',
			'search_items'        => 'Search Bookings',
			'not_found'           => 'No bookings found',
			'not_found_in_trash'  => 'No bookings found in trash',
			'parent_item_colon'   => '',
			'menu_name'           => 'Bookings'
		);

		$args = array(
			'labels' => $labels,
			'public' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'has_archive' => true,
			'hierarchical' => false,
			'menu_position' => null,
			'supports' => array( 'title', 'editor' )
		);

		register_post_type( 'tvr_booking', $args );
	}

	function custom_post_messages_booking() {
		global $post, $post_ID;

		$messages['tvr_booking'] = array(
			0  => '',
			1  => sprintf( 'Booking updated. <a href="%s">View booking</a>', esc_url( get_permalink( $post_ID ) ) ),
			2  => 'Custom field updated.',
			3  => 'Custom field updated.',
			4  => 'Booking updated.',
			5  => isset( $_GET['revision']) ? sprintf( 'Booking restored to revision from %s', wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
			6  => sprintf( 'Booking published. <a href="%s">View booking</a>', esc_url( get_permalink( $post_ID ) ) ),
			7  => 'Booking saved.',
			8  => sprintf( 'Booking submitted. <a target="_blank" href="%s">Preview booking</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink( $post_ID ) ) ) ),
			9  => sprintf( 'Booking scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview booking</a>',
				date_i18n( __( 'M j, Y @ G:i', 'rf_themes' ), strtotime( $post->post_date ) ), esc_url( get_permalink( $post_ID ) ) ),
			10 => sprintf( 'Booking draft updated. <a target="_blank" href="%s">Preview booking</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink( $post_ID ) ) ) ),
		);

		return $messages;
	}



	function get_apartments( $args = '' ) {
		global $wpdb;

		$defaults = array(
			'fields' => array( 'apartment_details', 'seasonal_pricing', 'default_price', '_thumbnail_id' ),
		);

		$args = wp_parse_args( $args, $defaults);

		$fields_string = "'" . implode( "', '", $args['fields'] ) . "'";
		$apartments = $wpdb->get_col( "SELECT DISTINCT( post_id ) FROM $wpdb->postmeta WHERE meta_key IN( $fields_string ) AND post_id IN ( SELECT ID FROM $wpdb->posts WHERE post_type = 'tvr_apartment' AND post_status = 'publish' ) " );

		return $apartments;
	}


	function get_detail_list() {
		$apartment_data = $this->apartments;
		$detail_list = array();
		foreach( $apartment_data as $apartment ) {
			if( !empty( $apartment->apartment_details ) ) {
				$detail_list = array_merge( $detail_list, array_keys( $apartment->apartment_details ) );
			}
		}
		return $detail_list;
	}


	function get_apartment_dropwdown_list() {
		global $wpdb;
		$apartments = $wpdb->get_results( "SELECT ID, post_title FROM $wpdb->posts WHERE post_type = 'tvr_apartment' AND post_status = 'publish' ORDER BY post_title ASC" );
		$options = array();
		foreach( $apartments as $apartment ) {
			$options[$apartment->post_title] = $apartment->ID;
		}
		return $options;
	}


	function get_details_array() {
		$options = array();
		if( !empty( $this->detail_list ) ) {
			foreach( $this->detail_list as $detail ) {
				$options[$detail] = $detail;
			}
		}

		return $options;
	}




	function arrange_data( $apartment_raw_data ) {
		$apartment_data = array();
		foreach( $apartment_raw_data as $data ) {
			$value = @unserialize( $data->meta_value );
			$value = ( is_array( $value ) ) ? $value : $data->meta_value;
			$apartment_data[$data->post_id][$data->meta_key] = call_user_func( array( $this, 'get_' . $data->meta_key . '_data' ), $value );

			$post = get_post( $data->post_id );
			$apartment_data[$data->post_id]['post_title'] = $post->post_title;

			if( !empty( $post->post_excerpt ) ){
				$excerpt = wpautop($post->post_excerpt);
			}
			else {
				$excerpt = preg_replace( '|\[(.+?)\](.+?\[/\\1\])?|s', '', $post->post_content );
				$excerpt = strip_tags( $excerpt );
				if( strlen( $excerpt ) > 250 ) {
					$excerpt = substr( $excerpt, 0, 250 );
					$spos = strrpos( $excerpt, ' ' );
					$excerpt = substr( $excerpt, 0, $spos ) . '...';
				}
			}

			$apartment_data[$data->post_id]['post_excerpt'] =  $excerpt;
		}

		foreach( $apartment_data as $apartment_id => $data ) {
			$apartment_data[$apartment_id]['current_price'] = $this->get_apartment_price_on_date( 0, $apartment_data[$apartment_id]['default_price'], $apartment_data[$apartment_id]['seasonal_pricing'] );

			$apartment_types = wp_get_object_terms( $apartment_id, 'tvr_apartment_type' );
			if( is_array( $apartment_types ) AND !empty( $apartment_types ) ) {
				foreach( $apartment_types as $type ) {
					$apartment_data[$apartment_id]['tvr_apartment_type'][$type->name] = $type->term_id;
				}
			}


			$apartment_features = wp_get_object_terms( $apartment_id, 'tvr_amenity' );
			if( is_array( $apartment_features ) AND !empty( $apartment_features ) ) {
				foreach( $apartment_features as $feature ) {
					$apartment_data[$apartment_id]['tvr_amenity'][$feature->name] = $feature->term_id;
				}
			}


		}


		foreach( $apartment_data as $id => $apartment ) {
			if( empty( $apartment['current_price'] ) ) {
				unset( $apartment_data[$id] );
			}
		}

		return $apartment_data;
	}

	function get__thumbnail_id_data( $default_thumbnail_data = '', $apartment_id = 0 ) {
		if( $apartment_id != 0 ) {
			$default_thumbnail_data = get_post_meta( $apartment_id, '_thumbnail_id', true );
		}

		if( empty( $default_thumbnail_data ) ) {
			return false;
		}

		$thumbnail = $default_thumbnail_data;

		return $thumbnail;

	}

	function get_apartment_details_data( $details_data = '', $apartment_id = 0 ) {
		if( $apartment_id != 0 ) {
			$details_data = get_post_meta( $apartment_id, 'apartment_details', true );
		}

		if( empty( $details_data['name'][0] ) ) {
			return array();
		}

		$apartment_details = array();
		$count = count( $details_data['name'] );
		for( $i=0; $i < $count; $i++  ) {
			$apartment_details[$details_data['name'][$i]] = $details_data['value'][$i];
		}

		return $apartment_details;

	}



	function get_sortable_list( $apartment_data ) {
		$sortable_list = array();
		foreach( $apartment_data as $apartment_id => $apartment ) {
			foreach( $apartment as $data_name => $data_value ) {
				if( is_array( $data_value ) ) {
					foreach( $data_value as $item_name => $item_value  ) {
						$sortable_list[$apartment_id][$item_name] = $item_value;
					}
				}
				else {
					$sortable_list[$apartment_id][$data_name] = $data_value;
				}
			}
		}
		return $sortable_list;
	}




}
?>